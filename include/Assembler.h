#ifndef ASSEMBLER_H_
#define ASSEMBLER_H_

#include <string>

#include "../lib/Eigen/Dense"
#include "../lib/Eigen/Sparse"
#include "InputHandlers.h"

using namespace Eigen;

namespace Assembly {

class AssemblyObject {
 public:
    // Constructor & destructor
    AssemblyObject();
    ~AssemblyObject();
    // Variables
    VectorXd x;          // vector of gauss nodes x-direction
    VectorXd y;          // vector of gauss nodes y-direction
    VectorXd w;          // vector of gauss weights
    MatrixXd D;          // derivative matrix
    MatrixXd Dx;         // derivative matrix in x-direction
    MatrixXd Dy;         // derivative matrix in y-direction
    MatrixXd D2;         // 2nd order derivative matrix
    MatrixXd D2x;        // 2nd order derivative matrix in x-direction
    MatrixXd D2y;        // 2nd order derivative matrix in y-direction
    MatrixXd Dm;         // m-order derivative matrix
    MatrixXd A;          // system matrix
	MatrixXd *Ax;		 // Ax pointer matrix in 2D 
	MatrixXd *Ay;		 // Ay pointer matrix in 2D 
    VectorXd b;          // vector of right hand side for 1D
    MatrixXd bmat;       // matrix of right hand side for 2D
    VectorXd u;          // vector of solution
    VectorXd u0;         // initial vector of solution for 1D
    MatrixXd u0mat;      // initial vector of solution for 2D

    SparseMatrix<double, RowMajor> Asp;	// sparse system matrix for 2D
    SparseMatrix<double, RowMajor> Dsp;	// sparse derivative matrix for 2D

    double x0, xl;                   // solution interval points x-direction
    double y0, yl;                   // solution interval points y-direction
    VectorXd ut0;					 // initial vector at t = 0 for 1D 
    MatrixXd ut0mat;				 // initial matrix at t = 0 for 2D
    std::string boundaryCondition;   // type of boundary condition
    std::string boundaryConditionx;  // type of x-boundary condition 2D
    std::string boundaryConditiony;  // type of y-boundary condition 2D 
    std::string problemTypeStr;      // type of problem string
    std::string equationTypeStr;     // type of equation string
    double bc0, bcl;                 // boundary condition values 1D
    double bc0x, bclx;               // boundary condition x values 2D
    double bc0y, bcly;               // boundary condition y values 2D
    int interpolationNodes;		     // number of interpolation nodes

    double time;	 				 // length of the time interval in secs
    double timeStep;				 // time step for solution in secs
    std::string timeIntegration;	 // time integration method
    double aDiffusion;				 // diffusion coefficient
    double vAdvection;				 // advection velocity

    std::string execution;	 		 // execution type
    int cores;					     // number of core in parallel execution

    // Methods
    AssemblyObject Assembler(void);

    void GetIntervalValues1D(const Input::InputObject &obj);
    void GetBoundaryValues1D(const Input::InputObject &obj);
    void GetIntervalValues2D(const Input::InputObject &obj);
    void GetBoundaryValues2D(const Input::InputObject &obj);
    void GetTimeIntervalValues(const Input::InputObject &obj);

    enum ProblemType {Bvp1D, Bvp2D, Ivp1D, Ivp2D};
    enum EquationType {Laplace, Poisson, Helmholtz, UnsteadyDiffusion,
                      UnsteadyAdvection, UnsteadyAdvectionDiffusion,
                      UnsteadyInviscidBurger};
    enum MethodType {ChebyshevGL, LegendreGL, ChebyshevG, LegendreG, Fourier};
    enum InitialGuess {Random, Zero};\

	enum BoundaryConditions { Dirichlet, Neumann, DirNeu, NeuDir };
	BoundaryConditions bcx;
	BoundaryConditions bcy;

    ProblemType DefineProblemType(void);
    static ProblemType DefineProblemTypeStatic(void);

    template <class T>
    EquationType DefineEquationType(const T& obj);
    static EquationType DefineEquationTypeStatic(std::string equationType);

    template <class T>
    MethodType DefineMethodType(const T& obj);

    template <class T>
    InitialGuess DefineInitialGuess(const T& obj);

    static MatrixXd NegativeSumTrick(MatrixXd D);
    static MatrixXd HelmholtzMatrix(MatrixXd D2, double k);
    static SparseMatrix<double, RowMajor> HelmholtzMatrix2D(SparseMatrix<double, RowMajor> D2, double k);

    static VectorXd Map2Dto1D(MatrixXd mat);

    // Boundary Condition Methods initialization
    void DirichletInit1D(void);
    void NeumannInit1D(void);
    void DirichletNeumannInit1D(void);
    void NeumannDirichletInit1D(void);

    void DirichletInit2Dx(void);
    void NeumannInit2Dx(void);
    void DirichletNeumannInit2Dx(void);
    void NeumannDirichletInit2Dx(void);

    void DirichletInit2Dy(void);
    void NeumannInit2Dy(void);
    void DirichletNeumannInit2Dy(void);
    void NeumannDirichletInit2Dy(void);
};

}  // namespace Assembly

namespace Polynomials {

class PolynomialBasis : public Assembly::AssemblyObject{
 private:
    // Variables
    int polynomialOrder;  // order of polynomials
    double xPoint;        // x-axis point to evaluate polynomials
 public:
    // Methods
    static VectorXd RandomInitialGuess1D(int N);
    static VectorXd ZeroInitialGuess1D(int N);
    static MatrixXd RandomInitialGuess2D(int N);
    static MatrixXd ZeroInitialGuess2D(int N);
    static VectorXd ChebyshevGaussNodes(int N, double x0, double xl);
    static VectorXd ChebyshevGaussWeights(int N);
    static VectorXd ChebyshevGaussLobattoNodes(int N, double x0, double xl);
    static VectorXd ChebyshevGaussLobattoWeights(int N);
    static VectorXd LegendreGaussNodes(int N, double x0, double xl);
    static VectorXd LegendreGaussWeights(int N);
    static VectorXd LegendreGaussLobattoNodes(int N, double x0, double xl);
    static VectorXd LegendreGaussLobattoWeights(int N);
    static double Legendre(int k, double x);
    static double LegendrePrime(int k, double x);
    static double Chebyshev(int k, double x);
    static double ChebyshevPrime(int k, double x);
    static MatrixXd DerivativeMatrix(VectorXd x, VectorXd w);
    static MatrixXd MDerivativeMatrix(int m, VectorXd x, VectorXd w);
    static MatrixXd DerivativeMatrix2(MatrixXd D);
};

}  // namespace Polynomials

namespace Fourier {

class FourierBasis : public Assembly::AssemblyObject{
 public:
    // Methods
    static VectorXd FourierNodes(int N);
    static MatrixXd DerivativeMatrix(int N, int m);
    static MatrixXd HelmholtzMatrix(MatrixXd D2, double k);
    static SparseMatrix<double, RowMajor> HelmholtzMatrix2D(SparseMatrix<double, RowMajor> D2, double k);

    static MatrixXd ToeplitzMatrix(VectorXd col, VectorXd row);
};

}  // namespace Fourier

namespace Dimensions {

class Extend2D: public Assembly::AssemblyObject{
 private:
    // Variables
 public:
    // Methods
    static SparseMatrix<double, RowMajor> DerivativeMatrix2D(MatrixXd Dx, MatrixXd Dy);
};

}  // namespace Dimensions 

#endif  // INCLUDE_ASSEMBLER_H_
