#ifndef VISUALIZER_H
#define VISUALIZER_H

#include <string>

#include "../lib/Eigen/Dense"
#include "Assembler.h"

using namespace Eigen;

namespace Visualizer {

class VisualizerObject{
 public:
    // Constructor & destructor
    VisualizerObject (const Assembly::AssemblyObject &obj);
    ~VisualizerObject();
    // Variables
    const std::string *problemTypeStr;	// problem type string
    const int *interpolationNodes; 		// number of interpolation nodes

    // Methods
    VisualizerObject Visualizer(const Visualizer::VisualizerObject &obj);

    void PlotResultsBvp1D(bool saveSolutionFile) const;
    void PlotResultsBvp2D(bool saveSolutionFile) const;
    void AnimateResultsIvp1D(bool saveSolutionFile) const;
    void AnimateResultsIvp2D(bool saveSolutionFile) const;
};

}  // namespace Visualizer
#endif  // INCLUDE_VISUALIZER_H
