#ifndef INPUTHANDLERS_H_
#define INPUTHANDLERS_H_

#include <string>

#include "../lib/Eigen/Dense"

using namespace Eigen;

void initialize(void);

namespace Input {

class Init {
 public:
     // Variables
     const std::string pathToInputFile = "../config/input_config.json";
     // Methods
     std::string GetProblemType(std::string path);
};

class EquationParser {
 public:
     // Methods
     VectorXd Parser1D(std::string func, VectorXd xv, int N);
     MatrixXd Parser2D(std::string func, VectorXd xv, VectorXd yv, int N);
};

class InputObject {
 public:
    // Variables
    std::string equationType;       // type of equation
    std::string basisType;          // type of polynomial type
    std::string gaussianType;       // type of Gaussian quadrature
    double x0, xl;                  // solution interval points x-direction
    double y0, yl;                  // solution interval points y-direction
    std::string boundaryConditionx; // type of x-boundary condition for 2D
    std::string boundaryConditiony; // type of y-boundary condition for 2D
    double bc0x, bclx;              // boundary condition values at x=0 and x=Lx
    double bc0y, bcly;              // boundary condition values at y=0 and y=Ly
    std::string initialGuess;       // initial guess u0
    std::string rhsFunction;        // right hand side function
    int N;                          // number of gauss nodes
    int interpolationNodes;         // number of interpolation nodes
    double kWave;                   // wave number for helmholtz equation
    double aDiffusion;              // diffusion coefficient
    double vAdvection;              // advection velocity
    double time;	 				// length of the time interval in secs
    double timeStep;				// time step for solution in secs
    std::string timeIntegration;	// time integration method
    std::string initialCondition;	// initial condition function
    std::string execution;			// serial or parallel (PETSc) execution
    int cores;						// number of cores


    // Methods
    void Set1dBvpInputObjectFromFile(std::string path);
    void Set1dIvpInputObjectFromFile(std::string path);
    void Set2dBvpInputObjectFromFile(std::string path);
    void Set2dIvpInputObjectFromFile(std::string path);
};

}  // namespace Input

namespace AppConfig {

class AppConfigObject {
 public:
    const std::string pathToAppConfigFile = "../config/app_config.json";
    // Variables
    bool plotSolution;          // should plot solution
    bool saveSolutionFile;      // should save solution in file
    std::string problemType;    // problem type

    // Methods
    void SetAppConfigObjectFromFile(std::string path);
};

}  // namespace AppConfig
#endif  // INCLUDE_INPUTHANDLERS_H_
