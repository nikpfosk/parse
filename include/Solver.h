#ifndef SOLVER_H
#define SOLVER_H

#include <string>

#include "../lib/Eigen/Dense"
#include "../lib/Eigen/Sparse"
#include "Assembler.h"

using namespace Eigen;

namespace Solver {

class SolverObject{
 public:
    // Constructor & destructor
    SolverObject (Assembly::AssemblyObject &obj);
    ~SolverObject();
    // Variables
    MatrixXd *A;                    // system matrix
    MatrixXd *D;                    // differentiation matrix
    MatrixXd *Dx;                   // differentiation matrix x-direction
    MatrixXd *Dy;                   // differentiation matrix y-direction
    VectorXd *b;                    // vector of right hand side
    VectorXd *u;                    // vector of solution
    VectorXd *u0;                   // initial vector of solution

    SparseMatrix<double, RowMajor> *Asp;		// sparse system matrix for 2D
    SparseMatrix<double, RowMajor> *Dsp;		// sparse derivative matrix for 2D

    VectorXd *x;                    // gauss nodes x-direction
    VectorXd *y;                    // gauss nodes y-direction
    double *bc0x, *bclx;            // boundary condition values x-direction
    double *bc0y, *bcly;            // boundary condition values y-direction
    double *x0, *xl;                // interval limit values x-direction
    double *y0, *yl;                // interval limit values y-direction
    std::string *boundaryConditionx;// type of boundary conditions x-direction
    std::string *boundaryConditiony;// type of boundary conditions y-direction
    std::string *equationTypeStr;   // type of equation string
    std::string *executionStr;   	// type of execution string
    int *cores;						// number of cores in parallel execution

    VectorXd *ut0;					// initial condition
    double *time;	 				// length of the time interval in secs
    double *timeStep;				// time step for solution in secs
    std::string *timeIntegration;	// time integration method
    int steps;  					// number of steps
    double *aDiffusion;				// diffusion coefficient
    double *vAdvection;				// advection velocity
    MatrixXd H;                     // auxiliary matrix for time step methods

    SparseMatrix<double, RowMajor> Hsp;       // auxiliary sparse matrix for time step methods


    Assembly::AssemblyObject::EquationType
        equationType;	// equation type from assembly object


    enum BoundaryConditions{Dirichlet, Neumann, DirNeu, NeuDir};
    BoundaryConditions bcx;
    BoundaryConditions bcy;
    enum TimeIntegration{ForwardEuler, BackwardEuler, Leapfrog,
                        AdamsBashforth, AdamsMoulton, RungeKutta3,
                        RungeKutta4, RungeKuttaLS3};

    // Methods
    SolverObject Solver(Solver::SolverObject &obj);

    template <class T>
    TimeIntegration DefineTimeIntegration(const T& obj);

    MatrixXd GetTimeStepMatrix(void);
    SparseMatrix<double, RowMajor> GetTimeStepMatrix2D(void);

    // Boundary Condition Methods
    void DirichletEnforce1D(void);
    void DirichletNeumannEnforce1D(void);
    void NeumannDirichletEnforce1D(void);

    void DirichletEnforce2Dx(void);
    void DirichletNeumannEnforce2Dx(void);
    void NeumannDirichletEnforce2Dx(void);

    void DirichletEnforce2Dy(void);
    void DirichletNeumannEnforce2Dy(void);
    void NeumannDirichletEnforce2Dy(void);

    // Solver Methods
    void DirectSolver(void);
    void IterativeSolver(void);
    void DirectTimeSolver(void);
    void IterativeTimeSolver(void);
    void IterativeSolverPetsc(SparseMatrix<double, RowMajor> *A,
                              VectorXd *b, VectorXd *u, int *cores);

    // Time Integration Methods
    void ForwardEulerSolver1D(void);
    void BackwardEulerSolver1D(void);
    void LeapfrogSolver1D(void);
    void AdamsBashforthSolver1D(void);
    void AdamsMoultonSolver1D(void);
    void RungeKutta3Solver1D(void);
    void RungeKutta4Solver1D(void);
    void RungeKuttaLS3Solver1D(void);

    void ForwardEulerSolver2D(void);
    void BackwardEulerSolver2D(void);
    void LeapfrogSolver2D(void);
    void AdamsBashforthSolver2D(void);
    void AdamsMoultonSolver2D(void);
    void RungeKutta3Solver2D(void);
    void RungeKutta4Solver2D(void);
    void RungeKuttaLS3Solver2D(void);

    // Print Methods
    void PrintResultsToFileBvp1D(void);
    void PrintResultsToFileBvp2D(void);
    void PrintResultsToFileIvp1DInit(void);
    void PrintResultsToFileIvp1D(double currentTime);
    void PrintResultsToFileIvp1DFinalize(void);
    void PrintResultsToFileIvp2DInit(void);
    void PrintResultsToFileIvp2D(double currentTime);
    void PrintResultsToFileIvp2DFinalize(void);
    void SerializeMatrixAscii(SparseMatrix<double, RowMajor> A,
        std::string filename);
    void SerializeVectorAscii(VectorXd v, std::string filename);
    void DeserializeVectorAscii(VectorXd &v, std::string filename);
};

}  // namespace Solver 
#endif  // INCLUDE_SOLVER_H
