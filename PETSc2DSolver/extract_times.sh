#!/bin/bash

#Provide filename as parameter
#Useful example: ./extract_times.sh "Laplace2D.*"

echo 'File= '$1
echo '=========='

COUNT=0

while read line; do
  COUNT=$(( $COUNT + 1 ))
  if [[ $line =~ "iterations" ]] ; then  
		awk -v var="$COUNT" 'FNR == var {print $5; print $6; exit}' $1
  fi
  if [[ $line =~ "./laplace2d" ]] ; then  
		awk -v var="$COUNT" 'FNR == var {print $8; print $9; exit}' $1
  fi
  if [[ $line =~ "Time (sec):" ]] ; then
		awk -v var="$COUNT" 'FNR == var {print $3; exit}' $1
		echo '=========='
  fi
done < $1
