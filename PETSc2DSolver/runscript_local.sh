#!/bin/bash

#Runs the matlab script with arguments and then extracts the timings from the log summary

cd LaplaceSEM2D

matlab -nodisplay -nodesktop -r "DriverLaplaceSolverLinearStandingWaves(32, 32, 30, 1)"

cd ..

make

./laplace2d -ksp_type fgmres -pc_type gamg -log_summary > log_summary.txt

cd LaplaceSEM2D

matlab -nodisplay -nodesktop -r "DriverLaplaceSolverLinearStandingWavesValidation(32, 32, 30, 1)"

cd ..

./extract_times.sh log_summary.txt
