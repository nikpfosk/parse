function [Kk] = advection2DPrismsimple(k,Np,J,MassMatrix,varargin)
% By Allan P. Engsig-Karup, apek@dtu.dk.
rz = varargin{1};
sz = varargin{2};
Dr = varargin{3};
Ds = varargin{4};
Dzk = diag(rz(:,k))*Dr+diag(sz(:,k))*Ds; % can be simplified 
% Local assembly term by term
Jk = J(:,k);
% Construct local element matrix by matrix-based integration
if nargin>8
    sigmaz = varargin{5};
    Szk = sigmaz(:,k);
    Kk = diag(Jk.*Szk)*MassMatrix*Dzk; % Mass matrix, with b
else
    Kk = diag(Jk)*MassMatrix*Dzk;% Mass matrix, no b
end
return
