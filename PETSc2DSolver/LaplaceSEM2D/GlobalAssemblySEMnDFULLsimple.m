function [A] = GlobalAssemblySEMnDFULLsimple(Nk,Np,ConnectionTable,varargin)
%
% By Allan P. Engsig-Karup, apek@dtu.dk.
%
%gidx = max(ConnectionTable(:));
Ai = ones(Np*Np*Nk,1);
Aj = ones(Np*Np*Nk,1);
As = zeros(Np*Np*Nk,1);
count = 0;
for k = 1 : Nk
    Ak = varargin{1}(k,Np,varargin{2:end});
    idx = ConnectionTable(k,:);
    for j = 1 : Np
        Ai(count + (1:Np)) = idx;
        Aj(count + (1:Np)) = idx(j);
        As(count + (1:Np)) = Ak(:,j);
        count = count + Np;
    end
end
A = sparse(Ai,Aj,As);
return

