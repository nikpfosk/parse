function [nx, ny, sJ] = Normals2Dquad(x,y,Dr,Ds,Nfaces,Nfpx,Nfps,K,rout,sout)
% function [nx, ny, sJ] = Normals2Dquad()
% Purpose : Compute outward pointing normals at elements faces and surface Jacobians
% By Allan P. Engsig-Karup.

%Globals2DspectralhpFEM;
xr = Dr*x; yr = Dr*y; xs = Ds*x; ys = Ds*y; %J = xr.*ys-xs.*yr;

% Build coarse-to-fine grid interpolation matrix
%[rout,sout] = Nodes2Dquad(Nf); rout = rout(:); sout = sout(:);

NODETOL = 1e-12;

% find all the nodes that lie on each edge (Faces)
fmask1 = find( abs(sout+1) < NODETOL)'; % south
fmask2 = find( abs(rout-1) < NODETOL)'; % east
fmask3 = find( abs(sout-1) < NODETOL)'; % north
fmask4 = find( abs(rout+1) < NODETOL)'; % west
Fmask  = [fmask1';fmask2';fmask3';fmask4']';

% interpolate geometric factors to face nodes
fxr = xr(Fmask, :); fxs = xs(Fmask, :); fyr = yr(Fmask, :); fys = ys(Fmask, :);

% build normals
nx = zeros(2*Nfpx+2*Nfps, K); ny = zeros(2*Nfpx+2*Nfps, K);
fid1 = (1:Nfpx)';         fid2 = (Nfpx+1:(Nfpx+Nfps))'; 
fid3 = ((Nfpx+Nfps)+1:(2*Nfpx+Nfps))'; fid4 = ((2*Nfpx+Nfps)+1:(2*Nfpx+2*Nfps))'; 

% face 1 : south
nx(fid1, :) =  fyr(fid1, :); ny(fid1, :) = -fxr(fid1, :); 

% face 2 : east
nx(fid2, :) =  fys(fid2, :); ny(fid2, :) = -fxs(fid2, :);

% face 3 : north
nx(fid3, :) = -fyr(fid3, :); ny(fid3, :) =  fxr(fid3, :);

% face 4 : west
nx(fid4, :) = -fys(fid4, :); ny(fid4, :) =  fxs(fid4, :);

% normalise
sJ = sqrt(nx.*nx+ny.*ny); nx = nx./sJ; ny = ny./sJ;
return;
