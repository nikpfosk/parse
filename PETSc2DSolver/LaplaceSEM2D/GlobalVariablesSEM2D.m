% Purpose: declare global variables

global EToV1D VX1D 
%Filter1DSVV epsiSVV SVVopX SVVopXdiff dt
global Np Nfp N K Ns
global r s 
global Dr Ds LIFT Drw Dsw MassMatrix
global Fx Fy nx ny jac Fscale J
global rx ry sx sy J sJ
global rk4a rk4b rk4c
global Nfaces EToE EToF EToV
global V invV
global x y NODETOL VX VY

% Low storage Runge-Kutta coefficients
rk4a = [            0.0 ...
        -567301805773.0/1357537059087.0 ...
        -2404267990393.0/2016746695238.0 ...
        -3550918686646.0/2091501179385.0  ...
        -1275806237668.0/842570457699.0];
rk4b = [ 1432997174477.0/9575080441755.0 ...
         5161836677717.0/13612068292357.0 ...
         1720146321549.0/2090206949498.0  ...
         3134564353537.0/4481467310338.0  ...
         2277821191437.0/14882151754819.0];
rk4c = [             0.0  ...
         1432997174477.0/9575080441755.0 ...
         2526269341429.0/6820363962896.0 ...
         2006345519317.0/3224310063776.0 ...
         2802321613138.0/2924317926251.0]; 

%global LINEARONOFF lumpedON OVERINTEGRATIONLINEARSYSTEMONOFF PLOTTINGONOFF SVVONOFF
%global SIGMATRANSFORMONOFF FSelements INTERIORelements 
global Scatter2D Scatter1D Gather2D 
global iSurface iInterior
global x X2D S2D h hx hxx X2Dp hd
global iboundary iboundaryp 
global DuplicateFSvariableDownColumns
global DMx1D
global Nk1D Nk Nk2D Np c
%global J rx sx tx ry sy ty rz sz tz Dr Ds Dt
global A_U A_L L2Dsub L2D II Sx2Dp
global g
global Ex Px Exx h0
%global Var_d Var_Ex Var_SigmaZ Var_SigmaX Var_E sigma Var_H Var_Hx
global J2D MassMatrix rx2D sx2D ry2D sy2D Dr2D Ds2D V2D
global c2D c1D c2DPERIODIC
global gidx2D gidx2Dp
global sol b
global M2D_L M2D_U rvec2 rvec Sz2D
global x1d
global J1D Ic2f1D MassMatrixfine1D rx1D Dr1D Mass1D
%global Ic2f1D Ic2f2D r2Dfine
global x1delm
global Np1D
global M1D
global MassMatrixfine2D
global R2D P2D Np2D P1D R1D
global gidx1D
global ConnectionTable1D
%global IIe
%global J1Df rx1Df Dr1Df 
%global rx2Df sx2Df ry2Df sy2Df Dr2Df Ds2Df J2Df
global s1D r1D
%global NeY
global tri
global k1 k2
global idxElementsObject2D idxFacesObjects2D velcogBody idxFaceL
