function [Scatter,Gather] = ScatterGatherSEMnD(Nk,Np,gidx,c)
%
% Implemented by Allan P. Engsig-Karup, apek@dtu.dk.
%

%% Create Global-To-Local (SCATTER) operator from connectivity tables in any spatial dimension.
ii = zeros(Nk*Np,1);
jj = zeros(Nk*Np,1);
ss =  ones(Nk*Np,1);
count = 0;
for k = 1 : Nk
    for i = 1 : Np
        count = count + 1;
        ii(count) = count;
        jj(count) = c(k,i);
    end
end
Scatter = sparse(ii,jj,ss);

%% Create Local-To-Global (GATHER) operator
% to a full 3D grid
Gather = spalloc(gidx,Nk*Np,gidx);
count = 0;
for k = 1 : Nk
    for i = 1 : Np
        count = count + 1;
        Gather(c(k,i),:) = 0;
        Gather(c(k,i),count) = 1;
    end
end
return
