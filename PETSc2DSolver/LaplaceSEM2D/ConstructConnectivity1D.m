function [ConnectionTable,gidx] = ConstructConnectivity1D(EToV,P)
% Build connectivity matrix for a ordered 1D mesh.
%
% By Allan P. Engsig-Karup, apek@dtu.dk.
K = size(EToV,1);
Np = P+1;

gidx = 2;
ConnectionTable = zeros(K,Np);
for k = 1 : K
    gidx = gidx - 1;
    for i = 1 : Np
        ConnectionTable(k,i) = gidx;
        gidx = gidx + 1;
    end
end
return
