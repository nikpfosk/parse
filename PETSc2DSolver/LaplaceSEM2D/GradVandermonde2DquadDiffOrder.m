function [V2Dr,V2Ds] = GradVandermonde2DquadDiffOrder(N,r,s,Ns)

% function [V2Dr,V2Ds] = GradVandermonde2D(N,r,s,Ns)
% Purpose : Initialize the gradient of the modal basis (i,j) at (r,s) at order N	

V2Dr = zeros(length(r),(N+1)*(Ns+1)); V2Ds = zeros(length(r),(N+1)*(Ns+1));

% Initialize matrices
sk = 1;
for i=0:N
  for j=0:Ns
    [V2Dr(:,sk)] = GradJacobiP(r, 0, 0, i).*JacobiP(s, 0, 0, j);
    [V2Ds(:,sk)] = JacobiP(r, 0, 0, i).*GradJacobiP(s, 0, 0, j);
    sk = sk+1;
  end
end
return
