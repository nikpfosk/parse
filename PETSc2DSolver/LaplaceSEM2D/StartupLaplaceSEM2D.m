% StartupOceanWaveSEM2D.m
% Purpose: Setup all operators etc. on a given mesh

disp('Initializing solver and constructing grid and metrics...')

%% Initialize solver and construct grid and metric
[ p, EToV1D ] = GenerateMesh1D(x);
Nk1D = size(EToV1D,1);
VX1D = p;

% Definition of constants
Npf = N+1; % number of points on a face
P   = N;
Npfs = Ns+1; % number of points on a vertical face
Np  = Npf*Npfs;
Np1D = Npf;

% Compute nodal set
[r1D] = JacobiGL(0,0,N); % local nodes,LGL
[s1D] = JacobiGL(0,0,Ns); % local nodes,LGL

%% Create Spectral/hp-FEM mesh
% build coordinates of all the nodes
% x defines interfaces of the elements
[x1d,x1delm] = mesh1D(x,EToV1D,r1D);

% Build reference element matrices
[V1D]  = Vandermonde1D(P,r1D);
[Vr1D] = GradVandermonde1D(P,r1D);

% calculate geometric factors
h1D  = p(EToV1D(:,2))-p(EToV1D(:,1));

% construct connectivity table and mapping operators
[ConnectionTable1D, gidx1D] = ConstructConnectivity1D(EToV1D,N);
c1D = ConnectionTable1D;

%% GLOBAL ASSEMBLY
% Build reference element matrices
% Standard element matrices
Dr1D   = Vr1D / V1D;

[M1D] = GlobalAssemblyHPFEM1DFULL(Nk1D,r1D,h1D,ConnectionTable1D,@massmatrix1D);
[A1D] = GlobalAssemblyHPFEM1DFULL(Nk1D,r1D,h1D,ConnectionTable1D,@advectionmatrix1D);

% Differentiation matrix in the plane
DMx1D = @(xxx)  M1D\(A1D*xxx(:));

% Construct 1D restriction operator
R1D = zeros(size(M1D,1)-1,size(M1D,1));
count = 0;
Restrictionidx = unique(ConnectionTable1D(:))';
for i = Restrictionidx
    count = count + 1;
    R1D(count,i) = 1;
end
R1D = sparse(R1D);

Np1D = N+1;
gidx1D = length(R1D*x1d(:));

% Construct 1D prolongation operator
P1D = zeros(size(M1D,1),size(M1D,1)-1);
idxs = 1 : length(Restrictionidx);
newidxs = R1D'*idxs';
newidxs2 = newidxs;
count = 0;
for i = 1 : length(newidxs2)
    count = count + 1;
    P1D(count,newidxs2(i)) = 1;
end
P1D = sparse(P1D);

%%%% FINISH 1D Stuff 

%% Generate 2D mesh for volume elements
% Extend 1D mesh tables to account for third dimension
% Purpose : Setup script, building operators, grid, metric,
%           and connectivity tables for 2D meshes of quadrilaterals.
if vert == 0
    s = linspace(0,1,NeY+1);
else
    s = sin(pi*(0:NeY)/(2*NeY)); % clustered toward free surface
end
[X,S] = meshgrid(x,s);
L = zeros(NeY+1,NeX+1);     % global nodes
L(:) = 1 : (NeX+1)*(NeY+1); % assign unique numbers
count = 0;
EToV = [];
for i = NeY :-1 : 1 % make sure that we have same numbering for free surface elements in both 1D and 2D meshes.
    for j = 1 : NeX
        count = count + 1;
        tmp = L(i:i+1,j:j+1); tmp(1:2) = tmp([2 1]);
        EToV(count,[1 2 3 4]) = [ tmp(:) ];
    end
end
Nk = size(EToV,1);
p = [X(:) S(:)];
VX = p(:,1)';
VY = p(:,2)';

%% Generate local mesh for reference element
% output: r,s
% local node coordinates for reference element nodes
r = r1D;%linspace(-1,1,N+1);
s = JacobiGL(0,0,Ns);
[r,s] = meshgrid(r,s); % tensor-product basis
r = r(:);
s = s(:);

% Compute index maps for node positions
tol = min(0.2/N^2,1e-5);

% Faces
fid1 = find( abs(r+1) < tol)'; % west
fid2 = find( abs(s+1) < tol)'; % south
fid3 = find( abs(r-1) < tol)'; % east
fid4 = find( abs(s-1) < tol)'; % north
% Interior
fint = setdiff(1:Np,[fid1 fid2 fid3 fid4]);

% Reorder local nodes to match the chosen ordering in terms of vertices,
% edges, interior nodes.
% LocalReorder odering must match with chosen ordering scheme: vertices,
% edges, interior nodes...
LocalReorder = [find(r==-1 & s==1) find(r==-1 & s==-1)...
    find(r==1 & s==-1) find(r==1 & s==1) fid1(2:Npfs-1) ...
    fid2(2:Npf-1) fid3(Npfs-1:-1:2) fid4(Npf-1:-1:2) fint];
if length(LocalReorder)~=length(r(:))
    wer % tol is probably not correct.
    return
end
r = r(LocalReorder);
s = s(LocalReorder);
clear fid1 fid2 fid3 fid4 fint

% Build reference element matrices
V1D  = Vandermonde1D(N, r1D); invV1D = inv(V1D);
Mass1D  = inv(V1D*V1D');
V1Dr = GradVandermonde1D(N, r1D);
Dr1D = Dmatrix1D(N, r1D, V1D);
% build coordinates of all the nodes
va = EToV1D(:,1)'; vb = EToV1D(:,2)';
xxx1D = ones(N+1,1)*VX1D(va) + 0.5*(r1D(:)+1)*(VX1D(vb)-VX1D(va));
% calculate geometric factors
[rx1D,J1D] = GeometricFactors1D(xxx1D,Dr1D);

% build coordinates of all the nodes
vd = EToV(:,1); va = EToV(:,2); vb = EToV(:,3); vc = EToV(:,4);
x = (1-r).*(1-s)/4*VX(va) + (1+r).*(1-s)/4*VX(vb) + ...
    (1+r).*(1+s)/4*VX(vc) + (1-r).*(1+s)/4*VX(vd);
y = (1-r).*(1-s)/4*VY(va) + (1+r).*(1-s)/4*VY(vb) + ...
    (1+r).*(1+s)/4*VY(vc) + (1-r).*(1+s)/4*VY(vd);

%% GENERATE CONNECTIVITY TABLE
% create connectivity list with global numbers for each local degree of
% freedom
Nv = size(p,1); % number of global vertices
[c2D,gidx2D,X2D,S2D] = ConstructConnectivityTable2DQuadDiffOrder(VX,VY,EToV,N,x,y,Ns);
[V2D] = Vandermonde2DquadDiffOrder(N, r, s, Ns);
[Dr2D,Ds2D] = Dmatrices2DquadDiffOrder(N, r, s, V2D, Ns);
[rx2D,sx2D,ry2D,sy2D,J2D] = GeometricFactors2D(x,y,Dr2D,Ds2D);

MassMatrix  = inv(V2D*V2D');

%% Modification for periodic operators
c2DPERIODIC = c2D;
II = speye(size(S2D));

%% GLOBAL ASSEMBLY
Nk2D = Nk;
Np2D = Np;
[M2Dp] = GlobalAssemblySEMnDFULLsimple(Nk,Np2D,c2DPERIODIC,@massmatrixsimple,J2D,MassMatrix);
[Kx2Dp] = GlobalAssemblySEMnDFULLsimple(Nk,Np2D,c2DPERIODIC,@dstiffnessmatrix2Dsimple,J2D,MassMatrix,rx2D,sx2D,Dr2D,Ds2D);
[Sy2Dp] = GlobalAssemblySEMnDFULLsimple(Nk,Np2D,c2DPERIODIC,@advectionmatrix2Dsimple,J2D,MassMatrix,ry2D,sy2D,Dr2D,Ds2D);
% make sure we have matrices of right size
if size(M2Dp)~=gidx2D
    [ii,jj,ss] = find(M2Dp);  M2Dp  = sparse(ii,jj,ss,gidx2D,gidx2D);
    [ii,jj,ss] = find(Kx2Dp); Kx2Dp = sparse(ii,jj,ss,gidx2D,gidx2D);
    [ii,jj,ss] = find(Sy2Dp); Sy2Dp = sparse(ii,jj,ss,gidx2D,gidx2D);
end

%% Generate condensation operators (make operators compact)
RestrictionIdx = unique(c2DPERIODIC(:))';
count = 0; 
R2D = zeros(length(RestrictionIdx),gidx2D);
for i = RestrictionIdx
    count = count + 1;
    R2D(count,i) = 1;
end
R2D = sparse(R2D);
    
%% Generate scatter operator (for visualization needs)
idxs = 1 : length(RestrictionIdx);
newidxs = R2D'*idxs'; % scatter without duplicate nodes on east boundaries set

newidxs2 = newidxs;
count = 0;
P2D = zeros(gidx2D,length(RestrictionIdx));
for i = 1 : length(newidxs2)
    count = count + 1;
    P2D(count,newidxs2(i)) = 1;
end
P2D = sparse(P2D);

Kx2DpCompact = R2D*Kx2Dp*P2D;
Sy2DpCompact = R2D*Sy2Dp*P2D;
M2DpCompact  = R2D*M2Dp*P2D;
gidx2Dp = size(M2DpCompact,1);

[Scatter1D,Gather1D] = ScatterGatherSEMnD(Nk1D,Np1D,gidx1D,c1D);
[Scatter2D,Gather2D] = ScatterGatherSEMnD(Nk2D,Np2D,gidx2D,c2D);

x1darray = Scatter1D*x1d;
x1darray = reshape(x1darray,Np1D,Nk1D);

%% create index maps
iboundary = find( abs(S2D-1)<1e-6 ); % free surface
[dummy, isort] = sort(X2D(iboundary));
iboundary = iboundary(isort);
iSurface = iboundary;
iInterior   = setdiff(1:gidx2D,iSurface);

%% Create operator which extract free surface values down the columns
FillFS = zeros(Nk*Np,Nk*Np);
count = 0;
if 1
    % Generate table of indexes for FS nodes
    XXX2D = Scatter2D*X2D(:);
    SSS2D = Scatter2D*S2D(:);
    iiiboundary = find( abs(SSS2D-1)<1e-6 ); % free surface
    [dummy, isort] = sort(XXX2D(iiiboundary));
    iiiboundary = iiiboundary(isort);
    xFS = XXX2D(iiiboundary);
    %    xFS = X2D(iboundary);
    for k = 1 : Nk
        for i = 1 : Np
            count = count + 1;
            % determine which FS node
            xcurrent = x(i,k);
            idx = find(abs(xcurrent-xFS)<1e-6);
            FillFS(count,iiiboundary(idx(1))) = 1;
        end
    end
else % original, works for one layer of elements in vertical
    for k = 1 : Nk
        for i = 1 : Np
            count = count + 1;
            FillFS(count,(k-1)*Np + mask(i)) = 1;
        end
    end
end
FillFS = sparse(FillFS);

%% simple test
tmp = zeros(Np,Nk);
tmp(:) = Scatter2D*S2D(:);
tmp(:) = FillFS*tmp(:);
check = tmp==1;
if check
else
    wer % FillFS not correct
end

%% Combined operator
DuplicateFSvariableDownColumns  = Gather2D*FillFS*Scatter2D;

iboundary = find( abs(S2D-1)<1e-6 ); % free surface
[dummy, isort] = sort(X2D(iboundary));
iboundary = iboundary(isort);

rvec2 = symamd(M2Dp);
[M2D_L,M2D_U] = lu(M2Dp(rvec2,rvec2));

X2Dp = X2D;
iboundaryp = iboundary;

% Detect which elements belong to the free surface from normals
[nx, ny, sJ] = Normals2Dquad(x,y,Dr2D,Ds2D,4,N+1,Ns+1,Nk,r,s);
[rowidx,columnidx] = find(ny>0);
[rowidxFS,columnidxFS] = find(y==1); 
FSelements = unique(columnidxFS);
disp(sprintf('A total of %d free surface elements have been detected.',length(FSelements)))
INTERIORelements = setdiff(1:Nk2D,FSelements);

