function [Ak] = advectionmatrix1D(hk,P,r) 
%
%
% By Allan P. Engsig-Karup, apek@dtu.dk, 05-11-2009.
%

%[r]  = JacobiGL(0,0,P);
[V]  = Vandermonde1D(P,r);
[Vr] = GradVandermonde1D(P,r);

% Standard element matrices
M    = inv(V*V');
Dr   = Vr / V;
A    = M*Dr;

% Local assembly
Ak = A;

return
