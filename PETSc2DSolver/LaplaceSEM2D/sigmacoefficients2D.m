function [Var_d, Var_Ex, Var_SigmaZ, Var_SigmaX, Var_S, Var_H, Var_hx, Var_dx] = sigmacoefficients2D(x,X2D,S2D,h,hx,E,Ex,iboundary,DuplicateFSvariableDownColumns,Scatter)

Var_d = X2D'*0;
Var_d(iboundary) = E + h;
Var_d = DuplicateFSvariableDownColumns*Var_d;

%disp('WARNING: sigmacoefficients2D (assumed linear)')
%Var_d(:) = h;

Var_dx = X2D'*0;
Var_dx(iboundary) = Ex + hx;
Var_dx = DuplicateFSvariableDownColumns*Var_dx;

Var_Ex = X2D'*0;
Var_Ex(iboundary) = Ex;
Var_Ex = DuplicateFSvariableDownColumns*Var_Ex;

Var_hx = X2D'*0;
Var_hx(iboundary) = hx;
Var_hx = DuplicateFSvariableDownColumns*Var_hx;

Var_SigmaX = (1-S2D')./Var_d.*Var_hx - (S2D'./Var_d).*Var_Ex;

Var_SigmaZ = 1./Var_d;

tmp = Var_SigmaX; Var_SigmaX = x*0;

Var_SigmaX(:) = Scatter*tmp(:);

tmp = Var_SigmaZ; Var_SigmaZ = x*0;
Var_SigmaZ(:) = Scatter*tmp(:);

Var_S = S2D';
tmp = Var_S; Var_S = x*0;
Var_S(:) = Scatter*tmp(:);

Var_H = X2D'*0;
Var_H(iboundary) = h;
Var_H = DuplicateFSvariableDownColumns*Var_H;
tmp = Var_H; Var_H = x*0;
Var_H(:) = Scatter*tmp(:);

tmp = Var_Ex; Var_Ex = x*0;
Var_Ex(:) = Scatter*tmp(:);

tmp = Var_hx; Var_hx = x*0;
Var_hx(:) = Scatter*tmp(:);

tmp = Var_d; Var_d = x*0;
Var_d(:) = Scatter*tmp(:);

tmp = Var_dx; Var_dx = x*0;
Var_dx(:) = Scatter*tmp(:);
return
