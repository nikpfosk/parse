function DriverLaplaceSolverLinearStandingWavesValidation(PL_in, Ps_in, NeX_in, NeY_in)
% close all
% clear all
% clc
%
% Driver script for executing a Laplace solver in 2D discretized
% using the Spectral Element Method (SEM).
%
% By Allan P. Engsig-Karup, apek@dtu.dk.
%
GlobalVariablesSEM2D; 
PL   = double(PL_in); % Pol. order in horizontal
Ps   = double(Ps_in); % Pol. order in vertical, essentially nodes in vertical
NeX  = double(NeX_in); % No. of elements in the x-direction
NeY  = double(NeY_in); % No. of elements in the y-direction

display(sprintf('Ps = %d, NeX = %d ',Ps,NeX));

%% Parameters
% Polynomial order used for approximation
N    = PL; % horizontal order
Ns   = Ps; % vertical order
vert = 1; % node clustering towards free surface on off

%% Wave parameters
g      = 9.81;
Lx     = 1;
kh     = 1;
lwave  = Lx;
kwave  = 2*pi/lwave;
hd     = kh/kwave;
cwave = sqrt(g/kwave*tanh(kh));
Twave = lwave / cwave;
wwave = 2*pi / Twave;
Hwave = 0.02;

%% Generate 2D Mesh
% adjust mesh to have element interfaces positioned near kinks in the
% bottom gradients
dxx = Lx / NeX;
x = [linspace(0,Lx,Lx/dxx+1)];
XX = x(:);
dxmin = min(diff(x));

% Setup all operators etc. on a given mesh
StartupLaplaceSEM2D;

%% INITIAL CONDITION
% Determine analytical solution at free surface
time = 0.25;
z = 0; % free surface level z=0
[U2, W2, E2, E2x, E2xx, P2] = LinearStandingwave1D(Hwave,cwave,kwave,z,hd,wwave,time,x1d);
            
% define bottom
h   = x1d*0+hd;
hx  = x1d*0;

% Initial conditions
E   = E2;
P   = P2;
W   = E*0;
Ex  = E*0;

% Allocate variables for linear system
b   = X2D(:)*0;
sol = b*0;

% Laplace solver
[W] = LaplaceSEM2DValidation(E,P);
% Exact solution
[U2, W2, E2, E2x, E2xx, P2] = LinearStandingwave1D(Hwave,cwave,kwave,z,hd,wwave,time,x1d);
% Plot vertical velocity components for exact and computer solutions

% plot(x1d,W2,'k',x1d,W,'rx')

display(sprintf('Error: %d',norm(W-W2)));
exit;
