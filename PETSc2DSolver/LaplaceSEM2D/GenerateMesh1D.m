function [ p, EToV ] = GenerateMesh1D(x)
% Generate ordered mesh data tables for a 1D mesh from a vertice list x
% By Allan P. Engsig-Karup, apek@dtu.dk.
p = x;
EToV = [(1:length(x(:))-1)' (2:length(x(:)))'];
return
