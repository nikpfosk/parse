function [x1d, X] = mesh1D(x,EToV,r)
% Create a 1D mesh from a set of vertices x and connectivities EToV.
%
% By Allan P. Engsig-Karup, apek@dtu.dk.

Np = length(r(:));
K  = size(EToV,1);
X  = zeros(Np,K);
for k = 1 : K
    hk = diff(x(EToV(k,:)));
    % create extended mesh
    X(1:Np,k)  = x(k) + (1+r)/2*hk;
end
x1d = X(1:Np-1,1:K);
x1d = [x1d(:); X(Np,K)];
return
