function [Dr,Ds] = Dmatrices2DquadDiffOrder(N,r,s,V,Ns)

% function [Dr,Ds] = Dmatrices2Dquad(N,r,s,V)
% Purpose : Initialize the (r,s) differentiation matrices
%	    on the quadrilateral, evaluated at (r,s) at order N

[Vr, Vs] = GradVandermonde2DquadDiffOrder(N, r, s, Ns);
Dr = Vr/V; Ds = Vs/V;
return
