function [A,b] = GlobalAssemblyHPFEM1DFULL(Nk,r,h,ConnectionTable,varargin)
%
% Global assembly. Algorithm 3.2.
%
% By Allan P. Engsig-Karup, apek@dtu.dk, 05-11-2009.
Np = length(r(:)); P = Np-1;
A = spalloc(Nk*Np-Nk+1,Nk*Np-Nk+1,Np*3*Nk);
b = zeros(Nk*Np-Nk+1,1);
for k = 1 : Nk
    if isempty(varargin)
        % default
        [Ak] = stiffnessmatrix1D(h(k),P,r);
    elseif nargin==5 % optional function handle
        Ak = varargin{1}(h(k),P,r);
    elseif nargin==6
        % special case where integrand are nonlinear. use local arrays as
        % input to procedure as well.
        la = varargin{2}(ConnectionTable(k,:)); % local array -- DOES NOT WORK WITH PERIODIC GRIDS ARRAYS DUE TO CONNECTIONTABLE (last element node)
        Ak = varargin{1}(h(k),P,r,la);
    elseif nargin==8
        % special case where integrand are nonlinear. use local arrays as
        % input to procedure as well.
        la = varargin{2}(ConnectionTable(k,:)); % local array -- DOES NOT WORK WITH PERIODIC GRIDS ARRAYS DUE TO CONNECTIONTABLE (last element node)
        lb = varargin{3}(ConnectionTable(k,:)); % local array -- DOES NOT WORK WITH PERIODIC GRIDS ARRAYS DUE TO CONNECTIONTABLE (last element node)
        Pquad = varargin{4}; % number of quadrature nodes at fine grid to do exact integration of integrals (no variation crimes)
        Ak = varargin{1}(h(k),P,r,la,lb,Pquad);
    else
        warning('Could not understand list of arguments.')
        wer
    end
    idx = ConnectionTable(k,:);
    A(idx,idx) = A(idx,idx) + Ak;
end
return


