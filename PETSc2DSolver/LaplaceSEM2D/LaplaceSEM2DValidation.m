function [W, L2D, Sz2D, b, sol] = LaplaceSEM2DValidation(E,P)
%
% This driver script executes the SEM solver.
%
% By Allan P. Engsig-Karup, apek@dtu.dk.
%

GlobalVariablesSEM2D;

%% Solve Linear System to get W
% Evaluate coefficients of sigma-transformed system
[Var_d, Var_Ex, Var_SigmaZ, Var_SigmaX, Var_S, Var_H, Var_Hx, Var_dx] = sigmacoefficients2D(x,X2D,S2D,h,hx,P1D*E*0,P1D*Ex*0,iboundary,DuplicateFSvariableDownColumns,Scatter2D);

II = speye(gidx2Dp,gidx2Dp);
[L2D] = GlobalAssemblySEMnDFULLsimple(Nk,Np,c2DPERIODIC,@laplacematrixsigmatransformedsymmetric2Dsimple,J2D,MassMatrix,rx2D,sx2D,ry2D,sy2D,Dr2D,Ds2D,Var_S,Var_d,Var_dx,Var_Hx);
if size(L2D)~=gidx2D
    [ii,jj,ss] = find(L2D);  L2D  = sparse(ii,jj,ss,gidx2D,gidx2D);
end
L2D = R2D*L2D*P2D; % Compress due to periodicity if it is used
L2Dsub = L2D(iInterior,iSurface);
L2D(iSurface,:) = II(iSurface,:);
L2D(:,iSurface) = II(:,iSurface);

%% Impose boundary conditions and setup linear system
b = b*0; % reset
b(iSurface)  = P(:);
b(iInterior) = -L2Dsub*b(iSurface); 
% % SOLVE LINEAR SYSTEM to determine W
% disp('LU factorization...')
% rvec = symamd(L2D);
% PetscBinaryWrite('A.mat', L2D);
% PetscBinaryWrite('b.mat', b);
%  [A_L,A_U] = lu(L2D(rvec,rvec));
%  disp('...completed!')
%  sol(rvec) = A_U\( A_L\b(rvec) );
sol = PetscBinaryRead('solution.mat');

% Global assembly of stiffness matrix without imposing boundary conditions 
sigmaz = x*0;
sigmaz(:) = (1./(Var_d));
[Sz2D] = GlobalAssemblySEMnDFULLsimple(Nk,Np,c2DPERIODIC,@advection2DPrismsimple,J2D,MassMatrix,ry2D,sy2D,Dr2D,Ds2D,sigmaz);
% Make sure size is correct
if size(Sz2D)~=gidx2D
    [ii,jj,ss] = find(Sz2D);  Sz2D = sparse(ii,jj,ss,gidx2D,gidx2D);
end
Sz2D = R2D*Sz2D*P2D; % Compress due to periodicity if it is used

% Determine vertical free surface velocity from solution
tmp = Sz2D * (sol(:));
WW(rvec2) = M2D_U \  ( M2D_L \( tmp(rvec2) ) );
W = WW(iSurface)';
%W = R1D*W; % restrict to periodic grid if this is used

return

