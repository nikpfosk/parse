function [c,gidx,X2D,Y2D] = ConstructConnectivityTable2DQuadDiffOrder(VX,VY,EToV,P,x,y,Ps)
%
% Construct local-to-global connectivity tables.
% For Quadrilaterals.
%
% Ordering used is: [Vertices Facenodes Interiornodes]
%
% FIXME: VX and VY not used really.
%
% By Allan P. Engsig-Karup, apek@dtu.dk.
Nfaces = size(EToV,2);
[EToE,EToF]= tiConnect2D(EToV,Nfaces);
Npf    = P + 1;
Npfs   = Ps + 1;
Np     = Npf*Npfs; % quads
Nv     = size(VX,1);   % number of global vertices
Nk     = size(EToV,1); % number of elements
c      = zeros(Nk,Np);
X2D    = VX(unique(EToV));
Y2D    = VY(unique(EToV));
gidx   = length(X2D);   % no assumption on vertices and numbering
for k = 1 : Nk

%    disp(sprintf('Element No. %d',k))
%    gidxold = gidx;
    
    % global vertices
    c(k,1:Nfaces) = EToV(k,:);

    % global edges (add edge nodes)
    for i = 1 : Nfaces
%        disp(sprintf('  Adding face No. %d',i))
    switch i
    case 1
        idx  = 0 + (1:Npfs-2);
        idx2 = (1:Npfs-2);
    case 2
        idx = (Npfs-2) + (1:Npf-2);
        idx2 = (1:Npf-2);
    case 3
        idx = (Npfs-2) + (Npf-2) + (1:Npfs-2);
        idx2 = (1:Npfs-2);
    case 4
        idx = (Npfs-2) + (Npf-2) + (Npfs-2) + (1:Npf-2);
        idx2 = (1:Npf-2);
    end
    if not(isempty(idx2)) % only add nodes if order is larger then one
        if EToE(k,i)>=k % if connecting element is new then we have new nodes
            % increase global numbering as usual
                
            c(k,Nfaces+idx) = gidx + idx2; %gidx+1:gidx+Npf-2; % insert edge nodes
            tmpx = x(Nfaces+idx,k);
            X2D = [X2D tmpx(:)'];
            tmpy = y(Nfaces+idx,k);
            Y2D = [Y2D tmpy(:)'];
%            plot(x,y,'k.')
%            hold on
%            plot(tmpx,tmpy,'ro')
%            hold off
%            drawnow
%            pause
%            X2D = [X2D x(gidx+1:gidx+Npf-2)];
%            Y2D = [Y2D y(gidx+1:gidx+Npf-2)];
            gidx = gidx + idx2(end); % Npf-2 new edge nodes added
        else % we already assigned global numbers to the edge nodes.
            % lookup node numbering
            kconnect = EToE(k,i); % element number of connecting element with edge nodes already set
            iconnect = EToF(k,i); % face number of connecting element            
            lidx = Nfaces+idx; % local index for element in question
            switch iconnect
                case 1
                    idx  = 0 + (Npfs-2:-1:1);
                case 2
                    idx = (Npfs-2) + (Npf-2:-1:1);
                case 3
                    idx = (Npfs-2) + (Npf-2) + (Npfs-2:-1:1);
                case 4
                    idx = 2*(Npfs-2) + (Npf-2) + (Npf-2:-1:1);
            end
            
            % reverse ordering in connecting element
            c(k,lidx) = c(kconnect,Nfaces+idx); 
%            c(k,lidx) = c(kconnect,Nfaces+(iconnect-1)*(Npf-2)+(1:Npf-2)); 
        end
    end
    end
%    disp(sprintf('  Adding interior nodes'))

    % global interior modes
    lidx = (Nfaces+2*(Npf-2)+2*(Npfs-2)+1):Np;
%    tmp = length(VX) + (k-1)*(Np-Nfaces) + Nfaces*(Npf-2) + (1 : (Np-Nfaces-Nfaces*(Npf-2)))
%    c(k,lidx) = tmp;

    c(k,lidx) = gidx + 1 : gidx + Np - Nfaces - 2*(Npf-2) - 2*(Npfs-2);
    tmpx = x(lidx,k);
    X2D = [X2D tmpx(:)'];
    tmpy = y(lidx,k);
    Y2D = [Y2D tmpy(:)'];
%    X2D = [X2D x(gidx + 1 : gidx + Np - Nfaces - Nfaces*(Npf-2))];
%    Y2D = [Y2D y(gidx + 1 : gidx + Np - Nfaces - Nfaces*(Npf-2))];
    gidx = gidx + Np - Nfaces - 2*(Npf-2) - 2*(Npfs-2);
    
%    close all
%    plot(X2D,Y2D,'k.'), hold on
%    plot(X2D(gidxold+1:gidx),Y2D(gidxold+1:gidx),'ro')
%    drawnow
%    pause
end
