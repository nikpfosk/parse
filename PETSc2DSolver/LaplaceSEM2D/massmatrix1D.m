function [Mk] = massmatrix1D(hk,P,r) 
%
%
% By Allan P. Engsig-Karup, apek@dtu.dk.
%

[V]  = Vandermonde1D(P,r);

% Standard element matrices
M    = inv(V*V');

% Local assembly
Mk = hk/2*M; 

return
