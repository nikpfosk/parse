function [Kk] = massmatrixsimple(k,Np,J,MassMatrix,varargin)
%
% By Allan P. Engsig-Karup, apek@dtu.dk.
%
Jk = J(:,k);
if nargin>4
    if nargin==4
        b  = varargin{1};
        bk = b(:,k);
        Kk = (diag(Jk))'*MassMatrix*(diag(bk));% Mass matrix
    else
        b  = varargin{1};
        bk = b(:,k);
        IIf = varargin{2};
        Kk = (diag(Jk)*IIf)'*MassMatrix*(diag(bk)*IIf);% Mass matrix
    end
else
    Kk = diag(Jk)*MassMatrix;% Mass matrix
end
return
