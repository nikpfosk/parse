function [Kk] = laplacematrixsigmatransformedsymmetric2Dsimple(k,Np,J,MassMatrix,varargin)
% By Allan P. Engsig-Karup, apek@dtu.dk.
Jk  = J(:,k);
rx  = varargin{1};
sx  = varargin{2};
ry  = varargin{3};
sy  = varargin{4};
Dr  = varargin{5};
Ds  = varargin{6};
s   = varargin{7};  %s  = s(:,k) ;
d   = varargin{8};  %d  = d(:,k)  + 0*Jk ;
dx  = varargin{9};  %dx = dx(:,k) + 0*Jk ;
hx  = varargin{10}; %hx = hx(:,k);

sdx = s.*dx;
k11 = d;
k12 = -(sdx-hx);
%k21 = k12;
tmp1 = sdx-hx;
%k22 = (1+(sdx).^2+hx.^2-2*sdx.*hx)./d;
k22 = (1+tmp1.^2)./d;

k11 = sqrt(k11);
%k12 = sqrt(k12);
k22 = sqrt(k22);

Dx = diag(rx(:,k))*Dr+diag(sx(:,k))*Ds;
Dy = diag(ry(:,k))*Dr+diag(sy(:,k))*Ds; % Dy = derivative in vertical sigma-coordinate
k11k = diag(k11(:,k));
k12k = diag(k12(:,k));
%    k21k = k12k;
k22k = diag(k22(:,k));
k11kDx = k11k*Dx;
k12kDx = k12k*Dx;
k22kDy = k22k*Dy;
Jk = diag(J(:,k));
M = MassMatrix;
Kk = -( (Jk*k11kDx)'*M*(k11kDx) + (Jk*k12kDx)'*M*(Dy) + (Jk*Dy)'*M*(k12kDx) + (Jk*k22kDy)'*M*(k22kDy) );

if norm(Kk-Kk',inf)>1e-8
    % %k
    % %norm(Kk-Kk2,inf)
    norm(Kk-Kk',inf)
    disp('not symmetric. laplacematrixsigmatransformedsymmetric2Dsimple')
    %     if norm(Kk-Kk',inf)>1
    wer % not symmetric
    %     end
end
return

