function [Kk] = dstiffnessmatrix2Dsimple(k,Np,J,MassMatrix,varargin)
% By Allan P. Engsig-Karup, apek@dtu.dk, 28-10-2014.
rX = varargin{1};
sX = varargin{2};
Dr = varargin{3};
Ds = varargin{4};
DXk = spdiags(rX(:,k),0,Np,Np)*Dr+spdiags(sX(:,k),0,Np,Np)*Ds;  % can be simplified 
% Local assembly term by term
Jk = spdiags(J(:,k),0,Np,Np);
% Construct local element matrix by matrix-based integration
if nargin>10
    b = varargin{7};
    bk = spdiags(b(:,k),0,Np,Np);
    Kk = Jk*bk*DXk'*MassMatrix*DXk; % Mass matrix, with b
else
    Kk = Jk*DXk'*MassMatrix*DXk;% Mass matrix, no b
end
return
