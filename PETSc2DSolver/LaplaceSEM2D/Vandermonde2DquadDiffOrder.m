function [V2D] = Vandermonde2DquadDiffOrder(N, r, s, Ns)

% function [V2D] = Vandermonde2DquadDiffOrder(N, r, s, Ns);
% Purpose : Initialize the 2D Vandermonde Matrix, 
%           V_{ij} = phi_j(r_i, s_i)
V2D = zeros(length(r(:)),(N+1)*(Ns+1));

% build the Vandermonde matrix
sk = 1;
for i=0:N
  for j=0:Ns
    V2D(:,sk) = JacobiP(r(:),0,0,i).*JacobiP(s(:),0,0,j);
    sk = sk+1;
  end
end
return
