function [Kk] = advectionmatrix2Dsimple(k,Np,J,MassMatrix,varargin)
%
% By Allan P. Engsig-Karup, apek@dtu.dk.
%
rX = varargin{1};
sX = varargin{2};
Dr = varargin{3};
Ds = varargin{4};
DXk = diag(rX(:,k))*Dr+diag(sX(:,k))*Ds;  
% Local assembly term by term
Jk = J(:,k);
% Construct local element matrix by matrix-based integration
if nargin>8
    if nargin==10
        b = varargin{5};
        bk = b(:,k);
        IIf = varargin{6};
        Kk = (diag(Jk.*bk)*IIf)'*MassMatrix*DXk; % Mass matrix, with b
    elseif nargin==9
        b = varargin{5};
        bk = b(:,k);        
        Kk = (diag(Jk.*bk))'*MassMatrix*DXk;% Mass matrix, no b
    else
        wer % error
    end
else
    Kk = diag(Jk)*MassMatrix*DXk;% Mass matrix, no b
end
return
