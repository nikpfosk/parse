function [uu, ww, eta, etax, etaxx, pp] = linearstandingwave1D(H,c,k,z,h,w,t,x)
%
% Analytic solution for a linear standing wave.
%
% By Allan P. Engsig-Karup, apek@dtu.dk.
if (c-w/k)>1e-8
    warning('phase speed does not match w/k as it should in input.')
end

% Compute analytical solution at level z
uu    = k*H*c*cosh(k*(z+h))./sinh(k*h).*sin(w*t).*sin(k*x);
ww    = -k*H*c*sinh(k*(z+h))./sinh(k*h).*sin(w*t).*cos(k*x);
eta   = H * cos(w*t)*cos(k*x);   % z = 0
etax  = -k*H*cos(w*t)*sin(k*x);  % z = 0
etaxx = -k^2*H*cos(w*t)*cos(k*x);  % z = 0
pp    = -H*c*cosh(k*(z+h))./sinh(k*h).*sin(w*t).*cos(k*x);
return

