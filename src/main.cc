/*
    File: main.cc

    Purpose: The main method of the framework

    @author: nikpfosk
*/

#include <iostream>
#include <string>
#include <fstream>

#include "../lib/Eigen/Dense"
#include "../include/InputHandlers.h"
#include "../include/Assembler.h"
#include "../include/Solver.h"
#include "../include/Visualizer.h"

using namespace Eigen;

// Controls operation of the program
int main(int argc, char *argv[]) {
    initialize();

    Assembly::AssemblyObject assemblyObj;
    assemblyObj = assemblyObj.Assembler();

    Solver::SolverObject solverObj(assemblyObj);
    solverObj = solverObj.Solver(solverObj);

    Visualizer::VisualizerObject visualizerObj(assemblyObj);
    visualizerObj = visualizerObj.Visualizer(visualizerObj);

    return 0;
}
