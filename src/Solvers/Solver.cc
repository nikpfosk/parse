/*
    File: Solver.cc

    Purpose: Contains methods that create and define the solver object
    @author: nikpfosk
*/

#include <iostream>
#include <string>
#include <fstream>

#include "../../include/Assembler.h"
#include "../../include/Solver.h"
#include "../../include/Visualizer.h"
#include "../../lib/unsupported/Eigen/IterativeSolvers"

using namespace Eigen;

/*
    Namespace: Solver
    Definitions for Solver
*/

Solver::SolverObject::SolverObject(Assembly::AssemblyObject &obj) {
    A = &obj.A;
    D = &obj.D;
    Dx = &obj.Dx;
    Dy = &obj.Dy;
    b = &obj.b;
    u0 = &obj.u0;
    u = &obj.u;
    x = &obj.x;
    y = &obj.y;
    bc0x = &obj.bc0x;
    bclx = &obj.bclx;
    bc0y = &obj.bc0y;
    bcly = &obj.bcly;
    x0 = &obj.x0;
    xl = &obj.xl;
    y0 = &obj.y0;
    yl = &obj.yl;
    Asp = &obj.Asp;
    Dsp = &obj.Dsp;
    boundaryConditionx = &obj.boundaryConditionx;
    boundaryConditiony = &obj.boundaryConditiony;
    equationTypeStr = &obj.equationTypeStr;

    ut0 = &obj.ut0;
    time = &obj.time;
    timeStep = &obj.timeStep;
    timeIntegration = &obj.timeIntegration;
    aDiffusion = &obj.aDiffusion;
    vAdvection = &obj.vAdvection;

    executionStr = &obj.execution;
    cores = &obj.cores;
}
Solver::SolverObject::~SolverObject() {
}

/*
    Interface for the solver class.

    @param the solver object
    @return the modified solver object
*/
Solver::SolverObject Solver::SolverObject::Solver(Solver::SolverObject &obj) {
    std::cout << "\n****************************************\n" << std::endl;
    std::cout << "Solver process started." << std::endl;

    //Assembly::AssemblyObject ptObj;
    Assembly::AssemblyObject::ProblemType problemType =
            Assembly::AssemblyObject::DefineProblemTypeStatic();

    if ((*boundaryConditionx) == "dirichlet") {
        bcx = BoundaryConditions::Dirichlet;
    } else if ((*boundaryConditionx) == "neumann") {
        bcx = BoundaryConditions::Neumann;
    } else if ((*boundaryConditionx) == "dirichlet-neumann") {
        bcx = BoundaryConditions::DirNeu;
    } else if ((*boundaryConditionx) == "neumann-dirichlet") {
        bcx = BoundaryConditions::NeuDir;
    } else {
        std::cout << "ERROR: Not valid boundary conditions type." << std::endl;
        throw std::exception();
    }

    switch (problemType) {
        case Assembly::AssemblyObject::ProblemType::Bvp2D:
        case Assembly::AssemblyObject::ProblemType::Ivp2D: {
            if ((*boundaryConditiony) == "dirichlet") {
                bcy = BoundaryConditions::Dirichlet;
            } else if ((*boundaryConditiony) == "neumann") {
                bcy = BoundaryConditions::Neumann;
            } else if ((*boundaryConditiony) == "dirichlet-neumann") {
                bcy = BoundaryConditions::DirNeu;
            } else if ((*boundaryConditiony) == "neumann-dirichlet") {
                bcy = BoundaryConditions::NeuDir;
            } else {
                std::cout << "ERROR: Not valid boundary conditions type." << std::endl;
                throw std::exception();
            }
            break;
        }
    }

    switch (problemType) {
        case Assembly::AssemblyObject::ProblemType::Ivp1D: {
            case Assembly::AssemblyObject::ProblemType::Ivp2D: {
                steps = int((*time) / (*timeStep)) + 1;
                break;
            }
        }
    }

    switch (problemType) {
        case Assembly::AssemblyObject::ProblemType::Bvp1D: {
            obj.DirectSolver();
            switch (bcx) {
                case Dirichlet: {
                    obj.DirichletEnforce1D();
                    break;
                }
                case DirNeu: {
                    obj.DirichletNeumannEnforce1D();
                    break;
                }
                case NeuDir: {
                    obj.NeumannDirichletEnforce1D();
                    break;
                }
            }
			this->PrintResultsToFileBvp1D();
            break;
        }
        case Assembly::AssemblyObject::ProblemType::Bvp2D: {
			obj.IterativeSolver();
            switch (bcx) {
                case Dirichlet: {
                    obj.DirichletEnforce2Dx();
                    break;
                }
                case DirNeu: {
                    obj.DirichletNeumannEnforce2Dx();
                    break;
                }
                case NeuDir: {
                    obj.NeumannDirichletEnforce2Dx();
                    break;
                }
            }
            switch (bcy) {
                case Dirichlet: {
                    obj.DirichletEnforce2Dy();
                    break;
                }
                case DirNeu: {
                    obj.DirichletNeumannEnforce2Dy();
                    break;
                }
                case NeuDir: {
                    obj.NeumannDirichletEnforce2Dy();
                    break;
                }
            }
			this->PrintResultsToFileBvp2D();
            break;
		}
        case Assembly::AssemblyObject::ProblemType::Ivp1D: {
            obj.DirectTimeSolver();
            break;
        }
        case Assembly::AssemblyObject::ProblemType::Ivp2D:
            obj.IterativeTimeSolver();
            break;
    }


    return obj;
}
