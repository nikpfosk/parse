/*
    File: Solver2D.cc

    Purpose: Contains methods that are used to solve 2D problems
    @author: nikpfosk
*/

#include <iostream>
#include <string>
#include <fstream>

#include "../../include/Solver.h"
#include "../../lib/unsupported/Eigen/IterativeSolvers"

using namespace Eigen;
/*
    After getting solution of system, apply the dirichlet BC in 2D x-direction.

    @param -
    @return -
    We work with pointers to the matrices and vectors.
*/
void Solver::SolverObject::DirichletEnforce2Dx(void) {
    int N = (*x).size();
	Map<MatrixXd> M((*u).data(), N, N);

    if (*bc0x == 0 && *bclx != 0) {  // right BC is not zero
        for (int i = 0; i < (*x).size(); i++) {
			for (int j = 0; j < (*y).size(); j++) {
				M(i,j) = M(i,j) + (*bclx * ((*x)(i) - *x0)) / (*xl - *x0);
			}
        }
    } else if (*bc0x != 0 && *bclx == 0) {  // left BC is not zero
        for (int i = 0; i < (*x).size(); i++) {
			for (int j = 0; j < (*y).size(); j++) {
				M(i,j) = M(i,j) + (*bc0x * ((*x)(i) - *xl)) / (*x0 - *xl);
			}
        }
    } else if (*bc0x != 0 && *bclx != 0) {  // left and right BC are not zero
        for (int i = 0; i < (*x).size(); i++) {
			for (int j = 0; j < (*y).size(); j++) {
				M(i,j) = M(i,j) + ((*bc0x - *bclx) / (*x0 - *xl)) * (*x)(i) +
					(*bc0x - ((*bc0x - *bclx) / (*x0 - *xl)) * *x0);
			}
        }
    }

	Map<RowVectorXd> vec(M.data(), M.size());
	*u = vec.eval();
}

/*
    After getting solution of system, apply the dirichlet-neumann BC in 2D x-direction.

    @param -
    @return -
    We work with pointers to the matrices and vectors.
*/
void Solver::SolverObject::DirichletNeumannEnforce2Dx(void) {
    int N = (*x).size();
	Map<MatrixXd> M((*u).data(), N, N);

    if (*bc0x != 0) {  // left BC is not zero
        for (int i = 0; i < (*x).size(); i++) {
			for (int j = 0; j < (*y).size(); j++) {
				M(i,j) = M(i,j) + (*bc0x * ((*x)(i) - *xl)) / (*x0 - *xl);
			}
        }
    }

	Map<RowVectorXd> vec(M.data(), M.size());
	*u = vec.eval();
}

/*
    After getting solution of system, apply the neumann-dirichlet BC in 2D x-direction.

    @param -
    @return -
    We work with pointers to the matrices and vectors.
*/
void Solver::SolverObject::NeumannDirichletEnforce2Dx(void) {
    int N = (*x).size();
	Map<MatrixXd> M((*u).data(), N, N);

    if (*bclx != 0) {  // rightBC is not zero
        for (int i = 0; i < (*x).size(); i++) {
			for (int j = 0; j < (*y).size(); j++) {
				M(i,j) = M(i,j) + (*bclx * ((*x)(i) - *x0)) / (*xl - *x0);
			}
        }
    }

	Map<RowVectorXd> vec(M.data(), M.size());
	*u = vec.eval();
}

/*
    After getting solution of system, apply the dirichlet BC in 2D y-direction.

    @param -
    @return -
    We work with pointers to the matrices and vectors.
*/
void Solver::SolverObject::DirichletEnforce2Dy(void) {
    int N = (*y).size();
	Map<MatrixXd> M((*u).data(), N, N);

    if (*bc0y == 0 && *bcly != 0) {  // right BC is not zero
        for (int i = 0; i < (*x).size(); i++) {
			for (int j = 0; j < (*y).size(); j++) {
				M(i,j) = M(i,j) + (*bcly * ((*y)(j) - *y0)) / (*yl - *y0);
			}
        }
    } else if (*bc0y != 0 && *bcly == 0) {  // left BC is not zero
        for (int i = 0; i < (*x).size(); i++) {
			for (int j = 0; j < (*y).size(); j++) {
				M(i,j) = M(i,j) + (*bc0y * ((*y)(j) - *yl)) / (*y0 - *yl);
			}
        }
    } else if (*bc0y != 0 && *bcly != 0) {  // left and right BC are not zero
        for (int i = 0; i < (*x).size(); i++) {
			for (int j = 0; j < (*y).size(); j++) {
				M(i,j) = M(i,j) + ((*bc0y - *bcly) / (*y0 - *yl)) * (*y)(j) +
					(*bc0y - ((*bc0y - *bcly) / (*y0 - *yl)) * *y0);
			}
        }
    }

	Map<RowVectorXd> vec(M.data(), M.size());
	*u = vec.eval();
}

/*
    After getting solution of system, apply the dirichlet-neumann BC in 2D y-direction.

    @param -
    @return -
    We work with pointers to the matrices and vectors.
*/
void Solver::SolverObject::DirichletNeumannEnforce2Dy(void) {
    int N = (*y).size();
	Map<MatrixXd> M((*u).data(), N, N);

    if (*bc0y != 0 && *bcly == 0) {  // left BC is not zero
        for (int i = 0; i < (*x).size(); i++) {
			for (int j = 0; j < (*y).size(); j++) {
				M(i,j) = M(i,j) + (*bc0y * ((*y)(j) - *yl)) / (*y0 - *yl);
			}
        }
    }

	Map<RowVectorXd> vec(M.data(), M.size());
	*u = vec.eval();
}

/*
    After getting solution of system, apply the neumann-dirichlet BC in 2D y-direction.

    @param -
    @return -
    We work with pointers to the matrices and vectors.
*/
void Solver::SolverObject::NeumannDirichletEnforce2Dy(void) {
    int N = (*y).size();
	Map<MatrixXd> M((*u).data(), N, N);

    if (*bc0y == 0 && *bcly != 0) {  // right BC is not zero
        for (int i = 0; i < (*x).size(); i++) {
			for (int j = 0; j < (*y).size(); j++) {
				M(i,j) = M(i,j) + (*bcly * ((*y)(j) - *y0)) / (*yl - *y0);
			}
        }
    }

	Map<RowVectorXd> vec(M.data(), M.size());
	*u = vec.eval();
}

/*
    Solves the system of linear equations with iterative solver

    @param -
    @return -
    We work with pointers to the matrices and vectors.
*/
void Solver::SolverObject::IterativeSolver(void) {
    if (executionStr->compare(std::string("serial")) == 0) {
        GMRES< SparseMatrix<double, RowMajor> > solver(*Asp);
        *u = solver.solve(*b);
        std::cout << "#iterations:     " << solver.iterations() << std::endl;
        std::cout << "estimated error: " << solver.error() << std::endl;
    }else if(executionStr->compare(std::string("parallel")) == 0) {
        VectorXd utemp = VectorXd::Zero((*b).size());
        (*u) = utemp;
        IterativeSolverPetsc(Asp, b, u, cores);
    }
}

/*
    Solves the system of linear equations iteratively in 2D Initial Value Problems (IVP)

    @param -
    @return -
    We work with pointers to the matrices and vectors.
*/
void Solver::SolverObject::IterativeTimeSolver(void) {
    TimeIntegration ti;
    if ((*timeIntegration) == "forwardEuler") {
        ti = TimeIntegration::ForwardEuler;
    } else if ((*timeIntegration) == "backwardEuler") {
        ti = TimeIntegration::BackwardEuler;
    } else if ((*timeIntegration) == "leapfrog") {
        ti = TimeIntegration::Leapfrog;
    } else if ((*timeIntegration) == "adamsBashforth") {
        ti = TimeIntegration::AdamsBashforth;
    } else if ((*timeIntegration) == "adamsMoulton") {
        ti = TimeIntegration::AdamsMoulton;
    } else if ((*timeIntegration) == "rungeKutta3") {
        ti = TimeIntegration::RungeKutta3;
    } else if ((*timeIntegration) == "rungeKutta4") {
        ti = TimeIntegration::RungeKutta4;
    } else if ((*timeIntegration) == "rungeKuttaLS3") {
        ti = TimeIntegration::RungeKuttaLS3;
    } else {
        std::cout << "ERROR: Not valid time integration type." << std::endl;
        throw std::exception();
    }

    switch (ti) {
        case ForwardEuler: {
            this->ForwardEulerSolver2D();
            break;
        }
        case BackwardEuler: {
            this->BackwardEulerSolver2D();
            break;
        }
        case Leapfrog: {
            this->LeapfrogSolver2D();
            break;
        }
        case AdamsBashforth: {
            this->AdamsBashforthSolver2D();
            break;
        }
        case AdamsMoulton: {
            this->AdamsMoultonSolver2D();
            break;
        }
        case RungeKutta3: {
            this->RungeKutta3Solver2D();
            break;
        }
        case RungeKutta4: {
            this->RungeKutta4Solver2D();
            break;
        }
        case RungeKuttaLS3: {
            this->RungeKuttaLS3Solver2D();
            break;
        }
    }
}

/*
    Integrates in time using the forward euler method

    @param -
    @return -
    We work with pointers to the matrices and vectors.
*/
void Solver::SolverObject::ForwardEulerSolver2D(void) {

    Hsp = GetTimeStepMatrix2D();

    VectorXd *uPrev;
    double currentTime = 0.;
    (*u) = (*ut0);
    uPrev = &(*u);

   this->PrintResultsToFileIvp2DInit();

    for (int i = 0; i < steps; i++) {
        switch (bcx) {
            case Dirichlet: {
                this->DirichletEnforce2Dx();
                break;
            }
            case DirNeu: {
                this->DirichletNeumannEnforce2Dx();
                break;
            }
            case NeuDir: {
                this->NeumannDirichletEnforce2Dx();
                break;
            }
        }
        switch (bcy) {
            case Dirichlet: {
                this->DirichletEnforce2Dy();
                break;
            }
            case DirNeu: {
                this->DirichletNeumannEnforce2Dy();
                break;
            }
            case NeuDir: {
                this->NeumannDirichletEnforce2Dy();
                break;
            }
        }
        this->PrintResultsToFileIvp2D(currentTime);
        switch (equationType) {
            case Assembly::AssemblyObject::EquationType
            ::UnsteadyDiffusion:
            case Assembly::AssemblyObject::EquationType
            ::UnsteadyAdvection:
            case Assembly::AssemblyObject::EquationType
            ::UnsteadyAdvectionDiffusion: {
                (*u) = (*uPrev) + Hsp * (*uPrev);
                break;
            }
            case Assembly::AssemblyObject::EquationType
            ::UnsteadyInviscidBurger: {
                (*u) = (*uPrev) - Hsp * (0.5 * (*uPrev).cwiseProduct((*uPrev)));
            }
        }
        (*uPrev) = (*u);
        currentTime += (*timeStep);
    }
    this->PrintResultsToFileIvp2DFinalize();
}

/*
    Integrates in time using the backward euler method

    @param -
    @return -
    We work with pointers to the matrices and vectors.
*/
void Solver::SolverObject::BackwardEulerSolver2D(void) {

    SparseMatrix<double, RowMajor> I((*ut0).size(), (*ut0).size());
    SparseMatrix<double, RowMajor> *Atemp;
    I.setIdentity();
    Hsp = GetTimeStepMatrix2D();
    Hsp = I - Hsp;

    //VectorXd uPrev = VectorXd::Zero((*ut0).size());
    VectorXd *uPrev;
    double currentTime = 0.;
    (*u) = (*ut0);
    uPrev = &(*u);

    this->PrintResultsToFileIvp2DInit();

    for (int i = 0; i < steps; i++) {
        switch (bcx) {
            case Dirichlet: {
                this->DirichletEnforce2Dx();
                break;
            }
            case DirNeu: {
                this->DirichletNeumannEnforce2Dx();
                break;
            }
            case NeuDir: {
                this->NeumannDirichletEnforce2Dx();
                break;
            }
        }
        switch (bcy) {
            case Dirichlet: {
                this->DirichletEnforce2Dy();
                break;
            }
            case DirNeu: {
                this->DirichletNeumannEnforce2Dy();
                break;
            }
            case NeuDir: {
                this->NeumannDirichletEnforce2Dy();
                break;
            }
        }
        this->PrintResultsToFileIvp2D(currentTime);
        switch (equationType) {
            case Assembly::AssemblyObject::EquationType
            ::UnsteadyDiffusion:
            case Assembly::AssemblyObject::EquationType
            ::UnsteadyAdvection:
            case Assembly::AssemblyObject::EquationType
            ::UnsteadyAdvectionDiffusion:
            case Assembly::AssemblyObject::EquationType
            ::UnsteadyInviscidBurger: {
                if (executionStr->compare(std::string("serial")) == 0) {
                    GMRES< SparseMatrix<double, RowMajor> > solver(Hsp);
                    *u = solver.solve(*uPrev);
                }else if(executionStr->compare(std::string("parallel")) == 0) {
                    (Atemp) = &Hsp;
                    IterativeSolverPetsc(Atemp, uPrev, u, cores);
                }
                break;
            }
        }
        (*uPrev) = (*u);
        currentTime += (*timeStep);
    }
    this->PrintResultsToFileIvp2DFinalize();
}

/*
    Integrates in time using the leapfrog method

    @param -
    @return -
    We work with pointers to the matrices and vectors.
*/
void Solver::SolverObject::LeapfrogSolver2D(void) {

    Hsp = GetTimeStepMatrix2D();

    VectorXd *uPrev;
    VectorXd *uPrev2;
    double currentTime = 0.;
    (*u) = (*ut0);
    uPrev = &(*u);
    uPrev2 = &(*uPrev);
    switch (bcx) {
        case Dirichlet: {
            this->DirichletEnforce2Dx();
            break;
        }
        case DirNeu: {
            this->DirichletNeumannEnforce2Dx();
            break;
        }
        case NeuDir: {
            this->NeumannDirichletEnforce2Dx();
            break;
        }
    }
    switch (bcy) {
        case Dirichlet: {
            this->DirichletEnforce2Dy();
            break;
        }
        case DirNeu: {
            this->DirichletNeumannEnforce2Dy();
            break;
        }
        case NeuDir: {
            this->NeumannDirichletEnforce2Dy();
            break;
        }
    }
    this->PrintResultsToFileIvp2DInit();
    this->PrintResultsToFileIvp2D(currentTime);
    currentTime += (*timeStep);
    switch (equationType) {
        case Assembly::AssemblyObject::EquationType
        ::UnsteadyDiffusion:
        case Assembly::AssemblyObject::EquationType
        ::UnsteadyAdvection:
        case Assembly::AssemblyObject::EquationType
        ::UnsteadyAdvectionDiffusion:
        case Assembly::AssemblyObject::EquationType
        ::UnsteadyInviscidBurger: {
            (*u) = (*uPrev) + Hsp * (*uPrev);
            break;
        }
    }
    (*uPrev) = (*u);
    switch (bcx) {
        case Dirichlet: {
            this->DirichletEnforce2Dx();
            break;
        }
        case DirNeu: {
            this->DirichletNeumannEnforce2Dx();
            break;
        }
        case NeuDir: {
            this->NeumannDirichletEnforce2Dx();
            break;
        }
    }
    switch (bcy) {
        case Dirichlet: {
            this->DirichletEnforce2Dy();
            break;
        }
        case DirNeu: {
            this->DirichletNeumannEnforce2Dy();
            break;
        }
        case NeuDir: {
            this->NeumannDirichletEnforce2Dy();
            break;
        }
    }
    this->PrintResultsToFileIvp2D(currentTime);
    currentTime += (*timeStep);

    for (int i = 0; i < steps - 2; i++) {
        if (i > 0) {
            switch (bcx) {
                case Dirichlet: {
                    this->DirichletEnforce2Dx();
                    break;
                }
                case DirNeu: {
                    this->DirichletNeumannEnforce2Dx();
                    break;
                }
                case NeuDir: {
                    this->NeumannDirichletEnforce2Dx();
                    break;
                }
            }
            switch (bcy) {
                case Dirichlet: {
                    this->DirichletEnforce2Dy();
                    break;
                }
                case DirNeu: {
                    this->DirichletNeumannEnforce2Dy();
                    break;
                }
                case NeuDir: {
                    this->NeumannDirichletEnforce2Dy();
                    break;
                }
            }
        }
        this->PrintResultsToFileIvp2D(currentTime);
        switch (equationType) {
            case Assembly::AssemblyObject::EquationType
            ::UnsteadyDiffusion:
            case Assembly::AssemblyObject::EquationType
            ::UnsteadyAdvection:
            case Assembly::AssemblyObject::EquationType
            ::UnsteadyAdvectionDiffusion:
            case Assembly::AssemblyObject::EquationType
            ::UnsteadyInviscidBurger: {
                (*u) = (*uPrev2) + 2 * Hsp * (*uPrev);
                break;
            }
        }
        (*uPrev2) = (*uPrev);
        (*uPrev) = (*u);
        currentTime += (*timeStep);
    }
    this->PrintResultsToFileIvp2DFinalize();
}

/*
    Integrates in time using the adams bashfort method

    @param -
    @return -
    We work with pointers to the matrices and vectors.
*/
void Solver::SolverObject::AdamsBashforthSolver2D(void) {

    Hsp = GetTimeStepMatrix2D();

    VectorXd *uPrev;
    VectorXd *uPrev2;
    VectorXd *uPrev3;
    double currentTime = 0.;
    (*u) = (*ut0);
    uPrev = &(*u);
    uPrev2 = &(*uPrev);
    uPrev3 = &(*uPrev2);
    switch (bcx) {
        case Dirichlet: {
            this->DirichletEnforce2Dx();
            break;
        }
        case DirNeu: {
            this->DirichletNeumannEnforce2Dx();
            break;
        }
        case NeuDir: {
            this->NeumannDirichletEnforce2Dx();
            break;
        }
    }
    switch (bcy) {
        case Dirichlet: {
            this->DirichletEnforce2Dy();
            break;
        }
        case DirNeu: {
            this->DirichletNeumannEnforce2Dy();
            break;
        }
        case NeuDir: {
            this->NeumannDirichletEnforce2Dy();
            break;
        }
    }
    this->PrintResultsToFileIvp2DInit();
    this->PrintResultsToFileIvp2D(currentTime);
    currentTime += (*timeStep);
    switch (equationType) {
        case Assembly::AssemblyObject::EquationType
        ::UnsteadyDiffusion:
        case Assembly::AssemblyObject::EquationType
        ::UnsteadyAdvection:
        case Assembly::AssemblyObject::EquationType
        ::UnsteadyAdvectionDiffusion:
        case Assembly::AssemblyObject::EquationType
        ::UnsteadyInviscidBurger: {
            (*u) = (*uPrev) + Hsp * (*uPrev);
            break;
        }
    }
    (*uPrev2) = (*uPrev);
    (*uPrev) = (*u);
    switch (bcx) {
        case Dirichlet: {
            this->DirichletEnforce2Dx();
            break;
        }
        case DirNeu: {
            this->DirichletNeumannEnforce2Dx();
            break;
        }
        case NeuDir: {
            this->NeumannDirichletEnforce2Dx();
            break;
        }
    }
    switch (bcy) {
        case Dirichlet: {
            this->DirichletEnforce2Dy();
            break;
        }
        case DirNeu: {
            this->DirichletNeumannEnforce2Dy();
            break;
        }
        case NeuDir: {
            this->NeumannDirichletEnforce2Dy();
            break;
        }
    }
    this->PrintResultsToFileIvp2D(currentTime);

    currentTime += (*timeStep);
    switch (equationType) {
        case Assembly::AssemblyObject::EquationType
        ::UnsteadyDiffusion:
        case Assembly::AssemblyObject::EquationType
        ::UnsteadyAdvection:
        case Assembly::AssemblyObject::EquationType
        ::UnsteadyAdvectionDiffusion:
        case Assembly::AssemblyObject::EquationType
        ::UnsteadyInviscidBurger: {
            (*u) = (*uPrev) + (-1./2) * Hsp * (*uPrev2)
                + (3./2) * Hsp * (*uPrev);
            break;
        }
    }
    (*uPrev3) = (*uPrev2);
    (*uPrev2) = (*uPrev);
    (*uPrev) = (*u);
    switch (bcx) {
        case Dirichlet: {
            this->DirichletEnforce2Dx();
            break;
        }
        case DirNeu: {
            this->DirichletNeumannEnforce2Dx();
            break;
        }
        case NeuDir: {
            this->NeumannDirichletEnforce2Dx();
            break;
        }
    }
    switch (bcy) {
        case Dirichlet: {
            this->DirichletEnforce2Dy();
            break;
        }
        case DirNeu: {
            this->DirichletNeumannEnforce2Dy();
            break;
        }
        case NeuDir: {
            this->NeumannDirichletEnforce2Dy();
            break;
        }
    }
    this->PrintResultsToFileIvp2D(currentTime);
    currentTime += (*timeStep);

    for (int i = 0; i < steps - 3; i++) {
        if (i > 0) {
            switch (bcx) {
                case Dirichlet: {
                    this->DirichletEnforce2Dx();
                    break;
                }
                case DirNeu: {
                    this->DirichletNeumannEnforce2Dx();
                    break;
                }
                case NeuDir: {
                    this->NeumannDirichletEnforce2Dx();
                    break;
                }
            }
            switch (bcy) {
                case Dirichlet: {
                    this->DirichletEnforce2Dy();
                    break;
                }
                case DirNeu: {
                    this->DirichletNeumannEnforce2Dy();
                    break;
                }
                case NeuDir: {
                    this->NeumannDirichletEnforce2Dy();
                    break;
                }
            }
        }
        this->PrintResultsToFileIvp2D(currentTime);
        switch (equationType) {
            case Assembly::AssemblyObject::EquationType
            ::UnsteadyDiffusion:
            case Assembly::AssemblyObject::EquationType
            ::UnsteadyAdvection:
            case Assembly::AssemblyObject::EquationType
            ::UnsteadyAdvectionDiffusion:
            case Assembly::AssemblyObject::EquationType
            ::UnsteadyInviscidBurger: {
                (*u) = (*uPrev) + (5./12) * Hsp * (*uPrev3) +
                    (-4./3) * Hsp * (*uPrev2) + (23./12) * Hsp * (*uPrev);
                break;
            }
        }
        (*uPrev3) = (*uPrev2);
        (*uPrev2) = (*uPrev);
        (*uPrev) = (*u);
        currentTime += (*timeStep);
    }
    this->PrintResultsToFileIvp2DFinalize();
}

/*
    Integrates in time using the adams moulton method

    @param -
    @return -
    We work with pointers to the matrices and vectors.
*/
void Solver::SolverObject::AdamsMoultonSolver2D(void) {

    SparseMatrix<double, RowMajor> I((*ut0).size(), (*ut0).size());
    SparseMatrix<double, RowMajor> Atemp((*ut0).size(), (*ut0).size());
    SparseMatrix<double, RowMajor> *AtempPtr;
    VectorXd utemp((*ut0).size());
    VectorXd *utempPtr;
    I.setIdentity();
    Hsp = GetTimeStepMatrix2D();

    VectorXd *uPrev;
    VectorXd *uPrev2;
    VectorXd *uPrev3;
    double currentTime = 0.;
    (*u) = (*ut0);
    uPrev = &(*u);
    uPrev2 = &(*uPrev);
    uPrev3 = &(*uPrev2);
    switch (bcx) {
        case Dirichlet: {
            this->DirichletEnforce2Dx();
            break;
        }
        case DirNeu: {
            this->DirichletNeumannEnforce2Dx();
            break;
        }
        case NeuDir: {
            this->NeumannDirichletEnforce2Dx();
            break;
        }
    }
    switch (bcy) {
        case Dirichlet: {
            this->DirichletEnforce2Dy();
            break;
        }
        case DirNeu: {
            this->DirichletNeumannEnforce2Dy();
            break;
        }
        case NeuDir: {
            this->NeumannDirichletEnforce2Dy();
            break;
        }
    }
    this->PrintResultsToFileIvp2DInit();
    this->PrintResultsToFileIvp2D(currentTime);
    currentTime += (*timeStep);
    switch (equationType) {
        case Assembly::AssemblyObject::EquationType
        ::UnsteadyDiffusion:
        case Assembly::AssemblyObject::EquationType
        ::UnsteadyAdvection:
        case Assembly::AssemblyObject::EquationType
        ::UnsteadyAdvectionDiffusion:
        case Assembly::AssemblyObject::EquationType
        ::UnsteadyInviscidBurger: {
            if (executionStr->compare(std::string("serial")) == 0) {
                GMRES< SparseMatrix<double, RowMajor> > solver1(I - Hsp);
                *u = solver1.solve((*uPrev));
                (*uPrev) = (*u);
                GMRES< SparseMatrix<double, RowMajor> > solver2(I - (1./2) * Hsp);
                *u = solver2.solve((*uPrev) + (1./2) * Hsp * (*uPrev));
            }else if(executionStr->compare(std::string("parallel")) == 0) {
                Atemp = I - Hsp;
                AtempPtr = &Atemp;
                IterativeSolverPetsc(AtempPtr, uPrev, u, cores);
                (*uPrev) = (*u);
                Atemp = I - (1./2) * Hsp;
                AtempPtr = &Atemp;
                utemp = (*uPrev) + (1./2) * Hsp * (*uPrev);
                utempPtr = &utemp;
                IterativeSolverPetsc(AtempPtr, utempPtr, u, cores);
            }
            break;
        }
    }
    (*uPrev2) = (*uPrev);
    (*uPrev) = (*u);
    switch (bcx) {
        case Dirichlet: {
            this->DirichletEnforce2Dx();
            break;
        }
        case DirNeu: {
            this->DirichletNeumannEnforce2Dx();
            break;
        }
        case NeuDir: {
            this->NeumannDirichletEnforce2Dx();
            break;
        }
    }
    switch (bcy) {
        case Dirichlet: {
            this->DirichletEnforce2Dy();
            break;
        }
        case DirNeu: {
            this->DirichletNeumannEnforce2Dy();
            break;
        }
        case NeuDir: {
            this->NeumannDirichletEnforce2Dy();
            break;
        }
    }
    this->PrintResultsToFileIvp2D(currentTime);

    currentTime += (*timeStep);
    switch (equationType) {
        case Assembly::AssemblyObject::EquationType
        ::UnsteadyDiffusion:
        case Assembly::AssemblyObject::EquationType
        ::UnsteadyAdvection:
        case Assembly::AssemblyObject::EquationType
        ::UnsteadyAdvectionDiffusion:
        case Assembly::AssemblyObject::EquationType
        ::UnsteadyInviscidBurger: {
            if (executionStr->compare(std::string("serial")) == 0) {
                GMRES< SparseMatrix<double, RowMajor> > solver(I - (5./12) * Hsp);
                *u = solver.solve((*uPrev) - (1./12) * Hsp * (*uPrev2) +
                          (8./12) * Hsp * (*uPrev));
            }else if(executionStr->compare(std::string("parallel")) == 0) {
                Atemp = I - (5./12) * Hsp;
                AtempPtr = &Atemp;
                utemp = (*uPrev) - (1./12) * Hsp * (*uPrev2) +
                          (8./12) * Hsp * (*uPrev);
                utempPtr = &utemp;
                IterativeSolverPetsc(AtempPtr, utempPtr, u, cores);
            }
            break;
        }
    }
    (*uPrev3) = (*uPrev2);
    (*uPrev2) = (*uPrev);
    (*uPrev) = (*u);
    switch (bcx) {
        case Dirichlet: {
            this->DirichletEnforce2Dx();
            break;
        }
        case DirNeu: {
            this->DirichletNeumannEnforce2Dx();
            break;
        }
        case NeuDir: {
            this->NeumannDirichletEnforce2Dx();
            break;
        }
    }
    switch (bcy) {
        case Dirichlet: {
            this->DirichletEnforce2Dy();
            break;
        }
        case DirNeu: {
            this->DirichletNeumannEnforce2Dy();
            break;
        }
        case NeuDir: {
            this->NeumannDirichletEnforce2Dy();
            break;
        }
    }
    this->PrintResultsToFileIvp2D(currentTime);
    currentTime += (*timeStep);

    for (int i = 0; i < steps - 3; i++) {
        if (i > 0) {
            switch (bcx) {
                case Dirichlet: {
                    this->DirichletEnforce2Dx();
                    break;
                }
                case DirNeu: {
                    this->DirichletNeumannEnforce2Dx();
                    break;
                }
                case NeuDir: {
                    this->NeumannDirichletEnforce2Dx();
                    break;
                }
            }
            switch (bcy) {
                case Dirichlet: {
                    this->DirichletEnforce2Dy();
                    break;
                }
                case DirNeu: {
                    this->DirichletNeumannEnforce2Dy();
                    break;
                }
                case NeuDir: {
                    this->NeumannDirichletEnforce2Dy();
                    break;
                }
            }
        }
        this->PrintResultsToFileIvp2D(currentTime);
        switch (equationType) {
            case Assembly::AssemblyObject::EquationType
            ::UnsteadyDiffusion:
            case Assembly::AssemblyObject::EquationType
            ::UnsteadyAdvection:
            case Assembly::AssemblyObject::EquationType
            ::UnsteadyAdvectionDiffusion:
            case Assembly::AssemblyObject::EquationType
            ::UnsteadyInviscidBurger: {
                if (executionStr->compare(std::string("serial")) == 0) {
                    GMRES< SparseMatrix<double, RowMajor> > solver(I - (9./24) * Hsp);
                    *u = solver.solve((*uPrev) + (1./24) * Hsp * (*uPrev3) - (5./24) * Hsp *
                        (*uPrev2) + (19./24) * Hsp * (*uPrev));
                }else if(executionStr->compare(std::string("parallel")) == 0) {
                    Atemp = I - (9./24) * Hsp;
                    AtempPtr = &Atemp;
                    utemp = (*uPrev) + (1./24) * Hsp * (*uPrev3) - (5./24) * Hsp *
                                (*uPrev2) + (19./24) * Hsp * (*uPrev);
                    utempPtr = &utemp;
                    IterativeSolverPetsc(AtempPtr, utempPtr, u, cores);
                }
                break;
            }
        }
        (*uPrev3) = (*uPrev2);
        (*uPrev2) = (*uPrev);
        (*uPrev) = (*u);
        currentTime += (*timeStep);
    }
    this->PrintResultsToFileIvp2DFinalize();
}

/*
    Integrates in time using the third order runge-kutta method

    @param -
    @return -
    We work with pointers to the matrices and vectors.
*/
void Solver::SolverObject::RungeKutta3Solver2D(void) {

    Hsp = GetTimeStepMatrix2D();

    VectorXd *uPrev;
    VectorXd k1 = VectorXd::Zero((*ut0).size());
    VectorXd k3 = VectorXd::Zero((*ut0).size());
    double currentTime = 0.;
    (*u) = (*ut0);
    uPrev = &(*u);

   this->PrintResultsToFileIvp2DInit();

    for (int i = 0; i < steps; i++) {
        switch (bcx) {
            case Dirichlet: {
                this->DirichletEnforce2Dx();
                break;
            }
            case DirNeu: {
                this->DirichletNeumannEnforce2Dx();
                break;
            }
            case NeuDir: {
                this->NeumannDirichletEnforce2Dx();
                break;
            }
        }
        switch (bcy) {
            case Dirichlet: {
                this->DirichletEnforce2Dy();
                break;
            }
            case DirNeu: {
                this->DirichletNeumannEnforce2Dy();
                break;
            }
            case NeuDir: {
                this->NeumannDirichletEnforce2Dy();
                break;
            }
        }
        this->PrintResultsToFileIvp2D(currentTime);
        switch (equationType) {
            case Assembly::AssemblyObject::EquationType
            ::UnsteadyDiffusion:
            case Assembly::AssemblyObject::EquationType
            ::UnsteadyAdvection:
            case Assembly::AssemblyObject::EquationType
            ::UnsteadyAdvectionDiffusion:
            case Assembly::AssemblyObject::EquationType
            ::UnsteadyInviscidBurger: {
                k1 = Hsp * (*uPrev);
                k3 = Hsp * ((*uPrev) + (2./3) * ((*timeStep) * Hsp *
                    ((*uPrev) + (1./3) * k1)) );
                (*u) = (*uPrev) + (1./4) * (*timeStep) * (k1 + 3 * k3);
                break;
            }
        }
        (*uPrev) = (*u);
        currentTime += (*timeStep);
    }
    this->PrintResultsToFileIvp2DFinalize();
}

/*
    Integrates in time using the fourth order runge-kutta method

    @param -
    @return -
    We work with pointers to the matrices and vectors.
*/
void Solver::SolverObject::RungeKutta4Solver2D(void) {

    Hsp = GetTimeStepMatrix2D();

    VectorXd *uPrev;
    VectorXd k1 = VectorXd::Zero((*ut0).size());
    VectorXd k2 = VectorXd::Zero((*ut0).size());
    VectorXd k3 = VectorXd::Zero((*ut0).size());
    VectorXd k4 = VectorXd::Zero((*ut0).size());
    double currentTime = 0.;
    (*u) = (*ut0);
    uPrev = &(*u);

   this->PrintResultsToFileIvp2DInit();

    for (int i = 0; i < steps; i++) {
        switch (bcx) {
            case Dirichlet: {
                this->DirichletEnforce2Dx();
                break;
            }
            case DirNeu: {
                this->DirichletNeumannEnforce2Dx();
                break;
            }
            case NeuDir: {
                this->NeumannDirichletEnforce2Dx();
                break;
            }
        }
        switch (bcy) {
            case Dirichlet: {
                this->DirichletEnforce2Dy();
                break;
            }
            case DirNeu: {
                this->DirichletNeumannEnforce2Dy();
                break;
            }
            case NeuDir: {
                this->NeumannDirichletEnforce2Dy();
                break;
            }
        }
        this->PrintResultsToFileIvp2D(currentTime);
        switch (equationType) {
            case Assembly::AssemblyObject::EquationType
            ::UnsteadyDiffusion:
            case Assembly::AssemblyObject::EquationType
            ::UnsteadyAdvection:
            case Assembly::AssemblyObject::EquationType
            ::UnsteadyAdvectionDiffusion:
            case Assembly::AssemblyObject::EquationType
            ::UnsteadyInviscidBurger: {
                k1 = Hsp * (*uPrev);
                k2 = Hsp * ((*uPrev) + (1./2) * (*timeStep) * k1);
                k3 = Hsp * ((*uPrev) + (1./2) * (*timeStep) * k2);
                k4 = Hsp * ((*uPrev) + (*timeStep) * k3);
                (*u) = (*uPrev) + (1./6) * (*timeStep) * (k1 + 2 * k2
                    + 2 * k3 + k4);
                break;
            }
        }
        (*uPrev) = (*u);
        currentTime += (*timeStep);
    }
    this->PrintResultsToFileIvp2DFinalize();
}

/*
    Integrates in time using the low-storage (2N) third-order
    runge-kutta method

    @param -
    @return -
    We work with pointers to the matrices and vectors.
*/
void Solver::SolverObject::RungeKuttaLS3Solver2D(void) {

    Hsp = GetTimeStepMatrix2D();

    VectorXd uPrev = VectorXd::Zero((*ut0).size());
    VectorXd g = VectorXd::Zero((*ut0).size());
    double currentTime = 0.;
    (*u) = (*ut0);
    uPrev = (*u);

   this->PrintResultsToFileIvp2DInit();

    for (int i = 0; i < steps; i++) {
        switch (bcx) {
            case Dirichlet: {
                this->DirichletEnforce2Dx();
                break;
            }
            case DirNeu: {
                this->DirichletNeumannEnforce2Dx();
                break;
            }
            case NeuDir: {
                this->NeumannDirichletEnforce2Dx();
                break;
            }
        }
        switch (bcy) {
            case Dirichlet: {
                this->DirichletEnforce2Dy();
                break;
            }
            case DirNeu: {
                this->DirichletNeumannEnforce2Dy();
                break;
            }
            case NeuDir: {
                this->NeumannDirichletEnforce2Dy();
                break;
            }
        }
        this->PrintResultsToFileIvp2D(currentTime);
        switch (equationType) {
            case Assembly::AssemblyObject::EquationType
            ::UnsteadyDiffusion:
            case Assembly::AssemblyObject::EquationType
            ::UnsteadyAdvection:
            case Assembly::AssemblyObject::EquationType
            ::UnsteadyAdvectionDiffusion:
            case Assembly::AssemblyObject::EquationType
            ::UnsteadyInviscidBurger: {
                g = Hsp * uPrev;
                uPrev = uPrev + (1./3) * g;
                g = (-5./9) * g + Hsp * (uPrev + (1./3) * (*timeStep) * uPrev);
                uPrev = uPrev + (15./16) * g;
                g = (-153./128) * g + Hsp * (uPrev + (3./4) * (*timeStep) * uPrev);
                (*u) = uPrev + (8./15) * g;
                break;
            }
        }
        uPrev = (*u);
        currentTime += (*timeStep);
    }
    this->PrintResultsToFileIvp2DFinalize();
}

/*
    Get sparse the auxiliary time step matrix for the 2D IVP methods

    @param -
    @return the time step matrix Hsp
*/
SparseMatrix<double, RowMajor> Solver::SolverObject::GetTimeStepMatrix2D(void) {

    equationType = Assembly::AssemblyObject::
            DefineEquationTypeStatic(*equationTypeStr);

    switch (equationType) {
        case Assembly::AssemblyObject::EquationType::UnsteadyDiffusion: {
            Hsp = (*timeStep) * (*aDiffusion) * (*Asp);
            break;
        }
        case Assembly::AssemblyObject::EquationType::UnsteadyAdvection: {
            Hsp = - (*timeStep) * (*vAdvection) * (*Asp);
            break;
        }
        case Assembly::AssemblyObject::EquationType::UnsteadyAdvectionDiffusion: {
            Hsp = (*timeStep) * ((*aDiffusion) * (*Asp) - (*vAdvection) * (*Dsp));
            break;
        }
        case Assembly::AssemblyObject::EquationType::UnsteadyInviscidBurger: {
            Hsp = (*timeStep) * (*Asp);
            break;
        }
    }

    return Hsp;
}
