/*
    File: SolverPETSC.cc

    Purpose: Contains methods that are used to solve with PETSc
             for distributed systems
    @author: nikpfosk
*/

#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <stdio.h>

#include "../../include/Solver.h"

using namespace Eigen;

/*
    Solves the system of linear equations with iterative solver
    using PETSc for distributed systems

    @param -
    @return -
    We work with pointers to the matrices and vectors.
*/
void Solver::SolverObject::IterativeSolverPetsc(SparseMatrix<double, RowMajor> *A, VectorXd *b,
                                                VectorXd *u, int *cores) {
#ifdef _WIN32
    std::cout << "PETSc implementation not available on Windows. A UNIX system is required."
#endif
    SerializeMatrixAscii(*A, "../src/Solvers/PETSc/mat.dat");
    SerializeVectorAscii(*b, "../src/Solvers/PETSc/rhs.dat");
    int i;
    std::string command = "/usr/bin/mpirun -np "+ std::to_string(*cores) +" ./../src/Solvers/PETSc/SolverPETSc -Ain ../src/Solvers/PETSc/mat.dat "
                          "-rhs ../src/Solvers/PETSc/rhs.dat -no_shift -ksp_type gmres -pc_type hypre";
    i = system(command.c_str());
    if (i != 0) {
        std::cout << "\nPETSc threw an error." << std::endl;
    }
    DeserializeVectorAscii(*u, "../src/Solvers/PETSc/solution.dat");
    remove("../src/Solvers/PETSc/mat.dat");
    remove("../src/Solvers/PETSc/rhs.dat");
    remove("../src/Solvers/PETSc/solution.dat");

    return;
}
