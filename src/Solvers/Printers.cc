/*
    File: Printers.cc

    Purpose: Contains methods that print out the results to output files
    @author: nikpfosk
*/

#include <iostream>
#include <string>
#include <fstream>
#include <cctype>
#include <sstream>

#include "../../include/Solver.h"
#include "../../lib/unsupported/Eigen/SparseExtra"

using namespace Eigen;
bool is_number(const std::string& s);
/*
    Print the 1D BVP solution in a file.

    @param -
    @return -
    We work with pointers to the matrices and vectors.
*/
void Solver::SolverObject::PrintResultsToFileBvp1D(void) {
    std::ofstream outFile;
    outFile.open("../src/Postprocessing/output/outputBvp1D.dat");
    outFile << "# Problem solution" << std::endl;
    outFile << "# x\ty" << std::endl;
    for (int i = 0; i < (*u).size(); i++) {
        outFile << (*x)(i) << "\t" << (*u)(i) << std::endl;
    }
    outFile << "# EOF" << std::endl;
    outFile.close();
}

/*
    Print the 2D BVP solution in a file.

    @param -
    @return -
    We work with pointers to the matrices and vectors.
*/
void Solver::SolverObject::PrintResultsToFileBvp2D(void) {
    std::ofstream outFile;
    outFile.open("../src/Postprocessing/output/outputBvp2D.dat");
    outFile << "# Problem solution" << std::endl;
    outFile << "# x\ty\tu" << std::endl;
    for (int i = 0; i < (*x).size(); i++) {
		for (int j = 0; j < (*y).size(); j++) {
			outFile << (*x)(i) << "\t" << (*y)(j) << "\t"<<
				(*u)(i * (*x).size() + j) << std::endl;
		}
    }
    outFile << "# EOF" << std::endl;
    outFile.close();
}

/*
    Print the 1D IVP solution in a file (initialize).

    @param -
    @return -
    We work with pointers to the matrices and vectors.
*/
void Solver::SolverObject::PrintResultsToFileIvp1DInit(void) {
    std::ofstream outFile;
    outFile.open("../src/Postprocessing/output/outputIvp1D.dat");
    outFile << "# Problem solution in time" << std::endl;
    outFile << "# Pattern:" << std::endl;
    outFile << "# Time (sec) in single value lines." << std::endl;
    outFile << "# Solution (x, y) in following pairs." << std::endl;
}

/*
    Print the 1D IVP solution in a file

    @param -
    @return -
    We work with pointers to the matrices and vectors.
*/
void Solver::SolverObject::PrintResultsToFileIvp1D(double currentTime) {
    std::ofstream outFile;
    outFile.open("../src/Postprocessing/output/outputIvp1D.dat",
                 std::ios_base::app | std::ios_base::out);
    outFile << currentTime << std::endl;
    for (int i = 0; i < (*u).size(); i++) {
        outFile << (*x)(i) << "\t" << (*u)(i) << std::endl;
    }
}

/*
    Print the 1D IVP solution in a file (finalize).

    @param -
    @return -
    We work with pointers to the matrices and vectors.
*/
void Solver::SolverObject::PrintResultsToFileIvp1DFinalize(void) {
    std::ofstream outFile;
    outFile.open("../src/Postprocessing/output/outputIvp1D.dat",
                 std::ios_base::app | std::ios_base::out);
    outFile << "# EOF" << std::endl;
    outFile.close();
}

/*
    Print the 2D IVP solution in a file (initialize).

    @param -
    @return -
    We work with pointers to the matrices and vectors.
*/
void Solver::SolverObject::PrintResultsToFileIvp2DInit(void) {
    std::ofstream outFile;
    outFile.open("../src/Postprocessing/output/outputIvp2D.dat");
    outFile << "# Problem solution in time" << std::endl;
    outFile << "# Pattern:" << std::endl;
    outFile << "# Time (sec) in single value lines." << std::endl;
    outFile << "# Solution (x, y, u) in following triplets." << std::endl;
}

/*
    Print the 2D IVP solution in a file

    @param -
    @return -
    We work with pointers to the matrices and vectors.
*/
void Solver::SolverObject::PrintResultsToFileIvp2D(double currentTime) {
    std::ofstream outFile;
    outFile.open("../src/Postprocessing/output/outputIvp2D.dat",
                 std::ios_base::app | std::ios_base::out);
    outFile << currentTime << std::endl;
    for (int i = 0; i < (*x).size(); i++) {
		for (int j = 0; j < (*y).size(); j++) {
			outFile << (*x)(i) << "\t" << (*y)(j) << "\t"<<
				(*u)(i * (*x).size() + j) << std::endl;
		}
    }
}

/*
    Print the 2D IVP solution in a file (finalize).

    @param -
    @return -
    We work with pointers to the matrices and vectors.
*/
void Solver::SolverObject::PrintResultsToFileIvp2DFinalize(void) {
    std::ofstream outFile;
    outFile.open("../src/Postprocessing/output/outputIvp2D.dat",
                 std::ios_base::app | std::ios_base::out);
    outFile << "# EOF" << std::endl;
    outFile.close();
}

/*
    Serialize matrix into an ascii file

    @param -
    @return -
    We work with pointers to the matrices and vectors.
*/
void Solver::SolverObject::SerializeMatrixAscii(SparseMatrix<double, RowMajor> A,
    std::string filename) {

	saveMarket(A, filename);

}

/*
    Serialize vector into an ascii file

    @param -
    @return -
    We work with pointers to the matrices and vectors.
*/
void Solver::SolverObject::SerializeVectorAscii(VectorXd v, std::string filename) {
    std::ofstream outFile;
    outFile.open(filename, std::ios_base::out);
    for (int i = 0; i < v.size(); i++) {
        outFile << i << " " << v(i) << std::endl;
    }
}

/*
    Deserialize vector from ascii file

    @param -
    @return -
    We work with pointers to the matrices and vectors.
*/
void Solver::SolverObject::DeserializeVectorAscii(VectorXd &v, std::string filename) {
    std::ifstream inFile;
	int offset = 7;
	std::string str;
    inFile.open(filename, std::ios_base::in);
    for (int i = 0; i < (v).size() + offset; i++) {
		if (i < offset)
			inFile >> str;
		else{
			inFile >> str;
            (v)(i-offset) = std::stod(str);
		}
	}
}

bool is_number(const std::string& s) {
	return !s.empty() && s.find_first_not_of("0123456789") == std::string::npos;
}
