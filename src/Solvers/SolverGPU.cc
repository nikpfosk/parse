/*
    File: SolverGPU.cc

    Purpose: Contains methods that are used to solve on GPUs
    @author: nikpfosk
*/

#include <iostream>
#include <string>
#include <fstream>

#include "../../include/Solver.h"
//#include <../../lib/cusp/csr_matrix.h>
//#include <../../lib/cusp/monitor.h>
//#include <../../lib/cusp/krylov/gmres.h>

using namespace Eigen;
