/*
    File: Solver1D.cc

    Purpose: Contains methods that are used to solve 1D problems
    @author: nikpfosk
*/

#include <iostream>
#include <string>
#include <fstream>

#include "../../include/Solver.h"

using namespace Eigen;

/*
    After getting solution of system, apply the dirichlet BC.

    @param -
    @return -
    We work with pointers to the matrices and vectors.
*/
void Solver::SolverObject::DirichletEnforce1D(void) {
    int N = (*u).size();
    VectorXd utemp = VectorXd::Zero(N+2);

    // Both left and right Dirichlet BC are zero
    utemp.segment(1, N) = *u;
    *u = utemp;

    if (*bc0x == 0 && *bclx != 0) {  // right BC is not zero
        for (int i = 0; i < (*u).size(); i++) {
            (*u)(i) = (*u)(i) + (*bclx * ((*x)(i) - *x0)) / (*xl - *x0);
        }
    } else if (*bc0x != 0 && *bclx == 0) {  // left BC is not zero
        for (int i = 0; i < (*u).size(); i++) {
            (*u)(i) = (*u)(i) + (*bc0x * ((*x)(i) - *xl)) / (*x0 - *xl);
        }
    } else if (*bc0x != 0 && *bclx != 0) {  // left and right BC are not zero
        for (int i = 0; i < (*u).size(); i++) {
            (*u)(i) = (*u)(i) + ((*bc0x - *bclx)/(*x0 - *xl)) * (*x)(i) +
                (*bc0x - ((*bc0x - *bclx)/(*x0 - *xl)) * *x0);
        }
    }
}

/*
    After getting solution of system, apply the dirichlet-neumann BC.

    @param -
    @return -
    We work with pointers to the matrices and vectors.
*/
void Solver::SolverObject::DirichletNeumannEnforce1D(void) {
    int N = (*u).size();
    VectorXd utemp = VectorXd::Zero(N);

    // Both left and right Dirichlet BC are zero
    utemp.segment(0, N) = *u;
    *u = utemp;

    if (*bc0x != 0) {  // left BC is not zero
        for (int i = 0; i < (*u).size(); i++) {
            (*u)(i) = (*u)(i) + (*bc0x * ((*x)(i) - *xl)) / (*x0 - *xl);
        }
    }
}

/*
    After getting solution of system, apply the neumann-dirichlet BC.

    @param -
    @return -
    We work with pointers to the matrices and vectors.
*/
void Solver::SolverObject::NeumannDirichletEnforce1D(void) {
    int N = (*u).size();
    VectorXd utemp = VectorXd::Zero(N+1);

    // Both left and right Dirichlet BC are zero
    utemp.segment(1, N) = *u;
    *u = utemp;

    if (*bclx != 0) {  // right BC is not zero
        for (int i = 0; i < (*u).size(); i++) {
            (*u)(i) = (*u)(i) + (*bclx * ((*x)(i) - *x0)) / (*xl - *x0);
        }
    }
}

/*
    Solves the system of linear equations with direct Householder QR

    @param -
    @return -
    We work with pointers to the matrices and vectors.
*/
void Solver::SolverObject::DirectSolver(void) {
    if (std::abs((*A).determinant()) < 1.e-03) {
        std::cout << "Determinant of system matrix is: " << (*A).determinant()
        << ". Cannot solve system.\n" << std::endl;
        return;
    }
    *u = (*A).colPivHouseholderQr().solve(*b);
    std::cout << "Solving with direct Householder QR." << std::endl;
    std::cout << "Error:\t" << ((*A)*(*u) - (*b)).norm() << std::endl;

/*
     Eigen::BiCGSTAB<Eigen::MatrixXd>  BCGST;
     BCGST.compute(*A);
     *u = BCGST.solve(*b);
     std::cout << "Here is the Vector x using BICGSTAB :\n"<< *u << std::endl;
     std::cout << "Iterations :\n"<< BCGST.iterations() << std::endl;
*/
/*
    Eigen::LLT<Eigen::MatrixXd> lltOfA(*A);  // Check if A is SPD
    if (lltOfA.info() == Eigen::NumericalIssue) {
        std::cout << "Non-SPD system marix\n" << std::endl;
        std::cout << "Proceeding to solve with serial BiCGSTAB\n" << std::endl;

    } else {
        std::cout << "SPD system marix\n" << std::endl;
        std::cout << "Proceeding to solve with serial CG\n" << std::endl;

        Eigen::ConjugateGradient<SparseMatrix<double, RowMajor>, Lower|Upper> cg;
        //cg.compute(A);
        //x = cg.solve(b);
        //std::cout << "#iterations:     " << cg.iterations() << std::endl;
        //std::cout << "estimated error: " << cg.error()      << std::endl;
        // update b, and solve again
        //x = cg.solve(b);
    }
    */
}

/*
    Solves the system of linear equations in 1D Initial Value Problems (IVP)

    @param -
    @return -
    We work with pointers to the matrices and vectors.
*/
void Solver::SolverObject::DirectTimeSolver(void) {
    TimeIntegration ti;
    if ((*timeIntegration) == "forwardEuler") {
        ti = TimeIntegration::ForwardEuler;
    } else if ((*timeIntegration) == "backwardEuler") {
        ti = TimeIntegration::BackwardEuler;
    } else if ((*timeIntegration) == "leapfrog") {
        ti = TimeIntegration::Leapfrog;
    } else if ((*timeIntegration) == "adamsBashforth") {
        ti = TimeIntegration::AdamsBashforth;
    } else if ((*timeIntegration) == "adamsMoulton") {
        ti = TimeIntegration::AdamsMoulton;
    } else if ((*timeIntegration) == "rungeKutta3") {
        ti = TimeIntegration::RungeKutta3;
    } else if ((*timeIntegration) == "rungeKutta4") {
        ti = TimeIntegration::RungeKutta4;
    } else if ((*timeIntegration) == "rungeKuttaLS3") {
        ti = TimeIntegration::RungeKuttaLS3;
    } else {
        std::cout << "ERROR: Not valid time integration type." << std::endl;
        throw std::exception();
    }

    switch (ti) {
        case ForwardEuler: {
            this->ForwardEulerSolver1D();
            break;
        }
        case BackwardEuler: {
            this->BackwardEulerSolver1D();
            break;
        }
        case Leapfrog: {
            this->LeapfrogSolver1D();
            break;
        }
        case AdamsBashforth: {
            this->AdamsBashforthSolver1D();
            break;
        }
        case AdamsMoulton: {
            this->AdamsMoultonSolver1D();
            break;
        }
        case RungeKutta3: {
            this->RungeKutta3Solver1D();
            break;
        }
        case RungeKutta4: {
            this->RungeKutta4Solver1D();
            break;
        }
        case RungeKuttaLS3: {
            this->RungeKuttaLS3Solver1D();
            break;
        }
    }
}

/*
    Integrates in time using the forward euler method

    @param -
    @return -
    We work with pointers to the matrices and vectors.
*/
void Solver::SolverObject::ForwardEulerSolver1D(void) {

    H = GetTimeStepMatrix();

    VectorXd uPrev = VectorXd::Zero((*u0).size());
    VectorXd uPrevSquare = VectorXd::Zero((*u0).size());
    double currentTime = 0.;
    (*u) = (*ut0);
    uPrev = (*u);

   this->PrintResultsToFileIvp1DInit();

    for (int i = 0; i < steps; i++) {
        switch (bcx) {
            case Dirichlet: {
                this->DirichletEnforce1D();
                break;
            }
            case DirNeu: {
                this->DirichletNeumannEnforce1D();
                break;
            }
            case NeuDir: {
                this->NeumannDirichletEnforce1D();
                break;
            }
        }
        this->PrintResultsToFileIvp1D(currentTime);
        switch (equationType) {
            case Assembly::AssemblyObject::EquationType
            ::UnsteadyDiffusion:
            case Assembly::AssemblyObject::EquationType
            ::UnsteadyAdvection:
            case Assembly::AssemblyObject::EquationType
            ::UnsteadyAdvectionDiffusion: {
                (*u) = uPrev + H * uPrev;
                break;
            }
            case Assembly::AssemblyObject::EquationType
            ::UnsteadyInviscidBurger: {
                (*u) = uPrev - H * (0.5 * uPrev.cwiseProduct(uPrev));
            }
        }
        uPrev = (*u);
        currentTime += (*timeStep);
    }
    this->PrintResultsToFileIvp1DFinalize();
}

/*
    Integrates in time using the backward euler method

    @param -
    @return -
    We work with pointers to the matrices and vectors.
*/
void Solver::SolverObject::BackwardEulerSolver1D(void) {

    MatrixXd I = MatrixXd::Identity((*A).rows(), (*A).cols());
    H = GetTimeStepMatrix();

    VectorXd uPrev = VectorXd::Zero((*u0).size());
    double currentTime = 0.;
    (*u) = (*ut0);
    uPrev = (*u);

    this->PrintResultsToFileIvp1DInit();

    for (int i = 0; i < steps; i++) {
        switch (bcx) {
            case Dirichlet: {
                this->DirichletEnforce1D();
                break;
            }
            case DirNeu: {
                this->DirichletNeumannEnforce1D();
                break;
            }
            case NeuDir: {
                this->NeumannDirichletEnforce1D();
                break;
            }
        }
        this->PrintResultsToFileIvp1D(currentTime);
        switch (equationType) {
            case Assembly::AssemblyObject::EquationType
            ::UnsteadyDiffusion:
            case Assembly::AssemblyObject::EquationType
            ::UnsteadyAdvection:
            case Assembly::AssemblyObject::EquationType
            ::UnsteadyAdvectionDiffusion:
            case Assembly::AssemblyObject::EquationType
            ::UnsteadyInviscidBurger: {
                (*u) = (I - H).colPivHouseholderQr().solve(uPrev);
                break;
            }
        }
        uPrev = (*u);
        currentTime += (*timeStep);
    }
    this->PrintResultsToFileIvp1DFinalize();
}

/*
    Integrates in time using the leapfrog method

    @param -
    @return -
    We work with pointers to the matrices and vectors.
*/
void Solver::SolverObject::LeapfrogSolver1D(void) {

    H = GetTimeStepMatrix();

    VectorXd uPrev = VectorXd::Zero((*u0).size());
    VectorXd uPrev2 = VectorXd::Zero((*u0).size());
    double currentTime = 0.;
    (*u) = (*ut0);
    uPrev = (*u);
    uPrev2 = uPrev;
    switch (bcx) {
        case Dirichlet: {
            this->DirichletEnforce1D();
            break;
        }
        case DirNeu: {
            this->DirichletNeumannEnforce1D();
            break;
        }
        case NeuDir: {
            this->NeumannDirichletEnforce1D();
            break;
        }
    }
    this->PrintResultsToFileIvp1DInit();
    this->PrintResultsToFileIvp1D(currentTime);
    currentTime += (*timeStep);
    switch (equationType) {
        case Assembly::AssemblyObject::EquationType
        ::UnsteadyDiffusion:
        case Assembly::AssemblyObject::EquationType
        ::UnsteadyAdvection:
        case Assembly::AssemblyObject::EquationType
        ::UnsteadyAdvectionDiffusion:
        case Assembly::AssemblyObject::EquationType
        ::UnsteadyInviscidBurger: {
            (*u) = uPrev + H * uPrev;
            break;
        }
    }
    uPrev = (*u);
    switch (bcx) {
        case Dirichlet: {
            this->DirichletEnforce1D();
            break;
        }
        case DirNeu: {
            this->DirichletNeumannEnforce1D();
            break;
        }
        case NeuDir: {
            this->NeumannDirichletEnforce1D();
            break;
        }
    }
    this->PrintResultsToFileIvp1D(currentTime);
    currentTime += (*timeStep);

    for (int i = 0; i < steps - 2; i++) {
        if (i > 0) {
            switch (bcx) {
                case Dirichlet: {
                    this->DirichletEnforce1D();
                    break;
                }
                case DirNeu: {
                    this->DirichletNeumannEnforce1D();
                    break;
                }
                case NeuDir: {
                    this->NeumannDirichletEnforce1D();
                    break;
                }
            }
        }
        this->PrintResultsToFileIvp1D(currentTime);
        switch (equationType) {
            case Assembly::AssemblyObject::EquationType
            ::UnsteadyDiffusion:
            case Assembly::AssemblyObject::EquationType
            ::UnsteadyAdvection:
            case Assembly::AssemblyObject::EquationType
            ::UnsteadyAdvectionDiffusion:
            case Assembly::AssemblyObject::EquationType
            ::UnsteadyInviscidBurger: {
                (*u) = uPrev2 + 2 * H * uPrev;
                break;
            }
        }
        uPrev2 = uPrev;
        uPrev = (*u);
        currentTime += (*timeStep);
    }
    this->PrintResultsToFileIvp1DFinalize();
}

/*
    Integrates in time using the adams bashfort method

    @param -
    @return -
    We work with pointers to the matrices and vectors.
*/
void Solver::SolverObject::AdamsBashforthSolver1D(void) {

    H = GetTimeStepMatrix();

    VectorXd uPrev = VectorXd::Zero((*u0).size());
    VectorXd uPrev2 = VectorXd::Zero((*u0).size());
    VectorXd uPrev3 = VectorXd::Zero((*u0).size());
    double currentTime = 0.;
    (*u) = (*ut0);
    uPrev = (*u);
    uPrev2 = uPrev;
    uPrev3 = uPrev2;
    switch (bcx) {
        case Dirichlet: {
            this->DirichletEnforce1D();
            break;
        }
        case DirNeu: {
            this->DirichletNeumannEnforce1D();
            break;
        }
        case NeuDir: {
            this->NeumannDirichletEnforce1D();
            break;
        }
    }
    this->PrintResultsToFileIvp1DInit();
    this->PrintResultsToFileIvp1D(currentTime);
    currentTime += (*timeStep);
    switch (equationType) {
        case Assembly::AssemblyObject::EquationType
        ::UnsteadyDiffusion:
        case Assembly::AssemblyObject::EquationType
        ::UnsteadyAdvection:
        case Assembly::AssemblyObject::EquationType
        ::UnsteadyAdvectionDiffusion:
        case Assembly::AssemblyObject::EquationType
        ::UnsteadyInviscidBurger: {
            (*u) = uPrev + H * uPrev;
            break;
        }
    }
    uPrev2 = uPrev;
    uPrev = (*u);
    switch (bcx) {
        case Dirichlet: {
            this->DirichletEnforce1D();
            break;
        }
        case DirNeu: {
            this->DirichletNeumannEnforce1D();
            break;
        }
        case NeuDir: {
            this->NeumannDirichletEnforce1D();
            break;
        }
    }
    this->PrintResultsToFileIvp1D(currentTime);

    currentTime += (*timeStep);
    switch (equationType) {
        case Assembly::AssemblyObject::EquationType
        ::UnsteadyDiffusion:
        case Assembly::AssemblyObject::EquationType
        ::UnsteadyAdvection:
        case Assembly::AssemblyObject::EquationType
        ::UnsteadyAdvectionDiffusion:
        case Assembly::AssemblyObject::EquationType
        ::UnsteadyInviscidBurger: {
            (*u) = uPrev + (-1./2) * H * uPrev2 + (3./2) * H * uPrev;
            break;
        }
    }
    uPrev3 = uPrev2;
    uPrev2 = uPrev;
    uPrev = (*u);
    switch (bcx) {
        case Dirichlet: {
            this->DirichletEnforce1D();
            break;
        }
        case DirNeu: {
            this->DirichletNeumannEnforce1D();
            break;
        }
        case NeuDir: {
            this->NeumannDirichletEnforce1D();
            break;
        }
    }
    this->PrintResultsToFileIvp1D(currentTime);
    currentTime += (*timeStep);

    for (int i = 0; i < steps - 3; i++) {
        if (i > 0) {
            switch (bcx) {
                case Dirichlet: {
                    this->DirichletEnforce1D();
                    break;
                }
                case DirNeu: {
                    this->DirichletNeumannEnforce1D();
                    break;
                }
                case NeuDir: {
                    this->NeumannDirichletEnforce1D();
                    break;
                }
            }
        }
        this->PrintResultsToFileIvp1D(currentTime);
        switch (equationType) {
            case Assembly::AssemblyObject::EquationType
            ::UnsteadyDiffusion:
            case Assembly::AssemblyObject::EquationType
            ::UnsteadyAdvection:
            case Assembly::AssemblyObject::EquationType
            ::UnsteadyAdvectionDiffusion:
            case Assembly::AssemblyObject::EquationType
            ::UnsteadyInviscidBurger: {
                (*u) = uPrev + (5./12) * H * uPrev3 +
                    (-4./3) * H * uPrev2 + (23./12) * H * uPrev;
                break;
            }
        }
        uPrev3 = uPrev2;
        uPrev2 = uPrev;
        uPrev = (*u);
        currentTime += (*timeStep);
    }
    this->PrintResultsToFileIvp1DFinalize();
}

/*
    Integrates in time using the adams moulton method

    @param -
    @return -
    We work with pointers to the matrices and vectors.
*/
void Solver::SolverObject::AdamsMoultonSolver1D(void) {

    MatrixXd I = MatrixXd::Identity((*A).rows(), (*A).cols());
    H = GetTimeStepMatrix();

    VectorXd uPrev = VectorXd::Zero((*u0).size());
    VectorXd uPrev2 = VectorXd::Zero((*u0).size());
    VectorXd uPrev3 = VectorXd::Zero((*u0).size());
    double currentTime = 0.;
    (*u) = (*ut0);
    uPrev = (*u);
    uPrev2 = uPrev;
    uPrev3 = uPrev2;
    switch (bcx) {
        case Dirichlet: {
            this->DirichletEnforce1D();
            break;
        }
        case DirNeu: {
            this->DirichletNeumannEnforce1D();
            break;
        }
        case NeuDir: {
            this->NeumannDirichletEnforce1D();
            break;
        }
    }
    this->PrintResultsToFileIvp1DInit();
    this->PrintResultsToFileIvp1D(currentTime);
    currentTime += (*timeStep);
    switch (equationType) {
        case Assembly::AssemblyObject::EquationType
        ::UnsteadyDiffusion:
        case Assembly::AssemblyObject::EquationType
        ::UnsteadyAdvection:
        case Assembly::AssemblyObject::EquationType
        ::UnsteadyAdvectionDiffusion:
        case Assembly::AssemblyObject::EquationType
        ::UnsteadyInviscidBurger: {
            (*u) = (I - H).colPivHouseholderQr().solve(uPrev);
            uPrev = (*u);
            (*u) = (I - (1./2) * H).colPivHouseholderQr().solve(
                    uPrev + (1./2) * H * uPrev);
            break;
        }
    }
    uPrev2 = uPrev;
    uPrev = (*u);
    switch (bcx) {
        case Dirichlet: {
            this->DirichletEnforce1D();
            break;
        }
        case DirNeu: {
            this->DirichletNeumannEnforce1D();
            break;
        }
        case NeuDir: {
            this->NeumannDirichletEnforce1D();
            break;
        }
    }
    this->PrintResultsToFileIvp1D(currentTime);

    currentTime += (*timeStep);
    switch (equationType) {
        case Assembly::AssemblyObject::EquationType
        ::UnsteadyDiffusion:
        case Assembly::AssemblyObject::EquationType
        ::UnsteadyAdvection:
        case Assembly::AssemblyObject::EquationType
        ::UnsteadyAdvectionDiffusion:
        case Assembly::AssemblyObject::EquationType
        ::UnsteadyInviscidBurger: {
            (*u) = (I - (5./12) * H).colPivHouseholderQr().solve(
                    uPrev - (1./12) * H *uPrev2 + (8./12) * H * uPrev);
            break;
        }
    }
    uPrev3 = uPrev2;
    uPrev2 = uPrev;
    uPrev = (*u);
    switch (bcx) {
        case Dirichlet: {
            this->DirichletEnforce1D();
            break;
        }
        case DirNeu: {
            this->DirichletNeumannEnforce1D();
            break;
        }
        case NeuDir: {
            this->NeumannDirichletEnforce1D();
            break;
        }
    }
    this->PrintResultsToFileIvp1D(currentTime);
    currentTime += (*timeStep);

    for (int i = 0; i < steps - 3; i++) {
        if (i > 0) {
            switch (bcx) {
                case Dirichlet: {
                    this->DirichletEnforce1D();
                    break;
                }
                case DirNeu: {
                    this->DirichletNeumannEnforce1D();
                    break;
                }
                case NeuDir: {
                    this->NeumannDirichletEnforce1D();
                    break;
                }
            }
        }
        this->PrintResultsToFileIvp1D(currentTime);
        switch (equationType) {
            case Assembly::AssemblyObject::EquationType
            ::UnsteadyDiffusion:
            case Assembly::AssemblyObject::EquationType
            ::UnsteadyAdvection:
            case Assembly::AssemblyObject::EquationType
            ::UnsteadyAdvectionDiffusion:
            case Assembly::AssemblyObject::EquationType
            ::UnsteadyInviscidBurger: {
                (*u) = (I - (9./24) * H).colPivHouseholderQr().solve(
                        uPrev + (1./24) * H * uPrev3 - (5./24) * H *
                        uPrev2 + (19./24) * H * uPrev);
                break;
            }
        }
        uPrev3 = uPrev2;
        uPrev2 = uPrev;
        uPrev = (*u);
        currentTime += (*timeStep);
    }
    this->PrintResultsToFileIvp1DFinalize();
}

/*
    Integrates in time using the third order runge-kutta method

    @param -
    @return -
    We work with pointers to the matrices and vectors.
*/
void Solver::SolverObject::RungeKutta3Solver1D(void) {

    H = GetTimeStepMatrix();

    VectorXd uPrev = VectorXd::Zero((*u0).size());
    VectorXd k1 = VectorXd::Zero((*u0).size());
    VectorXd k3 = VectorXd::Zero((*u0).size());
    double currentTime = 0.;
    (*u) = (*ut0);
    uPrev = (*u);

   this->PrintResultsToFileIvp1DInit();

    for (int i = 0; i < steps; i++) {
        switch (bcx) {
            case Dirichlet: {
                this->DirichletEnforce1D();
                break;
            }
            case DirNeu: {
                this->DirichletNeumannEnforce1D();
                break;
            }
            case NeuDir: {
                this->NeumannDirichletEnforce1D();
                break;
            }
        }
        this->PrintResultsToFileIvp1D(currentTime);
        switch (equationType) {
            case Assembly::AssemblyObject::EquationType
            ::UnsteadyDiffusion:
            case Assembly::AssemblyObject::EquationType
            ::UnsteadyAdvection:
            case Assembly::AssemblyObject::EquationType
            ::UnsteadyAdvectionDiffusion:
            case Assembly::AssemblyObject::EquationType
            ::UnsteadyInviscidBurger: {
                k1 = H * uPrev;
                k3 = H * (uPrev + (2./3) * ((*timeStep) * H *
                    (uPrev + (1./3) * k1)) );
                (*u) = uPrev + (1./4) * (*timeStep) * (k1 + 3 * k3);
                break;
            }
        }
        uPrev = (*u);
        currentTime += (*timeStep);
    }
    this->PrintResultsToFileIvp1DFinalize();
}

/*
    Integrates in time using the fourth order runge-kutta method

    @param -
    @return -
    We work with pointers to the matrices and vectors.
*/
void Solver::SolverObject::RungeKutta4Solver1D(void) {

    H = GetTimeStepMatrix();

    VectorXd uPrev = VectorXd::Zero((*u0).size());
    VectorXd k1 = VectorXd::Zero((*u0).size());
    VectorXd k2 = VectorXd::Zero((*u0).size());
    VectorXd k3 = VectorXd::Zero((*u0).size());
    VectorXd k4 = VectorXd::Zero((*u0).size());
    double currentTime = 0.;
    (*u) = (*ut0);
    uPrev = (*u);

   this->PrintResultsToFileIvp1DInit();

    for (int i = 0; i < steps; i++) {
        switch (bcx) {
            case Dirichlet: {
                this->DirichletEnforce1D();
                break;
            }
            case DirNeu: {
                this->DirichletNeumannEnforce1D();
                break;
            }
            case NeuDir: {
                this->NeumannDirichletEnforce1D();
                break;
            }
        }
        this->PrintResultsToFileIvp1D(currentTime);
        switch (equationType) {
            case Assembly::AssemblyObject::EquationType
            ::UnsteadyDiffusion:
            case Assembly::AssemblyObject::EquationType
            ::UnsteadyAdvection:
            case Assembly::AssemblyObject::EquationType
            ::UnsteadyAdvectionDiffusion:
            case Assembly::AssemblyObject::EquationType
            ::UnsteadyInviscidBurger: {
                k1 = H * uPrev;
                k2 = H * (uPrev + (1./2) * (*timeStep) * k1);
                k3 = H * (uPrev + (1./2) * (*timeStep) * k2);
                k4 = H * (uPrev + (*timeStep) * k3);
                (*u) = uPrev + (1./6) * (*timeStep) * (k1 + 2 * k2
                    + 2 * k3 + k4);
                break;
            }
        }
        uPrev = (*u);
        currentTime += (*timeStep);
    }
    this->PrintResultsToFileIvp1DFinalize();
}

/*
    Integrates in time using the low-storage (2N) third-order
    runge-kutta method

    @param -
    @return -
    We work with pointers to the matrices and vectors.
*/
void Solver::SolverObject::RungeKuttaLS3Solver1D(void) {

    H = GetTimeStepMatrix();

    VectorXd uPrev = VectorXd::Zero((*u0).size());
    VectorXd g = VectorXd::Zero((*u0).size());
    double currentTime = 0.;
    (*u) = (*ut0);
    uPrev = (*u);

   this->PrintResultsToFileIvp1DInit();

    for (int i = 0; i < steps; i++) {
        switch (bcx) {
            case Dirichlet: {
                this->DirichletEnforce1D();
                break;
            }
            case DirNeu: {
                this->DirichletNeumannEnforce1D();
                break;
            }
            case NeuDir: {
                this->NeumannDirichletEnforce1D();
                break;
            }
        }
        this->PrintResultsToFileIvp1D(currentTime);
        switch (equationType) {
            case Assembly::AssemblyObject::EquationType
            ::UnsteadyDiffusion:
            case Assembly::AssemblyObject::EquationType
            ::UnsteadyAdvection:
            case Assembly::AssemblyObject::EquationType
            ::UnsteadyAdvectionDiffusion:
            case Assembly::AssemblyObject::EquationType
            ::UnsteadyInviscidBurger: {
                g = H * uPrev;
                uPrev = uPrev + (1./3) * g;
                g = (-5./9) * g + H * (uPrev + (1./3) * (*timeStep) * uPrev);
                uPrev = uPrev + (15./16) * g;
                g = (-153./128) * g + H * (uPrev + (3./4) * (*timeStep) * uPrev);
                (*u) = uPrev + (8./15) * g;
                break;
            }
        }
        uPrev = (*u);
        currentTime += (*timeStep);
    }
    this->PrintResultsToFileIvp1DFinalize();
}

/*
    Get the auxiliary time step matrix for the 1D IVP methods

    @param -
    @return the time step matrix H
*/
MatrixXd Solver::SolverObject::GetTimeStepMatrix(void) {

    equationType = Assembly::AssemblyObject::
            DefineEquationTypeStatic(*equationTypeStr);

    switch (equationType) {
        case Assembly::AssemblyObject::EquationType::UnsteadyDiffusion: {
            H = (*timeStep) * (*aDiffusion) * (*A);
            break;
        }
        case Assembly::AssemblyObject::EquationType::UnsteadyAdvection: {
            H = - (*timeStep) * (*vAdvection) * (*A);
            break;
        }
        case Assembly::AssemblyObject::EquationType::UnsteadyAdvectionDiffusion: {
            H = (*timeStep) * ((*aDiffusion) * (*A) - (*vAdvection) * (*D));
            break;
        }
        case Assembly::AssemblyObject::EquationType::UnsteadyInviscidBurger: {
            H = (*timeStep) * (*A);
            break;
        }
    }

    return H;
}
