import os
import csv
import sys
import warnings
import numpy as np
import matplotlib.pyplot as plt
from scipy import interpolate
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from scipy import interpolate
from matplotlib.ticker import LinearLocator, FormatStrFormatter

os.getcwd()
os.path.exists("../src/Postprocessing/output")
fpath = os.path.join("../src/Postprocessing/output", "outputBvp2D.dat")

N = int(sys.argv[1])
save = int(sys.argv[2])

if (save == 1):
    saveToFile = True
else:
    saveToFile = False


x = []
y = []
u = []
x0 = 0.
xl = 0.

with open(fpath, 'r') as f:
    reader = csv.reader(f, dialect='excel', delimiter='\t')
    for row in reader:
        if (row[0][0] != '#'):
            if (float(row[0]) not in x):
                x.append(float(row[0]))
            if (float(row[1]) not in y):
                y.append(float(row[1]))
            u.append(float(row[2]))

if (x[0] < x[-1]):
    x0 = x[0]
    xl = x[-1]
else:
    x0 = x[-1]
    xl = x[0]

if (y[0] < y[-1]):
    y0 = y[0]
    yl = y[-1]
else:
    y0 = y[-1]
    yl = y[0]

hx = (xl - x0) / (N - 1)
hy = (yl - y0) / (N - 1)
xxl = np.zeros(N)
yyl = np.zeros(N)
for i in range(N):
    xxl[i] = x0 + i * hx
    yyl[i] = y0 + i * hy

u = np.asarray(u);

xx, yy = np.meshgrid(xxl, yyl)
uu = np.reshape(u,(len(x),len(y)))

f = interpolate.interp2d(x, y, uu, kind='cubic')
uunew = f(xxl, yyl)

fig = plt.figure()
 
ax = fig.gca(projection='3d')
 
surf = ax.plot_surface(xx, yy, uunew, rstride=1, cstride=1, cmap=cm.coolwarm,
                       linewidth=0, antialiased=False)
 
ax.zaxis.set_major_locator(LinearLocator(10))
ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))
 
fig.colorbar(surf, shrink=0.5, aspect=5)

plt.grid(True)
plt.title('ParSE Visualizer - Solution')
plt.xlabel('x')
plt.ylabel('y')
if (saveToFile):
    plt.savefig('../src/Postprocessing/plots/solution.png')
 
plt.show()
