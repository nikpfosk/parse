import os
import csv
import math
import sys
import warnings
import platform
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import animation
from scipy import interpolate
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from scipy import interpolate
from matplotlib.ticker import LinearLocator, FormatStrFormatter
if (platform.system() == "Windows"):
    plt.rcParams['animation.ffmpeg_path'] = 'C:\\ffmpeg\\bin\\ffmpeg.exe'

os.getcwd()
os.path.exists("../src/Postprocessing/output")
fpath = os.path.join("../src/Postprocessing/output", "outputIvp2D.dat")
#os.path.exists("output")
#fpath = os.path.join("output", "outputIvp2D.dat")

Nx = int(sys.argv[1])
save = int(sys.argv[2])

if (save == 1):
    saveToFile = True
else:
    saveToFile = False

timeStep = []
x = []
y = []
u = []
uArray = []

umin = 0.
umax = 0.
x0 = 0.
xl = 0.
y0 = 0.
yl = 0.

xInfo = False
yInfo = False

with open(fpath, 'r') as f:
    reader = csv.reader(f, dialect='excel', delimiter='\t')
    for row in reader:
        if (row[0][0] != '#'):
                if (len(row) == 1):
                    if (len(u) > 0):
                        if (float(min(u)) < umin):
                            umin = float(min(u))
                        if (float(max(u)) > umax):
                            umax = float(max(u))
                        uArray.append(u)
                    if (len(x) > 0 and xInfo == False):
                        xInfo = True
                    if (len(y) > 0 and yInfo == False):
                        yInfo = True
                    u = []
                    timeStep.append(float(row[0]))
                if (len(row) == 3):
                    if (xInfo == False and float(row[0]) not in x):
                        x.append(float(row[0]))
                    if (yInfo == False and float(row[1]) not in y):
                        y.append(float(row[1]))
                    u.append(float(row[2]))
if (x[0] < x[-1]):
    x0 = x[0]
    xl = x[-1]
else:
    x0 = x[-1]
    xl = x[0]

if (y[0] < y[-1]):
    y0 = y[0]
    yl = y[-1]
else:
    y0 = y[-1]
    yl = y[0]


#umin = math.floor(umin)
#umax = math.ceil(umax)


x0 = math.floor(x0)
xl = math.ceil(xl)

hx = (xl - x0) / (Nx - 1)
hy = (yl - y0) / (Nx - 1)
xxl = np.zeros(Nx)
yyl = np.zeros(Nx)
for i in range(Nx):
    xxl[i] = x0 + i * hx
    yyl[i] = y0 + i * hy

# Set up plotting
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
plt.grid(True)
plt.title('ParSE Visualizer - Solution')
plt.xlabel('x')
plt.ylabel('y')
ax.set_xlim(x0, xl)
ax.set_ylim(y0, yl)
ax.set_zlim(umin, umax)
ax.zaxis.set_major_locator(LinearLocator(10))
ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))
textvar = "Time: "+str(timeStep[0])+" sec"
annotate=plt.annotate("[Time: "+str(timeStep[0])+" sec]", xy=(1, 1), xytext=(-128, -24), va='top',
             size=14, xycoords='axes fraction', textcoords='offset points', bbox=dict(boxstyle="round", fc="1.0"))
def init():
    ui = uArray[0] 
    ui = np.asarray(ui)

    xx, yy = np.meshgrid(xxl, yyl)

    uu = np.reshape(ui,(len(x),len(y)))

    f = interpolate.interp2d(x, y, uu, kind='cubic')
    uunew = f(xxl, yyl)
    ax.set_xlim(x0, xl)
    ax.set_ylim(y0, yl)
    ax.set_zlim(umin, umax)
    textvar = "Time: "+str(timeStep[0])+" sec"
    annotate=plt.annotate("[Time: "+str(timeStep[0])+" sec]", xy=(1, 1), xytext=(-128, -24), va='top',
                 size=14, xycoords='axes fraction', textcoords='offset points', bbox=dict(boxstyle="round", fc="1.0"))
    surf = ax.plot_surface(xx, yy, uunew, rstride=1, cstride=1, cmap=cm.coolwarm,
                           linewidth=0, antialiased=False)
    if (platform.system() == "Windows"):
        plt.ion()
    return fig,
# Animation function
def animate(i):
    ax.clear()
    ui = uArray[i] 
    ui = np.asarray(ui)

    xx, yy = np.meshgrid(xxl, yyl)

    uu = np.reshape(ui,(len(x),len(y)))

    f = interpolate.interp2d(x, y, uu, kind='cubic')
    uunew = f(xxl, yyl)
    plt.grid(True)
    plt.title('ParSE Visualizer - Solution')
    plt.xlabel('x')
    plt.ylabel('y')
    ax.set_xlim(x0, xl)
    ax.set_ylim(y0, yl)
    ax.set_zlim(umin, umax)
    surf = ax.plot_surface(xx, yy, uunew, rstride=1, cstride=1, cmap=cm.coolwarm,
                           linewidth=0, antialiased=False)
    textvar = "Time: "+str(timeStep[0])+" sec"
    annotate=plt.annotate("[Time: "+str(timeStep[0])+" sec]", xy=(1, 1), xytext=(-128, -24), va='top',
                 size=14, xycoords='axes fraction', textcoords='offset points', bbox=dict(boxstyle="round", fc="1.0"))
    annotate.set_text("[Time: "+str(timeStep[i])+" sec]")
    cf = surf
    return fig,
anim = animation.FuncAnimation(fig, animate, init_func = init, frames = len(timeStep)-1, interval = 50, blit=False)
if (saveToFile):
    anim.save('../src/Postprocessing/plots/solution_animation.mp4', writer = "ffmpeg", fps=50, extra_args=['-vcodec', 'libx264'])
plt.show()
