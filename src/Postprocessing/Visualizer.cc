/*
    File: Visualizer.cc

    Purpose: Contains methods that create and define the visualizer object
    @author: nikpfosk
*/

#include <iostream>
#include <stdlib.h>
#include <string>
#include <fstream>
#include <stdio.h>

#include "../../include/Assembler.h"
#include "../../include/Solver.h"
#include "../../include/Visualizer.h"
#include "../../include/InputHandlers.h"

using namespace Eigen;

/*
    Namespace: Visualizer
    Definitions for Visualizer
*/

Visualizer::VisualizerObject::VisualizerObject(const Assembly::AssemblyObject
    &obj) {
    problemTypeStr = &obj.problemTypeStr;
    interpolationNodes = &obj.interpolationNodes;
}

Visualizer::VisualizerObject::~VisualizerObject() {
}

/*
    Interface for the visualizer class.

    @param the visualizer object
    @return the modified visualizer object
*/
Visualizer::VisualizerObject Visualizer::VisualizerObject::
Visualizer(const Visualizer::VisualizerObject &obj) {
    std::cout << "\n****************************************\n" << std::endl;
    std::cout << "Visualizer process started." << std::endl;

    AppConfig::AppConfigObject appConfigObj;
    appConfigObj.SetAppConfigObjectFromFile(appConfigObj.pathToAppConfigFile);

    if (problemTypeStr->compare(std::string("BVP_1D")) == 0) {
        if (appConfigObj.plotSolution)
            obj.PlotResultsBvp1D(appConfigObj.saveSolutionFile);
    } else if (problemTypeStr->compare(std::string("IVP_1D")) == 0) {
        if (appConfigObj.plotSolution)
            obj.AnimateResultsIvp1D(appConfigObj.saveSolutionFile);
    } else if (problemTypeStr->compare(std::string("BVP_2D")) == 0) {
        if (appConfigObj.plotSolution)
            obj.PlotResultsBvp2D(appConfigObj.saveSolutionFile);
    } else if (problemTypeStr->compare(std::string("IVP_2D")) == 0) {
        if (appConfigObj.plotSolution)
            obj.AnimateResultsIvp2D(appConfigObj.saveSolutionFile);
    } else {
        std::cout << "ERROR: Not valid problem type." << std::endl;
        throw std::exception();
    }

    std::cout << "\n****************************************\n" << std::endl;
    return obj;
}

/*
    Plot the 1D solution by calling python script.

    @param -
    @return -
    We work with pointers to the matrices and vectors.
*/
void Visualizer::VisualizerObject::PlotResultsBvp1D(bool saveSolutionFile) const {
    int i;
    std::string Nx = std::to_string(*interpolationNodes);
    std::string saveSolutionFileStr = std::to_string(saveSolutionFile);
#ifdef _WIN32
    std::string command = "\"C:\\Program Files\\Anaconda3\\python.exe\" ../src/Postprocessing/PlotBvp1D.py " + Nx
            + " " + saveSolutionFileStr;
    i = system(command.c_str());
    if (i != 0) {
        std::cout << "\nAnaconda threw an error. Make sure you have anaconda installed in C:\\Program Files\\Anaconda3." << std::endl;
    }
#else
    std::string command = "python3 ../src/Postprocessing/PlotBvp1D.py " + Nx
            + " " + saveSolutionFileStr;
    i = system(command.c_str());
    if (i != 0) {
        std::cout << "\nPython 3 threw an error." << std::endl;
        std::cout << "\nTrying Python 2 for plotting." << std::endl;
        std::string command = "python2 ../src/Postprocessing/PlotBvp1D.py " + Nx
            + " " + saveSolutionFileStr;
        i = system(command.c_str());
    }
    if (i != 0) {
        std::cout << "\nPython 2 threw an error." << std::endl;
        std::cout << "Cannot generate plots." << std::endl;
    }
#endif
}

/*
    Plot the 2D solution by calling python script.

    @param -
    @return -
    We work with pointers to the matrices and vectors.
*/
void Visualizer::VisualizerObject::PlotResultsBvp2D(bool saveSolutionFile) const {
    int i;
    std::string Nx = std::to_string(*interpolationNodes);
    std::string saveSolutionFileStr = std::to_string(saveSolutionFile);
#ifdef _WIN32
    std::string command = "\"C:\\Program Files\\Anaconda3\\python.exe\" ../src/Postprocessing/PlotBvp2D.py " + Nx
            + " " + saveSolutionFileStr;
    i = system(command.c_str());
    if (i != 0) {
        std::cout << "\nAnaconda threw an error. Make sure you have anaconda installed in C:\\Program Files\\Anaconda3." << std::endl;
    }
#else
    std::string command = "python3 ../src/Postprocessing/PlotBvp2D.py " + Nx
            + " " + saveSolutionFileStr;
    i = system(command.c_str());
    if (i != 0) {
        std::cout << "\nPython 3 threw an error." << std::endl;
        std::cout << "\nTrying Python 2 for plotting." << std::endl;
        std::string command = "python2 ../src/Postprocessing/PlotBvp2D.py " + Nx
            + " " + saveSolutionFileStr;
        i = system(command.c_str());
    }
    if (i != 0) {
        std::cout << "\nPython 2 threw an error." << std::endl;
        std::cout << "Cannot generate plots." << std::endl;
    }
#endif
}

/*
    Animate the 1D solution by calling python script.

    @param -
    @return -
    We work with pointers to the matrices and vectors.
*/
void Visualizer::VisualizerObject::AnimateResultsIvp1D(bool saveSolutionFile) const {
    int i;
    std::string Nx = std::to_string(*interpolationNodes);
    std::string saveSolutionFileStr = std::to_string(saveSolutionFile);
#ifdef _WIN32
    std::string command = "\"C:\\Program Files\\Anaconda3\\python.exe\" ../src/Postprocessing/AnimateIvp1D.py " + Nx
            + " " + saveSolutionFileStr;
    i = system(command.c_str());
    if (i != 0) {
        std::cout << "\nAnaconda threw an error. Make sure you have anaconda installed in C:\\Program Files\\Anaconda3." << std::endl;
    }
#else
    std::string command = "python3 ../src/Postprocessing/AnimateIvp1D.py " + Nx
        + " " + saveSolutionFileStr;
    i = system(command.c_str());
    if (i != 0) {
        std::cout << "\nPython 3 threw an error." << std::endl;
        std::cout << "\nTrying Python 2 for plotting" << std::endl;
        std::string command = "python2 ../src/Postprocessing/AnimateIvp1D.py " + Nx
            + " " + saveSolutionFileStr;
        i = system(command.c_str());
    }
    if (i != 0) {
        std::cout << "\nPython 2 threw an error." << std::endl;
        std::cout << "Cannot generate plots." << std::endl;
    }
#endif
}

/*
    Animate the 2D solution by calling python script.

    @param -
    @return -
    We work with pointers to the matrices and vectors.
*/
void Visualizer::VisualizerObject::AnimateResultsIvp2D(bool saveSolutionFile) const {
    int i;
    std::string Nx = std::to_string(*interpolationNodes);
    std::string saveSolutionFileStr = std::to_string(saveSolutionFile);
#ifdef _WIN32
    std::string command = "\"C:\\Program Files\\Anaconda3\\python.exe\" ../src/Postprocessing/AnimateIvp2D.py " + Nx
            + " " + saveSolutionFileStr;
    i = system(command.c_str());
    if (i != 0) {
        std::cout << "\nAnaconda threw an error. Make sure you have anaconda installed in C:\\Program Files\\Anaconda3." << std::endl;
    }
#else
    std::string command = "python3 ../src/Postprocessing/AnimateIvp2D.py " + Nx
        + " " + saveSolutionFileStr;
    i = system(command.c_str());
    if (i != 0) {
        std::cout << "\nPython 3 threw an error." << std::endl;
        std::cout << "\nTrying Python 2 for plotting" << std::endl;
        std::string command = "python2 ../src/Postprocessing/AnimateIvp2D.py " + Nx
            + " " + saveSolutionFileStr;
        i = system(command.c_str());
    }
    if (i != 0) {
        std::cout << "\nPython 2 threw an error." << std::endl;
        std::cout << "Cannot generate plots." << std::endl;
    }
#endif
}
