import os
import csv
import math
import sys
import warnings
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import animation

os.getcwd()
os.path.exists("../src/Postprocessing/output")
fpath = os.path.join("../src/Postprocessing/output", "outputIvp1D.dat")
#os.path.exists("output")
#fpath = os.path.join("output", "outputIvp1D.dat")

Nx = int(sys.argv[1])
save = int(sys.argv[2])

if (save == 1):
    saveToFile = True
else:
    saveToFile = False

timeStep = []
x = []
u = []
uArray = []

umin = 0.
umax = 0.
x0 = 0.
xl = 0.

xInfo = False

with open(fpath, 'r') as f:
    reader = csv.reader(f, dialect='excel', delimiter='\t')
    for row in reader:
        if (row[0][0] != '#'):
                if (len(row) == 1):
                    if (len(u) > 0):
                        if (float(min(u)) < umin):
                            umin = float(min(u))
                        if (float(max(u)) > umax):
                            umax = float(max(u))
                        uArray.append(u)
                    if (len(x) > 0 and xInfo == False):
                        xInfo = True
                    u = []
                    timeStep.append(float(row[0]))
                if (len(row) == 2):
                    if (xInfo == False):
                        x.append(float(row[0]))
                    u.append(float(row[1]))

if (x[0] < x[-1]):
    x0 = x[0]
    xl = x[-1]
else:
    x0 = x[-1]
    xl = x[0]

umin = math.floor(umin)
umax = math.ceil(umax)

x0 = math.floor(x0)
xl = math.ceil(xl)

h = (xl - x0) / (Nx - 1)
xx = np.zeros(Nx)
for i in range(Nx):
    xx[i] = x0 + i * h

# Set up plotting
fig = plt.figure()
ax = plt.axes(xlim=(x0, xl), ylim=(int(umin), int(umax)))
ax.grid(True)
plt.title('ParSE Visualizer - Solution')
plt.xlabel('x')
plt.ylabel('u')
textvar = "Time: "+str(timeStep[0])+" sec"
annotate=plt.annotate("[Time: "+str(timeStep[0])+" sec]", xy=(1, 1), xytext=(-128, -24), va='top',
             size=14, xycoords='axes fraction', textcoords='offset points', bbox=dict(boxstyle="round", fc="1.0"))
line, = ax.plot([], [], lw=2)
def init():
    line.set_data([], [])
    return fig,
# Animation function
def animate(i):
    with warnings.catch_warnings():
        warnings.filterwarnings("ignore", category = np.RankWarning)
        try:
            uu = np.polyval(np.polyfit(np.asarray(x), np.asarray(uArray[i]), Nx), xx)
        except np.RankWarning:
            print ("Warning: Data not enough to fit interpolation.")
    line.set_data(xx, uu)
    annotate.set_text("[Time: "+str(timeStep[i])+" sec]")
    return fig,

anim = animation.FuncAnimation(fig, animate, init_func = init, frames = len(timeStep)-1, interval = 50, blit=False)
if (saveToFile):
    anim.save('../src/Postprocessing/plots/solution_animation.mp4', fps=30, extra_args=['-vcodec', 'libx264'])
plt.show()
