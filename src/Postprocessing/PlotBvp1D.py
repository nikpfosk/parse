import os
import csv
import sys
import warnings
import numpy as np
import matplotlib.pyplot as plt

os.getcwd()
os.path.exists("../src/Postprocessing/output")
fpath = os.path.join("../src/Postprocessing/output", "outputBvp1D.dat")

Nx = int(sys.argv[1])
save = int(sys.argv[2])

if (save == 1):
    saveToFile = True
else:
    saveToFile = False


x = []
u = []
x0 = 0.
xl = 0.

with open(fpath, 'r') as f:
    reader = csv.reader(f, dialect='excel', delimiter='\t')
    for row in reader:
        if (row[0][0] != '#'):
            x.append(float(row[0]))
            u.append(float(row[1]))

if (x[0] < x[-1]):
    x0 = x[0]
    xl = x[-1]
else:
    x0 = x[-1]
    xl = x[0]

h = (xl - x0) / (Nx - 1)
xx = np.zeros(Nx)
for i in range(Nx):
    xx[i] = x0 + i * h

with warnings.catch_warnings():
    warnings.filterwarnings("ignore", category = np.RankWarning)
    try:
        uu = np.polyval(np.polyfit(np.asarray(x), np.asarray(u), Nx), xx)
    except np.RankWarning:
        print ("Warning: Data not enough to fit interpolation.")

plt.plot(xx, uu)
plt.hold(True)
plt.grid(True)
plt.title('ParSE Visualizer - Solution')
plt.xlabel('x')
plt.ylabel('u')
if (saveToFile):
    plt.savefig('../src/Postprocessing/plots/solution.png')
plt.show()
