/*
    File: InputHandlers.cc

    Purpose: Contains methods that handle input parameters for the preprocessing. It takes
            care for parsing the input parameters from the input files and creates the
            problem setup objects that will be used in the later stages.

    @author: nikpfosk
*/

#include <iostream>
#include <string>
#include <fstream>

#include "../../include/InputHandlers.h"
#include "../../lib/Json/json.hpp"
#include "../../lib/Exprtk/exprtk.hpp"

using json = nlohmann::json;
using namespace Eigen;

/*
    Print the framework logo in console

    @param -
    @return -
*/
void initialize(void) {
    std::ifstream logoFile("../logo.txt");
    std::string line;
    std::cout << "\n" << std::endl;
    while (std::getline(logoFile, line)) {
        std::cout << line << std::endl;
    }
    logoFile.close();
}

/*
    Namespace: Input
    Definitions for setting objects from input parameters from the input file
*/

/*
    Get problem type from JSON input file

    @param the path to the JSON input file
    @return the problem type as a string
*/
std::string Input::Init::GetProblemType(std::string path) {
    std::ifstream   i(path);
    json j;
    i >> j;

    return j["problemType"][0];
}

/*
    Creates RHS vector from user's input

    @param rhsFunc is the RHS function and x0,xl the beginning and end
        of the interval
    @return the RHS vector b
*/
VectorXd Input::EquationParser::Parser1D(std::string func, VectorXd xv,
    int N) {
    VectorXd b(xv.size());
    typedef exprtk::symbol_table<double>    symbol_table_t;
    typedef exprtk::expression<double>      expression_t;
    typedef exprtk::parser<double>          parser_t;

    std::string expression_string = func;

    double x;

    symbol_table_t symbol_table;
    symbol_table.add_variable("x", x);
    symbol_table.add_constants();

    expression_t expression;
    expression.register_symbol_table(symbol_table);

    parser_t parser;
    parser.compile(expression_string, expression);

    int i;
    for (i = 0; i < xv.size() ; i++) {
        x = xv(i);
        b(i) = expression.value();
    }
    return b;
}

/*
    Creates RHS vector from user's input

    @param rhsFunc is the RHS function and x0,xl and y0,yl the beginning
        and end of the interval
    @return the RHS vector b
*/
MatrixXd Input::EquationParser::Parser2D(std::string func, VectorXd xv,
    VectorXd yv, int N) {
    MatrixXd bmat(xv.size(),yv.size());
    bmat = MatrixXd::Zero(xv.size(),yv.size());
    typedef exprtk::symbol_table<double>    symbol_table_t;
    typedef exprtk::expression<double>      expression_t;
    typedef exprtk::parser<double>          parser_t;

    std::string expression_string = func;

    double x;
    double y;

    symbol_table_t symbol_table;
    symbol_table.add_variable("x", x);
    symbol_table.add_variable("y", y);
    symbol_table.add_constants();

    expression_t expression;
    expression.register_symbol_table(symbol_table);

    parser_t parser;
    parser.compile(expression_string, expression);

    int i, j;

    for (i = 0; i < xv.size(); i++) {
        x = xv(i);
		for (j = 0; j < yv.size(); j++) {
			y = yv(j);
			bmat(i,j) = expression.value();
		}
    }

    return bmat;
}

/*
    Creates 1D BVP Input Object from JSON file

    @param path is the path to the JSON file
    @return void
*/
void Input::InputObject::Set1dBvpInputObjectFromFile(std::string path) {
    // read a JSON file
    std::ifstream   i(path);
    json j;
    i >> j;

    std::cout << "\nProblem Type: \t" << j["problemType"][0]
    << "\n" << std::endl;

    // assign JSON data to variables
    int equationTypeSize;
    equationType = j["equationType"][0];
    equationTypeSize = j["equationType"].size();
    if (equationTypeSize > 1 && equationType == "helmholtz") {
        kWave = j["equationType"][1];
    }
    basisType = j["basisType"][0];
    gaussianType = j["gaussianType"][0];
    x0 = j["interval"][0];
    xl = j["interval"][1];
    boundaryConditionx = j["boundaryConditionx"][0];
    bc0x = j["boundaryValues"][0];
    bclx = j["boundaryValues"][1];
	initialGuess = j["initialGuess"][0];
    rhsFunction = j["rhsFunction"][0];
    N = int(j["order"][0]) + 1;
    interpolationNodes = j["interpolationNodes"][0];

    std::cout << "Problem setup parameters\n" << std::endl;
    std::cout << "Equation Type:\t\t" << equationType << std::endl;
    if (equationTypeSize > 1 && equationType == "helmholtz") {
        std::cout << "Helmholtz parameter:\t" << kWave<< std::endl;
    }
    std::cout << "Polynomial Type:\t" << basisType << std::endl;
    std::cout << "Gaussian Type:\t\t" << gaussianType << std::endl;
    std::cout << "Interval:\t\t" << "["<< x0 << "," << xl << "]" << std::endl;
    std::cout << "Boundary Conditions:\t" << boundaryConditionx << std::endl;
    std::cout << "Boundary Values:\t" << "["<< bc0x << "," << bclx << "]"
        << std::endl;
    std::cout << "Initial Guess:\t\t" << initialGuess << std::endl;
    std::cout << "RHS function:\t\t" << rhsFunction << std::endl;
    std::cout << "Basis Order:\t\t" << N << std::endl;
    std::cout << "Interpolation Nodes:\t" << interpolationNodes << std::endl;
}

/*
    Creates 1D IVP Input Object from JSON file

    @param path is the path to the JSON file
    @return void
*/
void Input::InputObject::Set1dIvpInputObjectFromFile(std::string path) {
    // read a JSON file
    std::ifstream   i(path);
    json j;
    i >> j;

    std::cout << "\nProblem Type: \t" << j["problemType"][0]
    << "\n" << std::endl;

    // assign JSON data to variables
    int equationTypeSize;
    equationType = j["equationType"][0];
    equationTypeSize = j["equationType"].size();
    if (equationTypeSize > 1 && equationType == "unsteadyDiffusion") {
        aDiffusion = j["equationType"][1];
    }
    if (equationTypeSize > 1 && equationType == "unsteadyAdvection") {
        vAdvection = j["equationType"][1];
    }
    if (equationTypeSize > 2 && equationType == "unsteadyAdvectionDiffusion") {
        vAdvection = j["equationType"][1];
        aDiffusion = j["equationType"][2];
    }
    basisType = j["basisType"][0];
    gaussianType = j["gaussianType"][0];
    x0 = j["interval"][0];
    xl = j["interval"][1];
    boundaryConditionx = j["boundaryConditionx"][0];
    bc0x = j["boundaryValues"][0];
    bclx = j["boundaryValues"][1];
    N = int(j["order"][0]) + 1;
    interpolationNodes = j["interpolationNodes"][0];
    time = j["time"][0];
    timeStep = j["timeStep"][0];
    timeIntegration = j["timeIntegration"][0];
    initialCondition = j["initialCondition"][0];

    std::cout << "Problem setup parameters\n" << std::endl;
    std::cout << "Equation Type:\t\t" << equationType << std::endl;
    if (equationTypeSize > 1 && equationType == "unsteadyDiffusion") {
        std::cout << "Diffusion coefficient:\t" << aDiffusion  << std::endl;
    }
    if (equationTypeSize > 1 && equationType == "unsteadyAdvection") {
        std::cout << "Advection velocity:\t" << vAdvection<< std::endl;
    }
    if (equationTypeSize > 2 && equationType == "unsteadyAdvectionDiffusion") {
        std::cout << "Advection velocity:\t" << vAdvection<< std::endl;
        std::cout << "Diffusion coefficient:\t" << aDiffusion  << std::endl;
    }
    std::cout << "Polynomial Type:\t" << basisType << std::endl;
    std::cout << "Gaussian Type:\t\t" << gaussianType << std::endl;
    std::cout << "Interval:\t\t" << "["<< x0 << "," << xl << "]" << std::endl;
    std::cout << "Boundary Conditions:\t" << boundaryConditionx << std::endl;
    std::cout << "Boundary Values:\t" << "["<< bc0x << "," << bclx << "]"
        << std::endl;
    std::cout << "Basis Order:\t\t" << N << std::endl;
    std::cout << "Interpolation Nodes:\t" << interpolationNodes << std::endl;
    std::cout << "Time Interval:\t\t" << time << std::endl;
    std::cout << "Time step:\t\t" << timeStep << std::endl;
    std::cout << "Time Integration:\t" << timeIntegration << std::endl;
    std::cout << "Initial Condition:\t" << initialCondition << std::endl;
}

/*
    Creates 2D BVP Input Object from JSON file

    @param path is the path to the JSON file
    @return void
*/
void Input::InputObject::Set2dBvpInputObjectFromFile(std::string path) {
    // read a JSON file
    std::ifstream   i(path);
    json j;
    i >> j;

    std::cout << "\nProblem Type: \t" << j["problemType"][0]
    << "\n" << std::endl;

    // assign JSON data to variables
    int equationTypeSize;
    equationType = j["equationType"][0];
    equationTypeSize = j["equationType"].size();
    if (equationTypeSize > 1 && equationType == "helmholtz") {
        kWave = j["equationType"][1];
    }
    basisType = j["basisType"][0];
    gaussianType = j["gaussianType"][0];
    x0 = j["intervalx"][0];
    xl = j["intervalx"][1];
    y0 = j["intervaly"][0];
    yl = j["intervaly"][1];
    boundaryConditionx = j["boundaryConditionx"][0];
    boundaryConditiony = j["boundaryConditiony"][0];
    bc0x = j["boundaryValuesx"][0];
    bclx = j["boundaryValuesx"][1];
    bc0y = j["boundaryValuesy"][0];
    bcly = j["boundaryValuesy"][1];
    rhsFunction = j["rhsFunction"][0];
    N = int(j["order"][0]) + 1;
    interpolationNodes = j["interpolationNodes"][0];
    execution = j["execution"][0];
    int executionTypeSize = j["execution"].size();
    if (executionTypeSize > 1 && execution == "parallel") {
        cores = j["execution"][1];
    }

    std::cout << "Problem setup parameters\n" << std::endl;
    std::cout << "Equation Type:\t\t" << equationType << std::endl;
    if (equationTypeSize > 1 && equationType == "helmholtz") {
        std::cout << "Helmholtz parameter:\t" << kWave<< std::endl;
    }
    std::cout << "Polynomial Type:\t" << basisType << std::endl;
    std::cout << "Gaussian Type:\t\t" << gaussianType << std::endl;
    std::cout << "Interval x:\t\t" << "["<< x0 << "," << xl << "]" << std::endl;
    std::cout << "Interval y:\t\t" << "["<< y0 << "," << yl << "]" << std::endl;
    std::cout << "Boundary Conditions x:\t" << boundaryConditionx << std::endl;
    std::cout << "Boundary Conditions y:\t" << boundaryConditiony << std::endl;
    std::cout << "Boundary Values x:\t" << "["<< bc0x << "," << bclx << "]"
        << std::endl;
    std::cout << "Boundary Values y:\t" << "["<< bc0y << "," << bcly << "]"
        << std::endl;
    std::cout << "RHS function:\t\t" << rhsFunction << std::endl;
    std::cout << "Basis Order:\t\t" << N << std::endl;
    std::cout << "Interpolation Nodes:\t" << interpolationNodes << std::endl;
    std::cout << "Execution mode:\t\t" << execution << std::endl;
    if (executionTypeSize > 1 && execution == "parallel") {
        std::cout << "Computational Cores:\t" << cores << std::endl;
    }
}

/*
    Creates 2D IVP Input Object from JSON file

    @param path is the path to the JSON file
    @return void
*/
void Input::InputObject::Set2dIvpInputObjectFromFile(std::string path) {
    // read a JSON file
    std::ifstream   i(path);
    json j;
    i >> j;

    std::cout << "\nProblem Type: \t" << j["problemType"][0]
    << "\n" << std::endl;

    // assign JSON data to variables
    int equationTypeSize;
    equationType = j["equationType"][0];
    equationTypeSize = j["equationType"].size();
    if (equationTypeSize > 1 && equationType == "unsteadyDiffusion") {
        aDiffusion = j["equationType"][1];
    }
    if (equationTypeSize > 1 && equationType == "unsteadyAdvection") {
        vAdvection = j["equationType"][1];
    }
    if (equationTypeSize > 2 && equationType == "unsteadyAdvectionDiffusion") {
        vAdvection = j["equationType"][1];
        aDiffusion = j["equationType"][2];
    }
    basisType = j["basisType"][0];
    gaussianType = j["gaussianType"][0];
    x0 = j["intervalx"][0];
    xl = j["intervalx"][1];
    y0 = j["intervaly"][0];
    yl = j["intervaly"][1];
    boundaryConditionx = j["boundaryConditionx"][0];
    boundaryConditiony = j["boundaryConditiony"][0];
    bc0x = j["boundaryValuesx"][0];
    bclx = j["boundaryValuesx"][1];
    bc0y = j["boundaryValuesy"][0];
    bcly = j["boundaryValuesy"][1];
    rhsFunction = j["rhsFunction"][0];
    N = int(j["order"][0]) + 1;
    interpolationNodes = j["interpolationNodes"][0];
    time = j["time"][0];
    timeStep = j["timeStep"][0];
    timeIntegration = j["timeIntegration"][0];
    initialCondition = j["initialCondition"][0];
    execution = j["execution"][0];
    int executionTypeSize = j["execution"].size();
    if (executionTypeSize > 1 && execution == "parallel") {
        cores = j["execution"][1];
    }

    std::cout << "Problem setup parameters\n" << std::endl;
    std::cout << "Equation Type:\t\t" << equationType << std::endl;
    if (equationTypeSize > 1 && equationType == "unsteadyDiffusion") {
        std::cout << "Diffusion coefficient:\t" << aDiffusion  << std::endl;
    }
    if (equationTypeSize > 1 && equationType == "unsteadyAdvection") {
        std::cout << "Advection velocity:\t" << vAdvection<< std::endl;
    }
    if (equationTypeSize > 2 && equationType == "unsteadyAdvectionDiffusion") {
        std::cout << "Advection velocity:\t" << vAdvection<< std::endl;
        std::cout << "Diffusion coefficient:\t" << aDiffusion  << std::endl;
    }
    std::cout << "Polynomial Type:\t" << basisType << std::endl;
    std::cout << "Gaussian Type:\t\t" << gaussianType << std::endl;
    std::cout << "Interval x:\t\t" << "["<< x0 << "," << xl << "]" << std::endl;
    std::cout << "Interval y:\t\t" << "["<< y0 << "," << yl << "]" << std::endl;
    std::cout << "Boundary Conditions x:\t" << boundaryConditionx << std::endl;
    std::cout << "Boundary Conditions y:\t" << boundaryConditiony << std::endl;
    std::cout << "Boundary Values x:\t" << "["<< bc0x << "," << bclx << "]"
        << std::endl;
    std::cout << "Boundary Values y:\t" << "["<< bc0y << "," << bcly << "]"
        << std::endl;
    std::cout << "RHS function:\t\t" << rhsFunction << std::endl;
    std::cout << "Basis Order:\t\t" << N << std::endl;
    std::cout << "Interpolation Nodes:\t" << interpolationNodes << std::endl;
    std::cout << "Time Interval:\t\t" << time << std::endl;
    std::cout << "Time step:\t\t" << timeStep << std::endl;
    std::cout << "Time Integration:\t" << timeIntegration << std::endl;
    std::cout << "Initial Condition:\t" << initialCondition << std::endl;
    std::cout << "Execution mode:\t\t" << execution << std::endl;
    if (executionTypeSize > 1 && execution == "parallel") {
        std::cout << "Computational Cores:\t" << cores << std::endl;
    }
}

/*
    Namespace: AppConfig
    Definitions for setting object for application configuration from file.
*/

/*
    Get fields from JSON app configuration file

    @param the path to the JSON app configuration file
    @return the app config object
*/
void AppConfig::AppConfigObject::SetAppConfigObjectFromFile(std::string path) {
    std::ifstream   i(path);
    json j;
    i >> j;

    // assign JSON data to variables
    plotSolution = j["plotSolution"];
    saveSolutionFile = j["saveSolutionFile"];

}
