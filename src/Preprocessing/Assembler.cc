/*
    File: Assembler.cc

    Purpose: Contains methods that create the Assembler object that is then
    used as an input to the solver.

    @author: nikpfosk
*/

#include <iostream>
#include <string>
#include <fstream>
#include <cmath>

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

#include "../../include/InputHandlers.h"
#include "../../include/Assembler.h"
#include "../../lib/Json/json.hpp"
#include "../../lib/Exprtk/exprtk.hpp"
#include "../../lib/unsupported/Eigen/KroneckerProduct"

using json = nlohmann::json;
using namespace Eigen;


/*
    Namespace: Assembler
    Set up objects to be used in the solvers
*/

/*
    Interface for the Assembler class.

    @param void
    @return the assembly object
*/
Assembly::AssemblyObject Assembly::AssemblyObject::Assembler(void) {
    std::cout << "\n****************************************\n" << std::endl;
    std::cout << "Assembly process started." << std::endl;

    Assembly::AssemblyObject assemblyObj;
    Input::Init initObj;
    Input::InputObject inputObj;
    Input::EquationParser eqObj;

    ProblemType problemType = DefineProblemType();
    assemblyObj.problemTypeStr = this->problemTypeStr;

    switch (problemType) {
        case Bvp1D: {
            inputObj.Set1dBvpInputObjectFromFile(initObj.pathToInputFile);
            assemblyObj.GetBoundaryValues1D(inputObj);
            assemblyObj.GetIntervalValues1D(inputObj);
            break;
        }
        case Bvp2D:
            inputObj.Set2dBvpInputObjectFromFile(initObj.pathToInputFile);
            assemblyObj.GetBoundaryValues2D(inputObj);
            assemblyObj.GetIntervalValues2D(inputObj);
            break;
        case Ivp1D:
            inputObj.Set1dIvpInputObjectFromFile(initObj.pathToInputFile);
            assemblyObj.GetBoundaryValues1D(inputObj);
            assemblyObj.GetIntervalValues1D(inputObj);
            break;
        case Ivp2D:
            inputObj.Set2dIvpInputObjectFromFile(initObj.pathToInputFile);
            assemblyObj.GetBoundaryValues2D(inputObj);
            assemblyObj.GetIntervalValues2D(inputObj);
            break;
    }

    MethodType methodType = DefineMethodType(&inputObj);
    EquationType equationType = DefineEquationType(&inputObj);
    assemblyObj.equationTypeStr = inputObj.equationType;
    InitialGuess initialGuess = DefineInitialGuess(&inputObj);


    switch (problemType) {
        case Assembly::AssemblyObject::ProblemType::Bvp1D:
        case Assembly::AssemblyObject::ProblemType::Ivp1D: {
            switch (methodType) {
                case ChebyshevGL: {
                    assemblyObj.x = Polynomials::PolynomialBasis::
                    ChebyshevGaussLobattoNodes(inputObj.N,
                    inputObj.x0, inputObj.xl);
                    assemblyObj.w = Polynomials::PolynomialBasis::
                    ChebyshevGaussLobattoWeights(inputObj.N);
                    break;
                }
                case LegendreGL: {
                    assemblyObj.x = Polynomials::PolynomialBasis::
                    LegendreGaussLobattoNodes(inputObj.N,
                    inputObj.x0, inputObj.xl);
                    assemblyObj.w = Polynomials::PolynomialBasis::
                    LegendreGaussLobattoWeights(inputObj.N);
                    break;
                }
                case ChebyshevG: {
                    assemblyObj.x = Polynomials::PolynomialBasis::
                    ChebyshevGaussNodes(inputObj.N,
                    inputObj.x0, inputObj.xl);
                    assemblyObj.w = Polynomials::PolynomialBasis::
                    ChebyshevGaussWeights(inputObj.N);
                    break;
                }
                case LegendreG: {
                    assemblyObj.x = Polynomials::PolynomialBasis::
                    LegendreGaussNodes(inputObj.N,
                    inputObj.x0, inputObj.xl);
                    assemblyObj.w = Polynomials::PolynomialBasis::
                    LegendreGaussWeights(inputObj.N);
                    break;
                }
                case Fourier: {
                    assemblyObj.x = Fourier::FourierBasis::FourierNodes(inputObj.N);
                    break;
                }
            }
            break;
        }
        case Assembly::AssemblyObject::ProblemType::Bvp2D:		
        case Assembly::AssemblyObject::ProblemType::Ivp2D: {
            switch (methodType) {
                case ChebyshevGL: {
                    assemblyObj.x = Polynomials::PolynomialBasis::
                    ChebyshevGaussLobattoNodes(inputObj.N,
                    inputObj.x0, inputObj.xl);
                    assemblyObj.y = Polynomials::PolynomialBasis::
                    ChebyshevGaussLobattoNodes(inputObj.N,
                    inputObj.y0, inputObj.yl);
                    assemblyObj.w = Polynomials::PolynomialBasis::
                    ChebyshevGaussLobattoWeights(inputObj.N);
                    break;
                }
                case LegendreGL: {
                    assemblyObj.x = Polynomials::PolynomialBasis::
                    LegendreGaussLobattoNodes(inputObj.N,
                    inputObj.x0, inputObj.xl);
                    assemblyObj.y = Polynomials::PolynomialBasis::
                    LegendreGaussLobattoNodes(inputObj.N,
                    inputObj.y0, inputObj.yl);
                    assemblyObj.w = Polynomials::PolynomialBasis::
                    LegendreGaussLobattoWeights(inputObj.N);
                    break;
                }
                case ChebyshevG: {
                    assemblyObj.x = Polynomials::PolynomialBasis::
                    ChebyshevGaussNodes(inputObj.N,
                    inputObj.x0, inputObj.xl);
                    assemblyObj.y = Polynomials::PolynomialBasis::
                    ChebyshevGaussNodes(inputObj.N,
                    inputObj.y0, inputObj.yl);
                    assemblyObj.w = Polynomials::PolynomialBasis::
                    ChebyshevGaussWeights(inputObj.N);
                    break;
                }
                case LegendreG: {
                    assemblyObj.x = Polynomials::PolynomialBasis::
                    LegendreGaussNodes(inputObj.N,
                    inputObj.x0, inputObj.xl);
                    assemblyObj.y = Polynomials::PolynomialBasis::
                    LegendreGaussNodes(inputObj.N,
                    inputObj.y0, inputObj.yl);
                    assemblyObj.w = Polynomials::PolynomialBasis::
                    LegendreGaussWeights(inputObj.N);
                    break;
                }
                case Fourier: {
                    assemblyObj.x = Fourier::FourierBasis::FourierNodes(inputObj.N);
                    assemblyObj.y = Fourier::FourierBasis::FourierNodes(inputObj.N);
                    break;
                }
            }
            break;
        }
    }

    switch (problemType) {
        case Ivp1D:
            assemblyObj.GetIntervalValues1D(inputObj);
            assemblyObj.GetTimeIntervalValues(inputObj);
            assemblyObj.ut0 = eqObj.Parser1D(inputObj.initialCondition,
                assemblyObj.x, inputObj.N);
            break;
        case Ivp2D:
            assemblyObj.GetTimeIntervalValues(inputObj);
			assemblyObj.ut0mat = eqObj.Parser2D(inputObj.initialCondition,
			assemblyObj.x, assemblyObj.y, inputObj.N);
            break;
    }

    switch (problemType) {
        case Assembly::AssemblyObject::ProblemType::Bvp1D:
        case Assembly::AssemblyObject::ProblemType::Ivp1D: {

            switch (equationType) {
                case Laplace: {
                    if (inputObj.basisType == "fourier") {
                        assemblyObj.A = Fourier::FourierBasis::
                            DerivativeMatrix(inputObj.N, 2);
                    } else {
                        assemblyObj.D = Polynomials::PolynomialBasis::
                            DerivativeMatrix(assemblyObj.x, assemblyObj.w);
                        assemblyObj.A = Polynomials::PolynomialBasis::
                            DerivativeMatrix2(assemblyObj.D);
                    }
                    assemblyObj.b = VectorXd::Zero(inputObj.N);
                    break;
                }
                case Poisson: {
                    if (inputObj.basisType == "fourier") {
                        assemblyObj.D = Fourier::FourierBasis::
                            DerivativeMatrix(inputObj.N, 1);
                        assemblyObj.A = Fourier::FourierBasis::
                            DerivativeMatrix(inputObj.N, 2);
                    } else {
                        assemblyObj.D = Polynomials::PolynomialBasis::
                            DerivativeMatrix(assemblyObj.x, assemblyObj.w);
                        assemblyObj.A = Polynomials::PolynomialBasis::
                            DerivativeMatrix2(assemblyObj.D);
                    }
                    assemblyObj.b = eqObj.Parser1D(inputObj.rhsFunction,
                    assemblyObj.x, inputObj.N);
                    break;
                }
                case Helmholtz: {
                    if (inputObj.basisType == "fourier") {
                        assemblyObj.D = Fourier::FourierBasis::
                            DerivativeMatrix(inputObj.N, 1);
                        assemblyObj.A = Fourier::FourierBasis::
                            DerivativeMatrix(inputObj.N, 2);
                        assemblyObj.A =
                            HelmholtzMatrix(assemblyObj.A, inputObj.kWave);
                    } else {
                        assemblyObj.D = Polynomials::PolynomialBasis::
                            DerivativeMatrix(assemblyObj.x, assemblyObj.w);
                        assemblyObj.D2 = Polynomials::PolynomialBasis::
                            DerivativeMatrix2(assemblyObj.D);
                        assemblyObj.A =
                            HelmholtzMatrix(assemblyObj.D2, inputObj.kWave);
                    }
                    assemblyObj.b = eqObj.Parser1D(inputObj.rhsFunction,
                    assemblyObj.x, inputObj.N);
                    break;
                }
                case UnsteadyDiffusion: {
                    assemblyObj.aDiffusion = inputObj.aDiffusion;
                    if (inputObj.basisType == "fourier") {
                        assemblyObj.D = Fourier::FourierBasis::
                            DerivativeMatrix(inputObj.N, 1);
                        assemblyObj.A = Fourier::FourierBasis::
                            DerivativeMatrix(inputObj.N, 2);
                    } else {
                        assemblyObj.D = Polynomials::PolynomialBasis::
                            DerivativeMatrix(assemblyObj.x, assemblyObj.w);
                        assemblyObj.A = Polynomials::PolynomialBasis::
                            DerivativeMatrix2(assemblyObj.D);
                    }
                    assemblyObj.ut0 = eqObj.Parser1D(inputObj.initialCondition,
                    assemblyObj.x, inputObj.N);
                    break;
                }
                case UnsteadyAdvection: {
                    assemblyObj.vAdvection= inputObj.vAdvection;
                    if (inputObj.basisType == "fourier") {
                        assemblyObj.D = Fourier::FourierBasis::
                            DerivativeMatrix(inputObj.N, 1);
                        assemblyObj.A = assemblyObj.D;
                    } else {
                        assemblyObj.D = Polynomials::PolynomialBasis::
                            DerivativeMatrix(assemblyObj.x, assemblyObj.w);
                        assemblyObj.A = assemblyObj.D;
                    }
                    assemblyObj.ut0 = eqObj.Parser1D(inputObj.initialCondition,
                    assemblyObj.x, inputObj.N);
                    break;
                }
                case UnsteadyAdvectionDiffusion: {
                    assemblyObj.vAdvection= inputObj.vAdvection;
                    assemblyObj.aDiffusion = inputObj.aDiffusion;
                    if (inputObj.basisType == "fourier") {
                        assemblyObj.D = Fourier::FourierBasis::
                            DerivativeMatrix(inputObj.N, 1);
                        assemblyObj.A = Fourier::FourierBasis::
                            DerivativeMatrix(inputObj.N, 2);
                    } else {
                        assemblyObj.D = Polynomials::PolynomialBasis::
                            DerivativeMatrix(assemblyObj.x, assemblyObj.w);
                        assemblyObj.A = Polynomials::PolynomialBasis::
                            DerivativeMatrix2(assemblyObj.D);
                    }
                    assemblyObj.ut0 = eqObj.Parser1D(inputObj.initialCondition,
                    assemblyObj.x, inputObj.N);
                    break;
                }
                case UnsteadyInviscidBurger: {
                    if (inputObj.basisType == "fourier") {
                        assemblyObj.D = Fourier::FourierBasis::
                            DerivativeMatrix(inputObj.N, 1);
                        assemblyObj.A = assemblyObj.D;
                    } else {
                        assemblyObj.D = Polynomials::PolynomialBasis::
                            DerivativeMatrix(assemblyObj.x, assemblyObj.w);
                        assemblyObj.A = assemblyObj.D;
                    }
                    assemblyObj.ut0 = eqObj.Parser1D(inputObj.initialCondition,
                    assemblyObj.x, inputObj.N);
                    break;
                }
            }
            break;
        }
        case Assembly::AssemblyObject::ProblemType::Bvp2D:
        case Assembly::AssemblyObject::ProblemType::Ivp2D: {	
            switch (equationType) {
                case Laplace: {
                    if (inputObj.basisType == "fourier") {
                        assemblyObj.D2x = Fourier::FourierBasis::
                            DerivativeMatrix(inputObj.N, 2);
                        assemblyObj.D2y = Fourier::FourierBasis::
                            DerivativeMatrix(inputObj.N, 2);
						assemblyObj.Ax = &assemblyObj.D2x;
						assemblyObj.Ay = &assemblyObj.D2y;
                    } else {
                        assemblyObj.Dx = Polynomials::PolynomialBasis::
                            DerivativeMatrix(assemblyObj.x, assemblyObj.w);
                        assemblyObj.D2x = Polynomials::PolynomialBasis::
                            DerivativeMatrix2(assemblyObj.Dx);
                        assemblyObj.Dy = Polynomials::PolynomialBasis::
                            DerivativeMatrix(assemblyObj.y, assemblyObj.w);
                        assemblyObj.D2y = Polynomials::PolynomialBasis::
                            DerivativeMatrix2(assemblyObj.Dy);
						assemblyObj.Ax = &assemblyObj.D2x;
						assemblyObj.Ay = &assemblyObj.D2y;
                    }
                    assemblyObj.b = VectorXd::Zero(pow(inputObj.N, 2));
                    break;
                }
                case Poisson: {
                    if (inputObj.basisType == "fourier") {
                        assemblyObj.Dx = Fourier::FourierBasis::
                            DerivativeMatrix(inputObj.N, 1);
                        assemblyObj.Dy = Fourier::FourierBasis::
                            DerivativeMatrix(inputObj.N, 1);
                        assemblyObj.D2x = Fourier::FourierBasis::
                            DerivativeMatrix(inputObj.N, 2);
                        assemblyObj.D2y = Fourier::FourierBasis::
                            DerivativeMatrix(inputObj.N, 2);
						assemblyObj.Ax = &assemblyObj.D2x;
						assemblyObj.Ay = &assemblyObj.D2y;
                    } else {
                        assemblyObj.Dx = Polynomials::PolynomialBasis::
                            DerivativeMatrix(assemblyObj.x, assemblyObj.w);
                        assemblyObj.D2x = Polynomials::PolynomialBasis::
                            DerivativeMatrix2(assemblyObj.Dx);
                        assemblyObj.Dy = Polynomials::PolynomialBasis::
                            DerivativeMatrix(assemblyObj.y, assemblyObj.w);
                        assemblyObj.D2y = Polynomials::PolynomialBasis::
                            DerivativeMatrix2(assemblyObj.Dy);
						assemblyObj.Ax = &assemblyObj.D2x;
						assemblyObj.Ay = &assemblyObj.D2y;
                    }
                    assemblyObj.bmat = eqObj.Parser2D(inputObj.rhsFunction,
                    assemblyObj.x, assemblyObj.y, inputObj.N);
                    break;
                }
                case Helmholtz: {
                    if (inputObj.basisType == "fourier") {
                        assemblyObj.Dx = Fourier::FourierBasis::
                            DerivativeMatrix(inputObj.N, 1);
                        assemblyObj.Dy = Fourier::FourierBasis::
                            DerivativeMatrix(inputObj.N, 1);
                        assemblyObj.D2x = Fourier::FourierBasis::
                            DerivativeMatrix(inputObj.N, 2);
                        assemblyObj.D2y = Fourier::FourierBasis::
                            DerivativeMatrix(inputObj.N, 2);
						assemblyObj.Ax = &assemblyObj.D2x;
						assemblyObj.Ay = &assemblyObj.D2y;
                    } else {
                        assemblyObj.Dx = Polynomials::PolynomialBasis::
                            DerivativeMatrix(assemblyObj.x, assemblyObj.w);
                        assemblyObj.D2x = Polynomials::PolynomialBasis::
                            DerivativeMatrix2(assemblyObj.Dx);
                        assemblyObj.Dy = Polynomials::PolynomialBasis::
                            DerivativeMatrix(assemblyObj.y, assemblyObj.w);
                        assemblyObj.D2y = Polynomials::PolynomialBasis::
                            DerivativeMatrix2(assemblyObj.Dy);
						assemblyObj.Ax = &assemblyObj.D2x;
						assemblyObj.Ay = &assemblyObj.D2y;
                    }
                    assemblyObj.bmat = eqObj.Parser2D(inputObj.rhsFunction,
                    assemblyObj.x, assemblyObj.y, inputObj.N);
                    break;
                }
                case UnsteadyDiffusion: {
                    assemblyObj.aDiffusion = inputObj.aDiffusion;
                    if (inputObj.basisType == "fourier") {
                        assemblyObj.Dx = Fourier::FourierBasis::
                            DerivativeMatrix(inputObj.N, 1);
                        assemblyObj.Dy = Fourier::FourierBasis::
                            DerivativeMatrix(inputObj.N, 1);
                        assemblyObj.D2x = Fourier::FourierBasis::
                            DerivativeMatrix(inputObj.N, 2);
                        assemblyObj.D2y = Fourier::FourierBasis::
                            DerivativeMatrix(inputObj.N, 2);
						assemblyObj.Ax = &assemblyObj.D2x;
						assemblyObj.Ay = &assemblyObj.D2y;
                    } else {
                        assemblyObj.Dx = Polynomials::PolynomialBasis::
                            DerivativeMatrix(assemblyObj.x, assemblyObj.w);
                        assemblyObj.D2x = Polynomials::PolynomialBasis::
                            DerivativeMatrix2(assemblyObj.Dx);
                        assemblyObj.Dy = Polynomials::PolynomialBasis::
                            DerivativeMatrix(assemblyObj.y, assemblyObj.w);
                        assemblyObj.D2y = Polynomials::PolynomialBasis::
                            DerivativeMatrix2(assemblyObj.Dy);
						assemblyObj.Ax = &assemblyObj.D2x;
						assemblyObj.Ay = &assemblyObj.D2y;
                    }
                    assemblyObj.bmat = eqObj.Parser2D(inputObj.rhsFunction,
                    assemblyObj.x, assemblyObj.y, inputObj.N);
                    break;
                }
                case UnsteadyAdvection: {
                    assemblyObj.vAdvection= inputObj.vAdvection;
                    if (inputObj.basisType == "fourier") {
                        assemblyObj.Dx = Fourier::FourierBasis::
                            DerivativeMatrix(inputObj.N, 1);
                        assemblyObj.Dy = Fourier::FourierBasis::
                            DerivativeMatrix(inputObj.N, 1);
						assemblyObj.Ax = &assemblyObj.Dx;
						assemblyObj.Ay = &assemblyObj.Dy;
                    } else {
                        assemblyObj.Dx = Polynomials::PolynomialBasis::
                            DerivativeMatrix(assemblyObj.x, assemblyObj.w);
                        assemblyObj.Dy = Polynomials::PolynomialBasis::
                            DerivativeMatrix(assemblyObj.y, assemblyObj.w);
						assemblyObj.Ax = &assemblyObj.Dx;
						assemblyObj.Ay = &assemblyObj.Dy;
                    }
                    assemblyObj.bmat = eqObj.Parser2D(inputObj.rhsFunction,
                    assemblyObj.x, assemblyObj.y, inputObj.N);
                    break;
                }
                case UnsteadyAdvectionDiffusion: {
                    assemblyObj.vAdvection= inputObj.vAdvection;
                    assemblyObj.aDiffusion = inputObj.aDiffusion;
                    if (inputObj.basisType == "fourier") {
                        assemblyObj.Dx = Fourier::FourierBasis::
                            DerivativeMatrix(inputObj.N, 1);
                        assemblyObj.Dy = Fourier::FourierBasis::
                            DerivativeMatrix(inputObj.N, 1);
                        assemblyObj.D2x = Fourier::FourierBasis::
                            DerivativeMatrix(inputObj.N, 2);
                        assemblyObj.D2y = Fourier::FourierBasis::
                            DerivativeMatrix(inputObj.N, 2);
						assemblyObj.Ax = &assemblyObj.D2x;
						assemblyObj.Ay = &assemblyObj.D2y;
                    } else {
                        assemblyObj.Dx = Polynomials::PolynomialBasis::
                            DerivativeMatrix(assemblyObj.x, assemblyObj.w);
                        assemblyObj.D2x = Polynomials::PolynomialBasis::
                            DerivativeMatrix2(assemblyObj.Dx);
                        assemblyObj.Dy = Polynomials::PolynomialBasis::
                            DerivativeMatrix(assemblyObj.y, assemblyObj.w);
                        assemblyObj.D2y = Polynomials::PolynomialBasis::
                            DerivativeMatrix2(assemblyObj.Dy);
						assemblyObj.Ax = &assemblyObj.D2x;
						assemblyObj.Ay = &assemblyObj.D2y;
                    }
                    assemblyObj.bmat = eqObj.Parser2D(inputObj.rhsFunction,
                    assemblyObj.x, assemblyObj.y, inputObj.N);
                    break;
                }
                case UnsteadyInviscidBurger: {
                    if (inputObj.basisType == "fourier") {
                        assemblyObj.Dx = Fourier::FourierBasis::
                            DerivativeMatrix(inputObj.N, 1);
                        assemblyObj.Dy = Fourier::FourierBasis::
                            DerivativeMatrix(inputObj.N, 1);
						assemblyObj.Ax = &assemblyObj.Dx;
						assemblyObj.Ay = &assemblyObj.Dy;
                    } else {
                        assemblyObj.Dx = Polynomials::PolynomialBasis::
                            DerivativeMatrix(assemblyObj.x, assemblyObj.w);
                        assemblyObj.Dy = Polynomials::PolynomialBasis::
                            DerivativeMatrix(assemblyObj.y, assemblyObj.w);
						assemblyObj.Ax = &assemblyObj.Dx;
						assemblyObj.Ay = &assemblyObj.Dy;
                    }
                    assemblyObj.bmat = eqObj.Parser2D(inputObj.rhsFunction,
                    assemblyObj.x, assemblyObj.y, inputObj.N);
                    break;
                }
            }
            break;
        }
    }


	assemblyObj.boundaryConditionx = inputObj.boundaryConditionx;
	assemblyObj.boundaryConditiony = inputObj.boundaryConditiony;

	if ((assemblyObj.boundaryConditionx) == "dirichlet") {
		bcx = BoundaryConditions::Dirichlet;
	}
	else if ((assemblyObj.boundaryConditionx) == "neumann") {
		bcx = BoundaryConditions::Neumann;
	}
	else if ((assemblyObj.boundaryConditionx) == "dirichlet-neumann") {
		bcx = BoundaryConditions::DirNeu;
	}
	else if ((assemblyObj.boundaryConditionx) == "neumann-dirichlet") {
		bcx = BoundaryConditions::NeuDir;
	}
	else {
		std::cout << "ERROR: Not valid boundary conditions type." << std::endl;
		throw std::exception();
	}

	switch (problemType) {
		case Assembly::AssemblyObject::ProblemType::Bvp1D:
		case Assembly::AssemblyObject::ProblemType::Ivp1D: {
			switch (initialGuess) {
				case Random: {
					assemblyObj.u0 = Polynomials::PolynomialBasis::
						RandomInitialGuess1D(inputObj.N);
					break;
				}
				case Zero: {
					assemblyObj.u0 = Polynomials::PolynomialBasis::
						ZeroInitialGuess1D(inputObj.N);
					break;
				}
			}
			switch (bcx) {
				case Dirichlet: {
					assemblyObj.DirichletInit1D();
					break;
				}
				case Neumann: {
					assemblyObj.NeumannInit1D();
					break;
				}
				case DirNeu: {
					assemblyObj.DirichletNeumannInit1D();
					break;
				}
				case NeuDir: {
					assemblyObj.NeumannDirichletInit1D();
					break;
				}
			}
			break;
		}
		case Assembly::AssemblyObject::ProblemType::Bvp2D:
		case Assembly::AssemblyObject::ProblemType::Ivp2D: {
			if ((assemblyObj.boundaryConditiony) == "dirichlet") {
				bcy = BoundaryConditions::Dirichlet;
			}
			else if ((assemblyObj.boundaryConditiony) == "neumann") {
				bcy = BoundaryConditions::Neumann;
			}
			else if ((assemblyObj.boundaryConditiony) == "dirichlet-neumann") {
				bcy = BoundaryConditions::DirNeu;
			}
			else if ((assemblyObj.boundaryConditiony) == "neumann-dirichlet") {
				bcy = BoundaryConditions::NeuDir;
			}
			else {
				std::cout << "ERROR: Not valid boundary conditions type." << std::endl;
				throw std::exception();
			}

			switch (initialGuess) {
				case Random: {
					assemblyObj.u0mat = Polynomials::PolynomialBasis::
						RandomInitialGuess2D(inputObj.N);
					break;
				}
				case Zero: {
					assemblyObj.u0mat = Polynomials::PolynomialBasis::
						ZeroInitialGuess2D(inputObj.N);
					break;
				}
			}

			switch (bcx) {
				case Dirichlet: {
					assemblyObj.DirichletInit2Dx();
					break;
				}
				case Neumann: {
					assemblyObj.NeumannInit2Dx();
					break;
				}
				case DirNeu: {
					assemblyObj.DirichletNeumannInit2Dx();
					break;
				}
				case NeuDir: {
					assemblyObj.NeumannDirichletInit2Dx();
					break;
				}
			}

			switch (bcy) {
				case Dirichlet: {
					assemblyObj.DirichletInit2Dy();
					break;
				}
				case Neumann: {
					assemblyObj.NeumannInit2Dy();
					break;
				}
				case DirNeu: {
					assemblyObj.DirichletNeumannInit2Dy();
					break;
				}
				case NeuDir: {
					assemblyObj.NeumannDirichletInit2Dy();
					break;
				}
			}

			break;
		}
	}

    switch (problemType) {
        case Assembly::AssemblyObject::ProblemType::Bvp2D:
        case Assembly::AssemblyObject::ProblemType::Ivp2D: {	
            switch (equationType) {
                case Laplace: {
                    if (inputObj.basisType == "fourier") {
                        assemblyObj.Asp = Dimensions::Extend2D::
							DerivativeMatrix2D(*assemblyObj.Ax, *assemblyObj.Ay);
                    } else {
                        assemblyObj.Asp = Dimensions::Extend2D::
							DerivativeMatrix2D(*assemblyObj.Ax, *assemblyObj.Ay);
                    }
                    break;
                }
                case Poisson: {
                    if (inputObj.basisType == "fourier") {
                        assemblyObj.Asp = Dimensions::Extend2D::
							DerivativeMatrix2D(*assemblyObj.Ax, *assemblyObj.Ay);
					} else {
                        assemblyObj.Asp = Dimensions::Extend2D::
							DerivativeMatrix2D(*assemblyObj.Ax, *assemblyObj.Ay);
					}
                    break;
                }
                case Helmholtz: {
                    if (inputObj.basisType == "fourier") {
                        assemblyObj.Asp = Dimensions::Extend2D::
							DerivativeMatrix2D(*assemblyObj.Ax, *assemblyObj.Ay);
                        assemblyObj.Asp = Assembly::AssemblyObject::
                            HelmholtzMatrix2D(assemblyObj.Asp, inputObj.kWave);
                    } else {
                        assemblyObj.Asp = Dimensions::Extend2D::
							DerivativeMatrix2D(*assemblyObj.Ax, *assemblyObj.Ay);
                        assemblyObj.Asp = Assembly::AssemblyObject::
                            HelmholtzMatrix2D(assemblyObj.Asp, inputObj.kWave);
                    }
                    break;
                }
                case UnsteadyDiffusion: {
                    if (inputObj.basisType == "fourier") {
                        assemblyObj.Asp = Dimensions::Extend2D::
							DerivativeMatrix2D(*assemblyObj.Ax, *assemblyObj.Ay);
					} else {
                        assemblyObj.Asp = Dimensions::Extend2D::
							DerivativeMatrix2D(*assemblyObj.Ax, *assemblyObj.Ay);
					}
                    break;
                }
                case UnsteadyAdvection: {
                    if (inputObj.basisType == "fourier") {
                        assemblyObj.Asp = Dimensions::Extend2D::
							DerivativeMatrix2D(*assemblyObj.Ax, *assemblyObj.Ay);
					} else {
                        assemblyObj.Asp = Dimensions::Extend2D::
							DerivativeMatrix2D(*assemblyObj.Ax, *assemblyObj.Ay);
					}
                    break;
                }
                case UnsteadyAdvectionDiffusion: {
                    if (inputObj.basisType == "fourier") {
                        assemblyObj.Asp = Dimensions::Extend2D::
							DerivativeMatrix2D(*assemblyObj.Ax, *assemblyObj.Ay);
					} else {
                        assemblyObj.Asp = Dimensions::Extend2D::
							DerivativeMatrix2D(*assemblyObj.Ax, *assemblyObj.Ay);
					}
                    assemblyObj.Dsp = Dimensions::Extend2D::
						DerivativeMatrix2D(assemblyObj.Dx, assemblyObj.Dy);
                    break;
                }
                case UnsteadyInviscidBurger: {
                    if (inputObj.basisType == "fourier") {
                        assemblyObj.Asp = Dimensions::Extend2D::
							DerivativeMatrix2D(*assemblyObj.Ax, *assemblyObj.Ay);
					} else {
                        assemblyObj.Asp = Dimensions::Extend2D::
							DerivativeMatrix2D(*assemblyObj.Ax, *assemblyObj.Ay);
					}
                    break;
                }
            }

            assemblyObj.execution = inputObj.execution;
            if (assemblyObj.execution == "parallel")
                assemblyObj.cores= inputObj.cores;

			assemblyObj.b = Assembly::AssemblyObject::Map2Dto1D(assemblyObj.bmat);
			assemblyObj.u0 = Assembly::AssemblyObject::Map2Dto1D(assemblyObj.u0mat);
			assemblyObj.ut0 = Assembly::AssemblyObject::Map2Dto1D(assemblyObj.ut0mat);
            break;
        }
    }

    return assemblyObj;  // return the assembly object to main
}

/*
    Constructor and destructor Definitions

    @param -
    @return -
*/

Assembly::AssemblyObject::AssemblyObject() {
    x = VectorXd::Zero(1);
    y = VectorXd::Zero(1);
    w = VectorXd::Zero(1);
    D = MatrixXd::Zero(1, 1);
    Dx = MatrixXd::Zero(1, 1);
    Dy = MatrixXd::Zero(1, 1);
    D2 = MatrixXd::Zero(1, 1);
    D2x = MatrixXd::Zero(1, 1);
    D2y = MatrixXd::Zero(1, 1);
    Dm = MatrixXd::Zero(1, 1);
    A = MatrixXd::Zero(1, 1);
    b = VectorXd::Zero(1);
    u = VectorXd::Zero(1);
    u0 = VectorXd::Zero(1);
    u0mat = MatrixXd::Zero(1, 1);
    ut0 = VectorXd::Zero(1);
    ut0mat = MatrixXd::Zero(1, 1);
    Asp = A.sparseView();
    Dsp = D.sparseView();
    x0 = -1;
    xl = 1;
    y0 = -1;
    yl = 1;
    bc0x = 0;
    bclx = 0;
    bc0y = 0;
    bcly = 0;
    boundaryCondition = "";
    boundaryConditionx = "";
    boundaryConditiony = "";
    problemTypeStr = "";
    equationTypeStr = "";
    time = 0;
    timeStep = 0;
    timeIntegration = "";
    aDiffusion = 0;
    vAdvection = 0;
    execution = "";
    cores = 1;
}
Assembly::AssemblyObject::~AssemblyObject() {
}

/*
    Get the interval values from the input object in the 1D cases

    @param const input object
    @return -
*/
void Assembly::AssemblyObject::
GetIntervalValues1D(const Input::InputObject &obj) {
    this->x0 = obj.x0;
    this->xl = obj.xl;
    this->interpolationNodes = obj.interpolationNodes;
}

/*
    Get the interval values from the input object in the 2D cases

    @param const input object
    @return -
*/
void Assembly::AssemblyObject::
GetIntervalValues2D(const Input::InputObject &obj) {
    this->x0 = obj.x0;
    this->xl = obj.xl;
    this->y0 = obj.y0;
    this->yl = obj.yl;
    this->interpolationNodes = obj.interpolationNodes;
}

/*
    Get the boundary values from the input object in the 1D cases

    @param const input object
    @return -
*/
void Assembly::AssemblyObject::
GetBoundaryValues1D(const Input::InputObject &obj) {
    this->boundaryConditionx = obj.boundaryConditionx;
    this->bc0x = obj.bc0x;
    this->bclx = obj.bclx;
}

/*
    Get the boundary values from the input object in the 2D cases

    @param const input object
    @return -
*/
void Assembly::AssemblyObject::
GetBoundaryValues2D(const Input::InputObject &obj) {
    this->boundaryConditionx = obj.boundaryConditionx;
    this->bc0x = obj.bc0x;
    this->bclx = obj.bclx;
    this->bc0y = obj.bc0y;
    this->bcly = obj.bcly;
}

/*
    Get the time interval values from the input object

    @param const input object
    @return -
*/
void Assembly::AssemblyObject::
GetTimeIntervalValues(const Input::InputObject &obj) {
    this->time = obj.time;
    this->timeStep = obj.timeStep;
    this->timeIntegration = obj.timeIntegration;
}

/*
    Defines the problem type to be solved

    @param const input object as template
    @return the ProblemType enum struct
*/
Assembly::AssemblyObject::ProblemType Assembly::AssemblyObject::
DefineProblemType(void) {
    Input::Init probTypeObj;
    this->problemTypeStr = probTypeObj.GetProblemType(probTypeObj.pathToInputFile);
    ProblemType pt;
    if (this->problemTypeStr == "BVP_1D") {
        pt = ProblemType::Bvp1D;
    } else if (this->problemTypeStr == "BVP_2D") {
        pt = ProblemType::Bvp2D;
    } else if (this->problemTypeStr == "IVP_1D") {
        pt = ProblemType::Ivp1D;
    } else if (this->problemTypeStr == "IVP_2D") {
        pt = ProblemType::Ivp2D;
    } else {
        std::cout << "ERROR: Not valid problem type." << std::endl;
        throw std::exception();
    }
    return pt;
}

/*
    Defines the problem type to be solved, static version for other classes

    @param const input object as template
    @return the ProblemType enum struct
*/
Assembly::AssemblyObject::ProblemType Assembly::AssemblyObject::
DefineProblemTypeStatic(void) {
    Input::Init probTypeObj;
    std::string problemTypeStr = probTypeObj.GetProblemType(probTypeObj.pathToInputFile);
    ProblemType pt;
    if (problemTypeStr == "BVP_1D") {
        pt = ProblemType::Bvp1D;
    } else if (problemTypeStr == "BVP_2D") {
        pt = ProblemType::Bvp2D;
    } else if (problemTypeStr == "IVP_1D") {
        pt = ProblemType::Ivp1D;
    } else if (problemTypeStr == "IVP_2D") {
        pt = ProblemType::Ivp2D;
    } else {
        std::cout << "ERROR: Not valid problem type." << std::endl;
        throw std::exception();
    }
    return pt;
}

/*
    Defines the equation type to be solved

    @param const input object as template
    @return the EquationType enum struct
*/
template <class T>
Assembly::AssemblyObject::EquationType Assembly::AssemblyObject::
DefineEquationType(const T& obj) {
    std::string equationType = obj->equationType;
    EquationType et;
    if (equationType == "laplace") {
        et = EquationType::Laplace;
    } else if (equationType == "poisson") {
        et = EquationType::Poisson;
    } else if (equationType == "helmholtz") {
        et = EquationType::Helmholtz;
    } else if (equationType == "unsteadyDiffusion") {
        et = EquationType::UnsteadyDiffusion;
    } else if (equationType == "unsteadyAdvection") {
        et = EquationType::UnsteadyAdvection;
    } else if (equationType == "unsteadyAdvectionDiffusion") {
        et = EquationType::UnsteadyAdvectionDiffusion;
    } else if (equationType == "unsteadyInviscidBurger") {
        et = EquationType::UnsteadyInviscidBurger;
    } else {
        std::cout << "ERROR: Not valid equation type." << std::endl;
        throw std::exception();
    }
    return et;
}

/*
    Defines the equation type to be solved, static version for other classes

    @param const input object as template
    @return the EquationType enum struct
*/
Assembly::AssemblyObject::EquationType Assembly::AssemblyObject::
DefineEquationTypeStatic(std::string equationType) {
    EquationType et;
    if (equationType == "laplace") {
        et = EquationType::Laplace;
    } else if (equationType == "poisson") {
        et = EquationType::Poisson;
    } else if (equationType == "helmholtz") {
        et = EquationType::Helmholtz;
    } else if (equationType == "unsteadyDiffusion") {
        et = EquationType::UnsteadyDiffusion;
    } else if (equationType == "unsteadyAdvection") {
        et = EquationType::UnsteadyAdvection;
    } else if (equationType == "unsteadyAdvectionDiffusion") {
        et = EquationType::UnsteadyAdvectionDiffusion;
    } else if (equationType == "unsteadyInviscidBurger") {
        et = EquationType::UnsteadyInviscidBurger;
    } else {
        std::cout << "ERROR: Not valid equation type." << std::endl;
        throw std::exception();
    }
    return et;
}
/*
    Defines the method type to solve the problem

    @param const input object as template
    @return the MethodType enum struct
*/
template <class T>
Assembly::AssemblyObject::MethodType Assembly::AssemblyObject::
DefineMethodType(const T& obj) {
    std::string basisType = obj->basisType;
    std::string gaussianType = obj->gaussianType;
    MethodType mt;
    if (basisType == "chebyshev" && gaussianType == "gauss-lobatto") {
        mt = MethodType::ChebyshevGL;
    } else if (basisType == "legendre" && gaussianType == "gauss-lobatto") {
        mt = MethodType::LegendreGL;
    } else if (basisType == "chebyshev" && gaussianType == "gauss") {
        mt = MethodType::ChebyshevG;
    } else if (basisType == "legendre" && gaussianType == "gauss") {
        mt = MethodType::LegendreG;
    } else if (basisType == "fourier") {
        mt = MethodType::Fourier;
    } else {
        std::cout << "ERROR: Not valid method type combination"
            "(basis & gaussian)." << std::endl;
        throw std::exception();
    }
    return mt;
}

/*
    Defines the problem type to be solved

    @param const input object as template
    @return the InitialGuess enum struct
*/
template <class T>
Assembly::AssemblyObject::InitialGuess Assembly::AssemblyObject::
DefineInitialGuess(const T& obj) {
    std::string initialGuess = obj->initialGuess;
    InitialGuess ig;
    if (initialGuess == "") {
        initialGuess = "random";
    }
    if (initialGuess == "random") {
        ig = InitialGuess::Random;
    } else if (initialGuess == "zero") {
        ig = InitialGuess::Zero;
    } else {
        std::cout << "ERROR: Not valid initial guess." << std::endl;
        throw std::exception();
    }
    return ig;
}

/*
    Enforce the negative sum trick on matrix D

    @param the matrix D
    @return the matrix D after the negative sum trick
*/
MatrixXd Assembly::AssemblyObject::NegativeSumTrick(MatrixXd D) {
    double sum;
    for (int i = 0; i < D.rows(); i++) {
     sum = 0;
     for (int j = 0; j < D.cols(); j++) {
         if (i != j) {
         sum += D(i, j);
         }
     }
     D(i, i) = -sum;
    }
    return D;
}

/*
    Return the system matrix

    @param the first and second order derivative matrices
    @return the Helmholtz system matrix
*/
MatrixXd Assembly::AssemblyObject::HelmholtzMatrix(MatrixXd D2, double k) {
    MatrixXd I;
    I = MatrixXd::Identity(D2.rows(), D2.cols());
    D2 = D2 + pow(k, 2) * I;

    return D2;
}

/*
    Return the sparse system matrix for 2D

    @param the first and second order derivative matrices
    @return the Helmholtz system matrix
*/
SparseMatrix<double, RowMajor> Assembly::AssemblyObject::HelmholtzMatrix2D(SparseMatrix<double, RowMajor> D2, double k) {
    SparseMatrix<double, RowMajor> I(D2.rows(), D2.cols());
    I.setIdentity();
    D2 = D2 + pow(k, 2) * I;

    return D2;
}

/*
    Remaps 2D RHS b matrix to 1D vector for system solver 

    @param  the 2D RHS matrix 
    @return the 1D RHS vector 
*/
VectorXd Assembly::AssemblyObject::Map2Dto1D(MatrixXd mat) {

	Map<RowVectorXd> m(mat.data(), mat.size());
	// If vector order is wrong enable this
	//Matrix<double, Dynamic, Dynamic, RowMajor> bmat2(bmat);
	//Map<RowVectorXd> b(bmat2.data(), bmat2.size());

	return m;
}

/*
Initialize matrices and vectors for solver for dirichlet BC. We remove top
and bottom rows/cols of system matrix and top and bottom vector elements.

@param -
@return -
We work with pointers to the matrices and vectors.
*/
void Assembly::AssemblyObject::DirichletInit1D(void) {
	int N = (this->A).cols();
	MatrixXd Atemp = (this->A).block(1, 1, N - 2, N - 2);
	(this->A) = Atemp;

	MatrixXd Dtemp = (this->D).block(1, 1, N - 2, N - 2);
	(this->D) = Dtemp;

	if ((this->b).size() == N) {
		VectorXd btemp = (this->b).segment(1, N - 2);
		(this->b) = btemp;
	}

	VectorXd u0temp = (this->u0).segment(1, N - 2);
	(this->u0) = u0temp;

	if ((this->ut0).size() == N) {
		VectorXd u0ttemp = (this->ut0).segment(1, N - 2);
		(this->ut0) = u0ttemp;
	}
}

/*
Initialize matrices and vectors for solver for Neumann BC. We switch top
and bottom rows/cols of system matrix and top and bottom vector elements
with rows/cols from the differentiation matrix D.

@param -
@return -
We work with pointers to the matrices and vectors.
*/
void Assembly::AssemblyObject::NeumannInit1D(void) {
	int N = (this->A).cols();
	(this->A).row(0) = (this->D).row(0);
	(this->A).row(N - 1) = (this->D).row(N - 1);

	if ((this->b).size() == N) {
		(this->b)(0) = this->bcl;
		(this->b)(N - 1) = this->bc0;
	}
}

/*
Initialize matrices and vectors for solver with Dirichlet-Neumann BC.
We switch top and bottom rows/cols of system matrix and top and bottom
vector elements with rows/cols from the differentiation matrix D for
Neumann and remove them for Dirichlet.

@param -
@return -
We work with pointers to the matrices and vectors.
*/
void Assembly::AssemblyObject::DirichletNeumannInit1D(void) {
	int N = (this->A).cols();
	MatrixXd Atemp = (this->A).block(0, 0, N - 1, N - 1);
	(this->A) = Atemp;
	(this->A).row(0) = (this->D).block(0, 0, 1, N - 1);

	MatrixXd Dtemp = (this->D).block(0, 0, N - 1, N - 1);
	(this->D) = Dtemp;

	if ((this->b).size() == N) {
		VectorXd btemp = (this->b).segment(0, N - 1);
		(this->b) = btemp;
		(this->b)(0) = this->bcl;
	}

	if ((this->ut0).size() == N) {
		VectorXd ut0temp = (this->ut0).segment(0, N - 1);
		(this->ut0) = ut0temp;
	}

	VectorXd u0temp = (this->u0).segment(0, N - 1);
	(this->u0) = u0temp;
}

/*
Initialize matrices and vectors for solver with Dirichlet-Neumann BC.
We switch top and bottom rows/cols of system matrix and top and bottom
vector elements with rows/cols from the differentiation matrix D for
Neumann and remove them for Dirichlet.

@param -
@return -
We work with pointers to the matrices and vectors.
*/
void Assembly::AssemblyObject::NeumannDirichletInit1D(void) {
	int N = (this->A).cols();
	MatrixXd Atemp = (this->A).block(1, 1, N - 1, N - 1);
	(this->A) = Atemp;
	(this->A).row(N - 2) = (this->D).block(N - 1, 1, 1, N - 1);

	MatrixXd Dtemp = (this->D).block(1, 1, N - 1, N - 1);
	(this->D) = Dtemp;

	if ((this->b).size() == N) {
		VectorXd btemp = (this->b).segment(1, N - 1);
		(this->b) = btemp;
		(this->b)(N - 2) = this->bc0;
	}

	if ((this->ut0).size() == N) {
		VectorXd ut0temp = (this->ut0).segment(1, N - 1);
		(this->ut0) = ut0temp;
	}

	VectorXd u0temp = (this->u0).segment(1, N - 1);
	(this->u0) = u0temp;
}

/*
Initialize matrices and vectors for solver for dirichlet BC. We remove top
and bottom rows/cols of system matrix and top and bottom vector elements.

@param -
@return -
We work with pointers to the matrices and vectors.
*/
void Assembly::AssemblyObject::DirichletInit2Dx(void) {
	int N = (*Ax).cols();
	(*Ax).row(0).setZero();
	(*Ax).row(N-1).setZero();
	(*Ax).col(0).setZero();
	(*Ax).col(N-1).setZero();

	(this->Dx).row(0).setZero();
	(this->Dx).row(N-1).setZero();
	(this->Dx).col(0).setZero();
	(this->Dx).col(N-1).setZero();

	(this->bmat).row(0).setZero();
	(this->bmat).row(N-1).setZero();
}

/*
Initialize matrices and vectors for solver for Neumann BC. We switch top
and bottom rows/cols of system matrix and top and bottom vector elements
with rows/cols from the differentiation matrix D.

@param -
@return -
We work with pointers to the matrices and vectors.
*/
void Assembly::AssemblyObject::NeumannInit2Dx(void) {
	int N = (*Ax).cols();
	(*Ax).row(0) = (this->Dx).row(0);
	(*Ax).row(N - 1) = (this->Dx).row(N - 1);

	if ((this->bmat).rows() == N) {
		for (int i = 0; i < N; i++) {
			(this->bmat)(0, i) = (this->bclx);
			(this->bmat)(N - 1, i) = (this->bc0x);
		}
	}
}

/*
Initialize matrices and vectors for solver with Dirichlet-Neumann BC.
We switch top and bottom rows/cols of system matrix and top and bottom
vector elements with rows/cols from the differentiation matrix D for
Neumann and remove them for Dirichlet.

@param -
@return -
We work with pointers to the matrices and vectors.
*/
void Assembly::AssemblyObject::DirichletNeumannInit2Dx(void) {
	int N = (*Ax).cols();
	(*Ax).row(0) = (this->Dx).row(0);
	(*Ax).row(N - 1).setZero();
	(*Ax).col(N - 1).setZero();

	(this->Dx).row(N - 1).setZero();
	(this->Dx).col(N - 1).setZero();

	(this->bmat).row(N-1).setZero();

	if ((this->bmat).rows() == N) {
		for (int i = 0; i < N; i++) {
			(this->bmat)(0, i) = (this->bclx);
		}
	}
}

/*
Initialize matrices and vectors for solver with Dirichlet-Neumann BC.
We switch top and bottom rows/cols of system matrix and top and bottom
vector elements with rows/cols from the differentiation matrix D for
Neumann and remove them for Dirichlet.

@param -
@return -
We work with pointers to the matrices and vectors.
*/
void Assembly::AssemblyObject::NeumannDirichletInit2Dx(void) {
	int N = (*Ax).cols();
	(*Ax).row(N - 1) = (this->Dx).row(N - 1);
	(*Ax).row(0).setZero();
	(*Ax).col(0).setZero();

	(this->Dx).row(0).setZero();
	(this->Dx).col(0).setZero();

	(this->bmat).row(0).setZero();

	if ((this->bmat).rows() == N) {
		for (int i = 0; i < N; i++) {
			(this->bmat)(N - 1, i) = (this->bc0x);
		}
	}
}

/*
Initialize matrices and vectors for solver for dirichlet BC. We remove top
and bottom rows/cols of system matrix and top and bottom vector elements.

@param -
@return -
We work with pointers to the matrices and vectors.
*/
void Assembly::AssemblyObject::DirichletInit2Dy(void) {
	int N = (*Ay).cols();
	(*Ay).row(0).setZero();
	(*Ay).row(N-1).setZero();
	(*Ay).col(0).setZero();
	(*Ay).col(N-1).setZero();

	(this->Dy).row(0).setZero();
	(this->Dy).row(N-1).setZero();
	(this->Dy).col(0).setZero();
	(this->Dy).col(N-1).setZero();

	(this->bmat).col(0).setZero();
	(this->bmat).col(N-1).setZero();

	if ((this->u0mat).rows() == N) {
		(this->u0mat).col(0).setZero();
		(this->u0mat).col(N - 1).setZero();
	}

	if ((this->ut0mat).rows() == N) {
		(this->ut0mat).col(0).setZero();
		(this->ut0mat).col(N - 1).setZero();
	}
}

/*
Initialize matrices and vectors for solver for Neumann BC. We switch top
and bottom rows/cols of system matrix and top and bottom vector elements
with rows/cols from the differentiation matrix D.

@param -
@return -
We work with pointers to the matrices and vectors.
*/
void Assembly::AssemblyObject::NeumannInit2Dy(void) {
	int N = (*Ay).cols();
	(*Ay).col(0) = (this->Dy).col(0);
	(*Ay).col(N - 1) = (this->Dy).col(N - 1);

	if ((this->bmat).rows() == N) {
		for (int i = 0; i < N; i++) {
			(this->bmat)(i, 0) = (this->bc0y);
			(this->bmat)(i, N - 1) = (this->bcly);
		}
	}
}

/*
Initialize matrices and vectors for solver with Dirichlet-Neumann BC.
We switch top and bottom rows/cols of system matrix and top and bottom
vector elements with rows/cols from the differentiation matrix D for
Neumann and remove them for Dirichlet.

@param -
@return -
We work with pointers to the matrices and vectors.
*/
void Assembly::AssemblyObject::DirichletNeumannInit2Dy(void) {
	int N = (*Ay).cols();
	(*Ay).row(0) = (this->Dy).row(0);
	(*Ay).row(N - 1).setZero();
	(*Ay).col(N - 1).setZero();

	(this->Dy).row(N - 1).setZero();
	(this->Dy).col(N - 1).setZero();

	(this->bmat).col(N-1).setZero();

	if ((this->bmat).rows() == N) {
		for (int i = 0; i < N; i++) {
			(this->bmat)(i, 0) = (this->bcly);
		}
	}
}

/*
Initialize matrices and vectors for solver with Dirichlet-Neumann BC.
We switch top and bottom rows/cols of system matrix and top and bottom
vector elements with rows/cols from the differentiation matrix D for
Neumann and remove them for Dirichlet.

@param -
@return -
We work with pointers to the matrices and vectors.
*/
void Assembly::AssemblyObject::NeumannDirichletInit2Dy(void) {
	int N = (*Ay).cols();
	(*Ay).row(N - 1) = (this->Dy).row(N - 1);
	(*Ay).row(0).setZero();
	(*Ay).col(0).setZero();

	(this->Dy).row(0).setZero();
	(this->Dy).col(0).setZero();

	(this->bmat).col(0).setZero();

	if ((this->bmat).rows() == N) {
		for (int i = 0; i < N; i++) {
			(this->bmat)(i, N - 1) = (this->bc0y);
		}
	}
}

/*
    Namespace: Polynomials
    Set up polynomial objects
*/

/*
    Return random initial guess vector u0

    @param N is the number of nodes
    @return the vector of the initial guess
*/
VectorXd Polynomials::PolynomialBasis::RandomInitialGuess1D(int N) {
    VectorXd u0 = VectorXd::Random(N);
    return u0;
}

/*
    Return zero initial guess vector u0

    @param N is the number of nodes
    @return the vector of the initial guess
*/
VectorXd Polynomials::PolynomialBasis::ZeroInitialGuess1D(int N) {
    VectorXd u0 = VectorXd::Zero(N);
    return u0;
}

/*
    Return random initial guess matrix u0mat

    @param N is the number of nodes
    @return the matrix of the initial guess
*/
MatrixXd Polynomials::PolynomialBasis::RandomInitialGuess2D(int N) {
    MatrixXd u0mat = MatrixXd::Random(N,N);
    return u0mat;
}

/*
    Return zero initial guess matrix u0mat

    @param N is the number of nodes
    @return the matrix of the initial guess
*/
MatrixXd Polynomials::PolynomialBasis::ZeroInitialGuess2D(int N) {
    MatrixXd u0mat = MatrixXd::Zero(N,N);
    return u0mat;
}

/*
    Return N Chebyshev Gauss nodes

    @param N is the number of nodes
    @return the vector of nodes
*/
VectorXd Polynomials::PolynomialBasis::ChebyshevGaussNodes(int N,
    double x0, double xl) {
    VectorXd x(N);
    for (int i = 0; i < N; i++) {
        x(i) = -cos((2*(i+1)-1) * M_PI / (2*N));
    }

    // Check for interval different than [-1,1]
    if (x0 != -1 || xl != 1) {
        for (int i = 0; i < N; i++) {
            x(i) = ((xl - x0)/2) * x(i) + ((x0 + xl)/2);
        }
    }

    return x;
}

/*
    Return N Chebyshev Gauss weights

    @param N is the number of nodes
    @return the vector of weights
*/
VectorXd Polynomials::PolynomialBasis::ChebyshevGaussWeights(int N) {
    VectorXd w(N);
    for (int i = 0; i < N; i++) {
        w(i) = M_PI / (N);
    }
        return w;
}

/*
    Return N Chebyshev Gauss-Lobatto nodes

    @param N is the number of nodes
    @return the vector of nodes
*/
VectorXd Polynomials::PolynomialBasis::ChebyshevGaussLobattoNodes(int N,
    double x0, double xl) {
    VectorXd x(N);
    for (int i = 0; i < N; i++) {
        x(i) = cos(i * M_PI / (N-1));
    }

    // Check for interval different than [-1,1]
    if (x0 != -1 || xl != 1) {
        for (int i = 0; i < N; i++) {
            x(i) = ((xl - x0)/2) * x(i) + ((x0 + xl)/2);
        }
    }

    return x;
}

/*
    Return N Chebyshev Gauss-Lobatto weights

    @param N is the number of nodes
    @return the vector of weights
*/
VectorXd Polynomials::PolynomialBasis::ChebyshevGaussLobattoWeights(int N) {
    VectorXd w(N);
    for (int i = 0; i < N; i++) {
        w(i) = M_PI / (N-1);
    }
        w(0) = w(0) / 2;
        w(N-1) = w(N-1) / 2;
        return w;
}

/*
    Return N Legendre Gauss nodes

    @param N is the number of nodes
    @return the vector of nodes
*/
VectorXd Polynomials::PolynomialBasis::LegendreGaussNodes(int N,
    double x0, double xl) {
    int iter = 100;
    int k;
    double tol = 1.e-09;
    double L, Lprime, delta;
    VectorXd x(N);

    if (N==1) {
        x(0) = 0;
    } else if (N==2) {
        x(0) = - sqrt(1./3);
        x(1) = - x(0);
    } else {
        for (int j = 0; j < ((N+1)/2)-1; j++) {
            x(j) = -cos((2*j+1) * M_PI / (2*N+1));
            k = 0;
            while (k < iter) {
                L = Legendre(N, x(j));
                Lprime = LegendrePrime(N, x(j));
                delta = - L / Lprime;
                x(j) += delta;
                if (fabs(delta) <= tol*fabs(x(j)))
                    break;
                k++;
            }
            x(N-j-1) = -x(j);
        }
    }

    if (N % 2 == 1) {
        x(N/2) = 0;
    }

    // Check for interval different than [-1,1]
    if (x0 != -1 || xl != 1) {
        for (int i = 0; i < N; i++) {
            x(i) = ((xl - x0)/2) * x(i) + ((x0 + xl)/2);
        }
    }

    return x;
}

/*
    Return N Legendre Gauss weights

    @param N is the number of nodes
    @return the vector of weights
*/
VectorXd Polynomials::PolynomialBasis::LegendreGaussWeights(int N) {
    double L, Lprime, delta;
    int iter = 100;
    int k;
    double tol = 1.e-09;
    VectorXd x(N);
    VectorXd w(N);

    if (N==1) {
        w(0) = 2;
    } else if (N==2) {
        w(0) = 1;
        w(1) = w(0);
    } else {
        for (int j = 0; j < ((N+1)/2)-1; j++) {
            x(j) = -cos((2*j+1) * M_PI / (2*N+1));
            k = 0;
            while (k < iter) {
                L = Legendre(N, x(j));
                Lprime = LegendrePrime(N, x(j));
                delta = - L / Lprime;
                x(j) += delta;
                if (fabs(delta) <= tol*fabs(x(j)))
                    break;
                k++;
            }
            //Lprime = LegendrePrime(N-1, x(j));
            w(j) = 2 / ((1-pow(x(j),2))*pow(Lprime,2));
            w(N-j-1) = w(j);
        }
    }

    if (N % 2 == 1) {
        Lprime = LegendrePrime(N, 0.);
        w(N/2) = 2 / pow(Lprime, 2);
    }
    return w;
}

/*
    Return N Legendre Gauss-Lobatto nodes

    @param N is the number of nodes
    @return the vector of nodes
*/
VectorXd Polynomials::PolynomialBasis::LegendreGaussLobattoNodes(int N,
    double x0, double xl) {
    VectorXd x(N);
    for (int i = 0; i < N; i++) {
        x(i) = cos(i * M_PI / (N-1));
    }

    // Check for interval different than [-1,1]
    if (x0 != -1 || xl != 1) {
        for (int i = 0; i < N; i++) {
            x(i) = ((xl - x0)/2) * x(i) + ((x0 + xl)/2);
        }
    }

    return x;
}

/*
    Return N Legendre Gauss-Lobatto weights

    @param N is the number of nodes
    @return the vector of weights
*/
VectorXd Polynomials::PolynomialBasis::LegendreGaussLobattoWeights(int N) {
    VectorXd w(N);
    for (int i = 0; i < N; i++) {
        w(i) = M_PI / (N-1);
    }
        w(0) = w(0) / 2;
        w(N-1) = w(N-1) / 2;
        return w;
}

/*
    Return the k-order legendre polynomial value at point x

    @param k is the polynomial order and x is the point of evaluation
    @return the legendre polynomial value
*/
double Polynomials::PolynomialBasis::Legendre(int k, double x) {
    double Lk, Lprev, Lprev2;

    if (k == 0) {
        return 1.;
    } else if (k == 1) {
        return 0.;
    }

    Lprev2 = 1;
    Lprev = x;

    for (int i = 1; i < k+1; i++) {
        Lk = ((2.0*i-1)/i) * x * Lprev - ((i-1.0)/i) * Lprev2;
        Lprev2 = Lprev;
        Lprev = Lk;
    }

    return Lk;
}

/*
    Return the k-order legendre prime polynomial value at point x

    @param k is the polynomial order and x is the point of evaluation
    @return the legendre prime polynomial value
*/
double Polynomials::PolynomialBasis::LegendrePrime(int k, double x) {
    double Lk, LkPrime, Lprev, Lprev2, LprevPrime, LprevPrime2;

    if (k == 0) {
        return 0.;
    } else if (k == 1) {
        return 1.;
    }

    Lprev2 = 1;
    Lprev = x;

    LprevPrime2 = 0;
    LprevPrime = 1;

    for (int i = 1; i < k+1; i++) {
        Lk = ((2.0*i-1)/i) * x * Lprev - ((i-1.0)/i) * Lprev2;
        LkPrime = (2.*i-1) * Lprev + LprevPrime2;
        Lprev2 = Lprev;
        Lprev = Lk;
        LprevPrime2 = LprevPrime;
        LprevPrime = LkPrime;
    }

    return LkPrime;
}

/*
    Return the polynomial derivative matrix

    @param x and w are the gaussian nodes and weights
    @return the polynomial derivative matrix
*/
MatrixXd Polynomials::PolynomialBasis::DerivativeMatrix(VectorXd x, VectorXd w) {
     int len = x.size();
     MatrixXd D(len, len);

     for (int i = 0; i < len; i++) {
         for (int j = 0; j < len; j++) {
             if (j != i) {
                 D(i, j) = pow(-1, i+j) * (w(j)/ w(i)) * (1/ (x(i) - x(j)));
             }
         }
     }

     D = NegativeSumTrick(D);

     return D;
}

/*
    Return the m-order polynomial derivative matrix

    @param m is the derivative order, x and w are the gaussian nodes and weights
    @return the m-order polynomial derivative matrix
*/
MatrixXd Polynomials::PolynomialBasis::MDerivativeMatrix(int m,
    VectorXd x, VectorXd w) {
     int len = x.size();
     MatrixXd D(len, len);
     MatrixXd Dm(len, len);
     D = DerivativeMatrix(x, w);

     if (m == 1)
        return D;
     for (int k = 2; k < m+1; k++) {
         for (int i = 0; i < len; i++) {
             D(i, i) = 0;
             for (int j = 0; j < len; j++) {
                 if (j != i) {
                    Dm(i, j) = (k / (x(i) - x(j))) *
                        ((w(j)/w(i)) * D(i, i) - D(i, j));
                    Dm(i, i) = Dm(i, i) - Dm(i, j);
                 }
             }
         }
     }

     return Dm;
}

/*
    Return the 2nd order derivative matrix

    @param the derivative matrix
    @return the 2nd order derivative matrix
*/
MatrixXd Polynomials::PolynomialBasis::DerivativeMatrix2(MatrixXd D) {
    MatrixXd D2;
    D2 = D * D;

    return D2;
}

/*
    Compute the Fourier nodes

    @param the number of nodes
    @return the vector of nodes
*/
VectorXd Fourier::FourierBasis::FourierNodes(int N) {
    VectorXd x(N);
    for (int i = 0; i < N; i++) {
        x(i) = 2 * M_PI * i / (N - 1);
    }

    return x;
}

/*
    Compute the Fourier differentiation matrix

    @param the number of nodes and the order of the matrix
    @return the differentiation matrix
*/
MatrixXd Fourier::FourierBasis::DerivativeMatrix(int N, int m) {
    MatrixXd D(N, N);
    VectorXd kk(N);
    int n1, n2, n;
    double h;

    for (int i = 0; i < N; i++) {
        kk(i) = i+1;
    }

    h = 2 * M_PI / N;
    n1 = (int)floor((N-1) / 2.);
    n2 = (int)ceil((N-1) / 2.);
    n = n1 + n2;

    if (m == 0) {
        D = MatrixXd::Identity(N, N);
        return D;
    }

    VectorXd kk_factor = VectorXd::Zero(kk.size());

    VectorXd topc = VectorXd::Zero(n);
    VectorXd col = VectorXd::Zero(n+1);
    VectorXd row = VectorXd::Zero(n+1);

    if (m == 1) {
        if (N % 2 == 0) {
            for (int i = 0; i < n2; i++) {
                topc(i) = 1 / tan((i+1) * h/2);
            }
            for (int i = 0; i < n1; i++) {
                topc(topc.size()-1-i) = -topc(i);
            }
            for (int i = 0; i < kk_factor.size(); i++) {
                kk_factor(i) = 0.5 * pow(-1, kk(i));
            }
            for (int i = 1; i < col.size(); i++) {
                col(i) = kk_factor(i-1) * topc(i-1);
                row(i) = -col(i);
            }
        } else {
            for (int i = 0; i < n2; i++) {
                topc(i) = 1 / sin((i+1) * h/2);
            }
            for (int i = 0; i < n1; i++) {
                topc(topc.size()-1-i) = -topc(i);
            }
            for (int i = 0; i < kk_factor.size(); i++) {
                kk_factor(i) = 0.5 * pow(-1, kk(i));
            }
            for (int i = 1; i < col.size(); i++) {
                col(i) = kk_factor(i-1) * topc(i-1);
                row(i) = -col(i);
            }
        }
    } else if (m == 2) {
        if (N % 2 == 0) {
            for (int i = 0; i < n2; i++) {
                topc(i) = 1 / pow(sin((i+1) * h/2), 2);
            }
            for (int i = 0; i < n1; i++) {
                topc(topc.size()-1-i) = topc(i);
            }
            for (int i = 0; i < kk_factor.size(); i++) {
                kk_factor(i) = -0.5 * pow(-1, kk(i));
            }
            col(0) = (-pow(M_PI, 2) / (3*pow(h, 2))) - 1./6;
            row(0) = col(0);
            for (int i = 1; i < col.size(); i++) {
                col(i) = kk_factor(i-1) * topc(i-1);
                row(i) = col(i);
            }
        } else {
            for (int i = 0; i < n2; i++) {
                topc(i) = (1 / sin((i+1) * h/2)) * (1 / tan((i+1) * h/2));
            }
            for (int i = 0; i < n1; i++) {
                topc(topc.size()-1-i) = -topc(i);
            }
            for (int i = 0; i < kk_factor.size(); i++) {
                kk_factor(i) = -0.5 * pow(-1, kk(i));
            }
            col(0) = (-pow(M_PI, 2) / (3*pow(h, 2))) + 1./12;
            row(0) = col(0);
            for (int i = 1; i < col.size(); i++) {
                col(i) = kk_factor(i-1) * topc(i-1);
                row(i) = col(i);
            }
        }
    }

    D = ToeplitzMatrix(col, row);

    return D;
}

/*
    Compute the Toeplitz matrix

    @param the vectors of the first column and row
    @return the toeplitz
*/
MatrixXd Fourier::FourierBasis::ToeplitzMatrix(VectorXd col, VectorXd row) {
    int N = col.size();
    MatrixXd T = MatrixXd::Zero(N, N);
    int temp = 0;

    for (int i = 0; i < N; i++) {
        for (int j = 0; j < N; j++) {
            if (i-j >= 0) {
                T(i,j) = col(i-j);
            } else if (j-i > 0) {
                T(i,j) = row(j-i);
            }
        }
    }
    T = NegativeSumTrick(T);

    return T;
}

/*
    Return the 2D derivative matrix

    @param the derivative matrix
    @return the 2-dimensional derivative matrix
*/

SparseMatrix<double, RowMajor> Dimensions::Extend2D::DerivativeMatrix2D(MatrixXd Dx, MatrixXd Dy){
    SparseMatrix<double, RowMajor> D2D;
    SparseMatrix<double, RowMajor> Dxsp;
    SparseMatrix<double, RowMajor> Dysp;
    MatrixXd I;
    I = MatrixXd::Identity(Dx.rows(), Dx.cols());

    Dxsp = Dx.sparseView();
    Dysp = Dy.sparseView();

    D2D = kroneckerProduct(I, Dxsp).eval() + kroneckerProduct(Dysp, I).eval();

    return D2D;
}
