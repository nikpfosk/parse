## PARSE ##

PARallel Spectral Elements : A Spectral Element (SEM) framework with parallel computing capabilities. 

Spectral element methods are gaining popularity in scientific and engineering applications due to their 
high accuracy and flexibility. The framework is designed to solve common stationary and time-dependent 
differential equations in one and two dimensions. The core is developed in C++, taking advantage of optimized 
libraries for dense and sparse matrix manipulations, and built-in Python wrappers are developed for the 
visualization and rendering of results. The user can simply determine the type of equation to be solved along
with the boundary conditions, order of approximation, basis types and solver type. The entire assembly, solver 
and visualization processes are automatically performed. Parallel computing capabilities are embedded for 
distributed memory systems and there is a plan to enable GPU computations in the future.
